package com.angkorteam.cicd.tomcat90.extension.openssl;

import org.apache.tomcat.util.net.SSLHostConfigCertificate;
import org.apache.tomcat.util.net.SSLUtil;

public class OpenSSLImplementation extends org.apache.tomcat.util.net.openssl.OpenSSLImplementation {

    @Override
    public SSLUtil getSSLUtil(SSLHostConfigCertificate certificate) {
        return new OpenSSLUtil(certificate);
    }

}
