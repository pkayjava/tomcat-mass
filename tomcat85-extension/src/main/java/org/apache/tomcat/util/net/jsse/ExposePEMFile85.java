package org.apache.tomcat.util.net.jsse;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class ExposePEMFile85 extends org.apache.tomcat.util.net.jsse.PEMFile {

    public ExposePEMFile85(String filename) throws IOException, GeneralSecurityException {
        super(filename);
    }

    public ExposePEMFile85(String filename, String password) throws IOException, GeneralSecurityException {
        super(filename, password);
    }

}
