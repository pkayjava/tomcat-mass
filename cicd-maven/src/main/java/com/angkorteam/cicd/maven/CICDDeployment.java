package com.angkorteam.cicd.maven;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.model.Build;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

@Mojo(name = "upload", defaultPhase = LifecyclePhase.INSTALL)
public class CICDDeployment extends AbstractMojo {

	@Parameter(readonly = true, defaultValue = "${project}")
	protected MavenProject project;

	@Parameter(readonly = true, defaultValue = "${project.build}")
	protected Build build;

	@Parameter(readonly = true, defaultValue = "${project.artifact}")
	protected Artifact artifact;

	@Parameter(property = "serverUrl", required = true)
	protected URL serverUrl;

	@Parameter(property = "serverToken", required = true)
	protected String serverToken;

	@Parameter(property = "serverSecret", required = true)
	protected String serverSecret;

	@Parameter(property = "serverHost", required = true)
	protected String serverHost;

	@Parameter(property = "serverInstance", required = true)
	protected String serverInstance;

	@Parameter(property = "serverVhost", required = false)
	protected String serverVhost;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {

		if ("war".equalsIgnoreCase(this.artifact.getType())) {
			if (this.serverVhost == null || "".equals(this.serverVhost) || this.serverUrl == null || this.serverHost == null || "".equals(this.serverHost) || this.serverInstance == null || "".equals(this.serverInstance) || this.serverToken == null || "".equals(this.serverToken) || this.serverSecret == null || "".equals(this.serverSecret)) {
				getLog().info("serverUrl, serverHost, serverInstance, serverVhost, serverToken, serverSecret are required");
				return;
			}
		} else if ("jar".equalsIgnoreCase(this.artifact.getType())) {
			if (this.serverUrl == null || this.serverHost == null || "".equals(this.serverHost) || this.serverInstance == null || "".equals(this.serverInstance) || this.serverToken == null || "".equals(this.serverToken) || this.serverSecret == null || "".equals(this.serverSecret)) {
				getLog().info("serverUrl, serverHost, serverInstance, serverToken, serverSecret are required");
				return;
			}
		} else {
			return;
		}

		File file = this.artifact.getFile();
		String address = this.serverUrl.toExternalForm();
		if (address.endsWith("/")) {
			address = address.substring(0, address.length() - 1);
		}
		if ("war".equalsIgnoreCase(this.artifact.getType()) || "jar".equalsIgnoreCase(this.artifact.getType())) {
			try (CloseableHttpClient client = HttpClients.createDefault()) {
				HttpPost post = null;
				if ("war".equalsIgnoreCase(this.artifact.getType())) {
					post = new HttpPost(this.serverUrl.toExternalForm() + "/deploy/war/" + this.serverHost + "/" + this.serverInstance);
				} else if ("jar".equalsIgnoreCase(this.artifact.getType())) {
					post = new HttpPost(this.serverUrl.toExternalForm() + "/deploy/library/" + this.serverHost + "/" + this.serverInstance);
				}

				final String pair = this.serverToken + ":" + this.serverSecret;
				final byte[] encodedBytes = Base64.encodeBase64(pair.getBytes());

				post.addHeader("Authorization", "Basic " + new String(encodedBytes));

				FileBody fileBody = new FileBody(file);

				MultipartEntityBuilder bodyBuilder = MultipartEntityBuilder.create();
				if ("war".equalsIgnoreCase(this.artifact.getType())) {
					bodyBuilder.addPart("warFile", fileBody);
					bodyBuilder.addPart("vhost", new StringBody(this.serverVhost, ContentType.TEXT_PLAIN));
				} else if ("jar".equalsIgnoreCase(this.artifact.getType())) {
					bodyBuilder.addPart("jarFile", fileBody);
				}
				HttpEntity requestBody = bodyBuilder.build();

				post.setEntity(requestBody);

				if (getLog().isInfoEnabled()) {
					String header = "CICD Server - " + post.getRequestLine().getUri();
					String body = "Deploying " + file.getName() + " to " + this.serverInstance;
					String timestamp = "Timestamp " + DateFormatUtils.ISO_8601_EXTENDED_DATETIME_TIME_ZONE_FORMAT.format(new Date());
					String filename = "Filename " + file.getName();
					String vhost = "VHost " + this.serverVhost;
					int length = Math.max(filename.length(), Math.max(header.length(), Math.max(body.length(), timestamp.length())));
					getLog().info(StringUtils.repeat("=", length + 4));
					getLog().info("= " + header + StringUtils.repeat(" ", length - header.length()) + " =");
					getLog().info(StringUtils.repeat("=", length + 4));
					if ("war".equalsIgnoreCase(this.artifact.getType())) {
						getLog().info("= " + vhost + StringUtils.repeat(" ", length - vhost.length()) + " =");
					}
					getLog().info("= " + filename + StringUtils.repeat(" ", length - filename.length()) + " =");
					getLog().info("= " + timestamp + StringUtils.repeat(" ", length - timestamp.length()) + " =");
					getLog().info(StringUtils.repeat("=", length + 4));
				}
				try (CloseableHttpResponse response = client.execute(post)) {
					getLog().info(response.getStatusLine().toString());
					HttpEntity responseBody = response.getEntity();
					EntityUtils.consume(responseBody);
				} catch (IOException e) {
					getLog().info("error : " + e.getMessage());
				}
			} catch (IOException e) {
				getLog().info("error : " + e.getMessage());
			}
		}
	}

}
