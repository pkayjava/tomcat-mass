package com.angkorteam.cicd.maven;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.artifact.handler.ArtifactHandler;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.model.Build;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.logging.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

@Mojo(name = "document", defaultPhase = LifecyclePhase.COMPILE)
public class CICDDocument extends AbstractMojo {

	@Parameter(readonly = true, defaultValue = "${project}")
	protected MavenProject project;

	@Parameter(readonly = true, defaultValue = "${project.build}")
	protected Build build;

	@Parameter(readonly = true, defaultValue = "${project.artifact}")
	protected Artifact artifact;

	@Component
	protected Logger logger;

	@Parameter(defaultValue = "${localRepository}", required = true, readonly = true)
	protected ArtifactRepository localRepository;

	@Parameter(property = "swaggerInfo", required = true)
	protected String swaggerInfo;

	@Parameter(property = "jvm", required = false)
	protected String jvm;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		Artifact artifact = this.project.getPluginArtifactMap().get("com.angkorteam.cicd:cicd-maven");

		ArtifactHandler handler = artifact.getArtifactHandler();

		File jarFile = this.localRepository.find(artifact).getFile();
		File pomFile = new File(jarFile.getParentFile(), FilenameUtils.getBaseName(jarFile.getName()) + ".pom");

		List<Artifact> libraries = new ArrayList<>();

		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(pomFile);
			Element root = document.getDocumentElement();
			for (int i = 0; i < root.getChildNodes().getLength(); i++) {
				if (root.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE && ((Element) root.getChildNodes().item(i)).getTagName().equals("dependencies")) {
					Element dependencies = (Element) root.getChildNodes().item(i);
					for (int j = 0; j < dependencies.getChildNodes().getLength(); j++) {
						if (dependencies.getChildNodes().item(j).getNodeType() == Node.ELEMENT_NODE && ((Element) dependencies.getChildNodes().item(j)).getTagName().equals("dependency")) {
							Element dependency = (Element) dependencies.getChildNodes().item(j);
							String groupId = dependency.getElementsByTagName("groupId").item(0).getTextContent();
							String artifactId = dependency.getElementsByTagName("artifactId").item(0).getTextContent();
							String version = dependency.getElementsByTagName("version").item(0).getTextContent();
							if (version.startsWith("${")) {
								version = (String) this.project.getProperties().get(version.substring(2, version.length() - 1));
							}
							Artifact library = this.localRepository.find(new DefaultArtifact(groupId, artifactId, version, null, "jar", null, handler));
							libraries.add(library);
						}
					}
				}
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
		}

		List<String> jars = new ArrayList<>();
		jars.add(jarFile.getAbsolutePath());

		for (Artifact library : libraries) {
			jars.add(library.getFile().getAbsolutePath());
		}

		List<String> compileClasses = null;
		try {
			compileClasses = this.project.getCompileClasspathElements();
			jars.addAll(compileClasses);
		} catch (DependencyResolutionRequiredException e) {
		}

		File swaggerFolder = new File(this.project.getBuild().getDirectory(), "swagger");
		swaggerFolder.mkdirs();

		CommandLine cmdLine = null;
		if (this.jvm != null && !"".equals(this.jvm)) {
			cmdLine = CommandLine.parse(new File(this.jvm).getAbsolutePath() + "/bin/java");
		} else {
			cmdLine = CommandLine.parse(System.getProperty("java.home") + "/bin/java");
		}
		cmdLine.addArgument("-classpath", false);
		cmdLine.addArgument(StringUtils.join(jars, File.pathSeparatorChar), true);
		cmdLine.addArgument(SwaggerDocument.class.getName());
		File swaggerFile = null;
		if (this.swaggerInfo != null && !"".equals(this.swaggerInfo)) {
			swaggerFile = new File(this.swaggerInfo);
		}
		if (swaggerFile != null && swaggerFile.isFile()) {
			cmdLine.addArgument(swaggerFile.getAbsolutePath(), true);
			if (compileClasses != null) {
				for (String compileClass : compileClasses) {
					cmdLine.addArgument(compileClass, true);
				}
			}
			DefaultExecutor executor = new DefaultExecutor();
			executor.setWorkingDirectory(swaggerFolder);
			try {
				executor.execute(cmdLine);
			} catch (IOException e) {
			}
		}
	}

}
