package com.angkorteam.cicd.maven;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.TreeSet;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import io.swagger.models.Swagger;
import io.swagger.util.Json;

public class SwaggerDocument {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		File userDir = new File(System.getProperty("user.dir"));
		if (args != null && args.length > 0) {
			// ObjectMapper objectMapper = Json.mapper();
			File swaggerFile = new File(args[0]);
			// Swagger swagger =
			// objectMapper.readValue(FileUtils.readFileToString(swaggerFile, "UTF-8"),
			// Swagger.class);
			Swagger swagger = new Swagger();
			if (swaggerFile.isFile()) {
				// path process
				for (int i = 1; i < args.length; i++) {
					Class<?> clazz = Class.forName(args[i]);
					processPath(swagger, clazz);
				}
			}
			Json.pretty().writeValue(new File(userDir, "swagger.json"), swagger);
		}
	}

	public static void processPath(Swagger swagger, Class<?> clazz) {
		if (clazz.isAnnotationPresent(Api.class)) {
			Api api = clazz.getAnnotation(Api.class);
			String value = normolizePath(api.value());
			String[] tags = api.tags();
			String produces = api.produces();
			String consumes = api.consumes();
			String protocols = api.protocols();
			Authorization[] authorizations = api.authorizations();
			boolean hidden = api.hidden();
			boolean springPath = false;
			boolean wsPath = false;
			if (clazz.isAnnotationPresent(RestController.class)) {
				RestController restController = clazz.getAnnotation(RestController.class);
				springPath = true;
			} else if (clazz.isAnnotationPresent(Path.class)) {
				Path path = clazz.getAnnotation(Path.class);
				wsPath = true;
				if (path.value() != null && !"".equals(path.value()) && !"/".equals(path.value())) {
					value = normolizePath(path.value());
				} else {
					value = "/";
				}
			}

			Method[] methods = clazz.getDeclaredMethods();
			if (methods != null && methods.length > 0) {
				for (Method method : methods) {
					if (springPath) {
						processSpringPath(swagger, clazz, value, method);
					}
					if (wsPath) {
						processWsPath(swagger, clazz, value, method);
					}
				}
			}
		}
	}

	public static void processSpringPath(Swagger swagger, Class<?> clazz, String parentPath, Method method) {
		if (method.isAnnotationPresent(GetMapping.class) || method.isAnnotationPresent(PostMapping.class) || method.isAnnotationPresent(PutMapping.class) || method.isAnnotationPresent(DeleteMapping.class) || method.isAnnotationPresent(PatchMapping.class)) {
			if (method.isAnnotationPresent(GetMapping.class)) {
				GetMapping mapping = method.getAnnotation(GetMapping.class);
				processSpringPath(swagger, parentPath, new RequestMethod[] { RequestMethod.GET }, mapping.name(), mapping.value(), mapping.params(), mapping.params(), mapping.headers(), mapping.consumes(), mapping.produces());
			}
			if (method.isAnnotationPresent(PostMapping.class)) {
				PostMapping mapping = method.getAnnotation(PostMapping.class);
				processSpringPath(swagger, parentPath, new RequestMethod[] { RequestMethod.POST }, mapping.name(), mapping.value(), mapping.params(), mapping.params(), mapping.headers(), mapping.consumes(), mapping.produces());
			}
			if (method.isAnnotationPresent(PutMapping.class)) {
				PutMapping mapping = method.getAnnotation(PutMapping.class);
				processSpringPath(swagger, parentPath, new RequestMethod[] { RequestMethod.PUT }, mapping.name(), mapping.value(), mapping.params(), mapping.params(), mapping.headers(), mapping.consumes(), mapping.produces());
			}
			if (method.isAnnotationPresent(DeleteMapping.class)) {
				DeleteMapping mapping = method.getAnnotation(DeleteMapping.class);
				processSpringPath(swagger, parentPath, new RequestMethod[] { RequestMethod.DELETE }, mapping.name(), mapping.value(), mapping.params(), mapping.params(), mapping.headers(), mapping.consumes(), mapping.produces());
			}
			if (method.isAnnotationPresent(PatchMapping.class)) {
				PatchMapping mapping = method.getAnnotation(PatchMapping.class);
				processSpringPath(swagger, parentPath, new RequestMethod[] { RequestMethod.PATCH }, mapping.name(), mapping.value(), mapping.params(), mapping.params(), mapping.headers(), mapping.consumes(), mapping.produces());
			}
		} else if (method.isAnnotationPresent(RequestMapping.class)) {
			RequestMapping mapping = method.getAnnotation(RequestMapping.class);
			processSpringPath(swagger, parentPath, mapping.method(), mapping.name(), mapping.value(), mapping.params(), mapping.params(), mapping.headers(), mapping.consumes(), mapping.produces());
		}
	}

	public static void processSpringPath(Swagger swagger, String parentPath, RequestMethod[] method, String name, String[] value, String[] path, String[] params, String[] headers, String[] consumes, String[] produces) {
		
		Set<String> paths = new TreeSet<>();
		if (value != null) {
			for (String v : value) {
				paths.add(normolizePath(parentPath,v));
			}
		}
		if (path != null) {
			for (String p : path) {
				paths.add(normolizePath(parentPath,p));
			}
		}
		
		for (String p : paths) {
			io.swagger.models.Path swaggerPath = swagger.getPaths().get(p);
			if (swaggerPath == null) {
				swaggerPath = new io.swagger.models.Path();
				swagger.getPaths().put(p, swaggerPath);
			}
			for (RequestMethod m : method) {
				
			}
		}


//		String name() default ""; 
//
//	 
//		RequestMethod[] method() default {};
// 
//		String[] params() default {};
//
//		 
//		String[] headers() default {};
//
//	 
//		String[] consumes() default {};
//
//		 
//		String[] produces() default {};
		
	}

	public static void processWsPath(Swagger swagger, Class<?> clazz, String value, Method method) {

	}

	public static String normolizePath(String path) {
		if (path == null || "".equals(path) || "/".equals(path)) {
			return "/";
		} else {
			if (path.startsWith("/") && path.endsWith("/")) {
				return path.substring(1, path.length() - 1);
			} else {
				if (path.startsWith("/")) {
					return path.substring(1, path.length());
				} else if (path.endsWith("/")) {
					return path.substring(0, path.length() - 1);
				} else {
					return path;
				}
			}
		}
	}

	public static String normolizePath(String parentPath, String path) {
		parentPath = normolizePath(parentPath);
		path = normolizePath(path);
		if ("/".equals(parentPath)) {
			return path;
		} else {
			return parentPath + path;
		}
	}

	public static void processSwagger(Swagger swagger, Class<?> clazz) {
		if (clazz.isAnnotationPresent(Api.class) && clazz.isAnnotationPresent(RestController.class)) {
			Method[] methods = clazz.getDeclaredMethods();
			if (methods != null && methods.length > 0) {
				for (Method method : methods) {
					processSpringMethod(swagger, clazz, method);
				}
			}
		} else if (clazz.isAnnotationPresent(Api.class) && clazz.isAnnotationPresent(Path.class)) {
			Method[] methods = clazz.getDeclaredMethods();
			if (methods != null && methods.length > 0) {
				for (Method method : methods) {
					processWsMethod(swagger, clazz, method);
				}
			}
		}
	}

	public static void processSpringMethod(Swagger swagger, Class<?> clazz, Method method) {

	}

	public static void processWsMethod(Swagger swagger, Class<?> clazz, Method method) {
		String path = null;
		String verbose = "GET";

		if (method.isAnnotationPresent(Path.class) || method.isAnnotationPresent(PUT.class) || method.isAnnotationPresent(POST.class) || method.isAnnotationPresent(GET.class)) {
		}
	}

}
