select 
	tbl_tomcat_file.path,
	tbl_tomcat_file.vhost,
	group_concat(DISTINCT tbl_tomcat_vhost_port.tls SEPARATOR ', ') as tls,
	group_concat(DISTINCT tbl_tomcat_vhost_port.tls_mode SEPARATOR ', ') as tls_mode,
	group_concat(DISTINCT tbl_tomcat_vhost_protocol.name SEPARATOR ', ') as protocol,
	group_concat(DISTINCT tbl_tomcat_vhost_port.port SEPARATOR ', ') as port
from tbl_tomcat_file
inner join tbl_tomcat_vhost on tbl_tomcat_vhost.name = tbl_tomcat_file.vhost
inner join tbl_tomcat_vhost_port on tbl_tomcat_vhost_port.tomcat_vhost_id = tbl_tomcat_vhost.id
inner join tbl_tomcat_vhost_protocol on tbl_tomcat_vhost_protocol.tomcat_vhost_id = tbl_tomcat_vhost.id
group by tbl_tomcat_file.id;

select 
	tbl_tomcat_file.path,
	tbl_tomcat_file.vhost,
	tbl_tomcat_vhost_port.tls,
	tbl_tomcat_vhost_port.tls_mode,
	tbl_tomcat_vhost_protocol.name as protocol,
	tbl_tomcat_vhost_port.port
from tbl_tomcat_file
inner join tbl_tomcat_vhost on tbl_tomcat_vhost.name = tbl_tomcat_file.vhost
inner join tbl_tomcat_vhost_port on tbl_tomcat_vhost_port.tomcat_vhost_id = tbl_tomcat_vhost.id
inner join tbl_tomcat_vhost_protocol on tbl_tomcat_vhost_protocol.tomcat_vhost_id = tbl_tomcat_vhost.id;

select 
	tbl_tomcat_vhost.name,
	tbl_tomcat_vhost_protocol.name as protocol
from tbl_tomcat_vhost_protocol
inner join tbl_tomcat_vhost on tbl_tomcat_vhost_protocol.tomcat_vhost_id = tbl_tomcat_vhost.id;


select tbl_tomcat_vhost.name, tbl_tomcat_vhost_port.port, tbl_tomcat_vhost_port.tls, tbl_tomcat_vhost_port.tls_mode from tbl_tomcat_vhost_port inner join tbl_tomcat_vhost on tbl_tomcat_vhost_port.tomcat_vhost_id = tbl_tomcat_vhost.id;