#!/bin/sh

# CATALINA_OPTS="$CATALINA_OPTS -javaagent:/opt/glowroot/glowroot.jar"

# JAVA_OPTS="$JAVA_OPTS -Djavax.net.debug=ssl"
JAVA_OPTS="$JAVA_OPTS -Djava.security.egd=file:/dev/./urandom"

JAVA_OPTS="$JAVA_OPTS -DwinBash=C:/apps/Git/git-bash.exe"
JAVA_OPTS="$JAVA_OPTS -DactivemqServer=failover:tcp://172.16.1.107:61616"
JAVA_OPTS="$JAVA_OPTS -DactivemqChannel=stateChannel"
JAVA_OPTS="$JAVA_OPTS -DdashboardServer=http://172.16.1.107:8080/api"
JAVA_OPTS="$JAVA_OPTS -Dtoken=123456"
JAVA_OPTS="$JAVA_OPTS -Dsecret=123456"
JAVA_OPTS="$JAVA_OPTS -DtimeToLive=20000"

JAVA_OPTS="$JAVA_OPTS -DlocalPort=7001"
JAVA_OPTS="$JAVA_OPTS -DclientId=host01"
JPDA_SUSPEND=y
