@echo off

set "JAVA_OPTS=%JAVA_OPTS% -DwinBash=C:/apps/Git/git-bash.exe"
set "JAVA_OPTS=%JAVA_OPTS% -DactivemqServer=failover:tcp://172.16.1.107:61616"
set "JAVA_OPTS=%JAVA_OPTS% -DactivemqChannel=stateChannel"
set "JAVA_OPTS=%JAVA_OPTS% -DdashboardServer=http://172.16.1.107:8080/api"
set "JAVA_OPTS=%JAVA_OPTS% -Dtoken=123456"
set "JAVA_OPTS=%JAVA_OPTS% -Dsecret=123456"
set "JAVA_OPTS=%JAVA_OPTS% -DlocalPort=7001"
set "JAVA_OPTS=%JAVA_OPTS% -DclientId=host01"
set "JAVA_OPTS=%JAVA_OPTS% -DtimeToLive=20000"

set "JPDA_SUSPEND=y"
