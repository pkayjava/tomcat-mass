package com.angkorteam.cicd.host;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.IOUtils;

public class Functions {

	public static String sha512Hex(File file) {
		FileInputStream stream = null;
		try {
			stream = new FileInputStream(file);
			return DigestUtils.sha512Hex(stream);
		} catch (IOException e) {
			return null;
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public static void unTarGz(File targz, File output) throws IOException {
		InputStream fi = null;
		try {
			fi = Files.newInputStream(Paths.get(targz.getAbsolutePath()));
			InputStream bi = new BufferedInputStream(fi);
			InputStream gzi = new GzipCompressorInputStream(bi);
			ArchiveInputStream i = new TarArchiveInputStream(gzi);

			ArchiveEntry entry = null;
			while ((entry = i.getNextEntry()) != null) {
				if (!i.canReadEntryData(entry)) {
					continue;
				}
				String name = new File(output, entry.getName()).getAbsolutePath();
				File f = new File(name);
				if (entry.isDirectory()) {
					if (!f.isDirectory() && !f.mkdirs()) {
					}
				} else {
					File parent = f.getParentFile();
					if (!parent.isDirectory() && !parent.mkdirs()) {
					}
					OutputStream o = null;
					try {
						o = Files.newOutputStream(f.toPath());
						IOUtils.copy(i, o);
						if ("catalina.sh".equals(f.getName())) {
							f.setExecutable(true);
						} else if (f.getName().equals("java")) {
							f.setExecutable(true);
						}
					} finally {
						if (o != null) {
							o.close();
						}
					}
				}
			}
		} finally {
			if (fi != null) {
				fi.close();
			}
		}
	}

}
