package com.angkorteam.cicd.host;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.apache.catalina.LifecycleState;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import com.angkorteam.cicd.xml.command.TomcatStatusCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;

public class ClientSocket implements Runnable {

	private static final Log LOGGER = LogFactory.getLog(ClientSocket.class);

	private final HostListener listener;

	private DataInputStream stream;

	public ClientSocket(HostListener listener, Socket client) {
		try {
			this.stream = new DataInputStream(client.getInputStream());
		} catch (IOException e) {
		}
		this.listener = listener;
	}

	@Override
	public void run() {
		while (true && this.stream != null) {
			try {
				String text = this.stream.readUTF();
				TextMessage message = this.listener.activemqSession.createTextMessage(text);
				LOGGER.info("sent " + message);
				this.listener.activemqProducer.send(message);
				XmlMessage p = XmlMessage.fromText(text);
				if (p != null && p.getCommandClass().equals(TomcatStatusCommand.class.getName())) {
					TomcatStatusCommand command = (TomcatStatusCommand) p.getCommand();
					if (LifecycleState.INITIALIZING.name().equals(command.getStatus())) {
						if (!this.listener.running.contains(command.getInstance())) {
							this.listener.running.add(command.getInstance());
						}
					} else if (LifecycleState.DESTROYED.name().equals(command.getStatus())) {
						this.listener.running.remove(command.getInstance());
					}
				}
			} catch (JMSException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}
	}

}
