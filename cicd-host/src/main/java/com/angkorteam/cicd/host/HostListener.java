package com.angkorteam.cicd.host;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleListener;
import org.apache.catalina.LifecycleState;
import org.apache.catalina.core.StandardServer;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import com.angkorteam.cicd.xml.XmlFile;
import com.angkorteam.cicd.xml.XmlJdk;
import com.angkorteam.cicd.xml.command.HostJdkCommand;
import com.angkorteam.cicd.xml.command.HostStatusCommand;
import com.angkorteam.cicd.xml.command.TomcatFileCommand;
import com.angkorteam.cicd.xml.command.TomcatStatusCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;
import com.angkorteam.cicd.xml.tomcat.XmlConnector;
import com.angkorteam.cicd.xml.tomcat.XmlHost;
import com.angkorteam.cicd.xml.tomcat.XmlListener;
import com.angkorteam.cicd.xml.tomcat.XmlSSLHostConfig;
import com.angkorteam.cicd.xml.tomcat.XmlServer;

import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.Unirest;
import io.github.openunirest.request.GetRequest;
import oshi.SystemInfo;
import oshi.software.os.OperatingSystem;
import oshi.software.os.OperatingSystemVersion;

public class HostListener implements LifecycleListener, Runnable {

	private static final Log LOGGER = LogFactory.getLog(HostListener.class);

	protected File hostHome;
	protected File instanceHome;
	protected File jdkHome;

	protected String clientId;
	protected String winBash;

	protected static final String WIN_START = "win-start.sh";
	protected static final String WIN_DEBUG = "win-debug.sh";
	protected static final String WIN_STOP = "win-stop.sh";

	protected String activemqServer;
	protected String activemqChannel;
	protected String dashboardServer;
	protected int timeToLive = -1;
	protected String token;
	protected String secret;

	protected Map<String, InstanceExecutor> instanceExecutor = new HashMap<String, InstanceExecutor>();

	protected List<String> instance = new ArrayList<String>();

	protected Connection activemqConnection;
	protected Session activemqSession;
	protected MessageConsumer activemqConsumer;
	protected MessageProducer activemqProducer;

	protected int localPort = 5000;
	protected ServerSocket server;
	protected Thread serverThread;
	protected List<String> running = Collections.synchronizedList(new ArrayList<String>());

	protected SystemInfo systemInfo = new SystemInfo();
	protected OperatingSystem operatingSystem = null;

	@Override
	public void lifecycleEvent(LifecycleEvent event) {
		if (event.getSource() != null) {
			if (event.getSource() instanceof StandardServer) {
				StandardServer server = (StandardServer) event.getSource();
				if (event.getLifecycle().getState() == LifecycleState.INITIALIZING) {

					if (SystemUtils.IS_OS_WINDOWS) {
						if (System.getProperty("winBash") != null && !"".equals(System.getProperty("winBash"))) {
							File winBash = new File(FilenameUtils.normalize(System.getProperty("winBash")));
							if (!winBash.exists()) {
								throw new RuntimeException(System.getProperty("winBash")
										+ " is not found, please install https://git-scm.com/download/win");
							}
						}
					}

					try {
						this.operatingSystem = systemInfo.getOperatingSystem();
					} catch (UnsupportedOperationException e) {
					}

					initSystem(server);

					initJdk();

					connectActiveMQ();
					openServerSocket();
					try {
						TextMessage message = this.activemqSession
								.createTextMessage(buildMessage(event.getLifecycle().getStateName()).toText());
						LOGGER.info("sent " + message.getText());
						this.activemqProducer.send(message);
					} catch (JMSException e) {
						LOGGER.info(e.getMessage());
					}
				} else {
					if (this.activemqSession != null) {
						try {
							TextMessage message = this.activemqSession
									.createTextMessage(buildMessage(event.getLifecycle().getStateName()).toText());
							LOGGER.info("sent " + message.getText());
							this.activemqProducer.send(message);
						} catch (JMSException e) {
							LOGGER.info(e.getMessage());
						}
					}
					if (event.getLifecycle().getState() == LifecycleState.STARTED) {
						for (File instanceHome : this.instanceHome.listFiles()) {
							if (instanceHome.isDirectory()) {
								buildInstanceSetenv(instanceHome.getName(), new HashMap<String, String>());
								copyInstanceLibrary(instanceHome.getName());
							}
						}
					} else if (event.getLifecycle().getState() == LifecycleState.DESTROYING) {
						for (File instanceHome : this.instanceHome.listFiles()) {
							if (instanceHome.isDirectory()) {
								if (this.running.contains(instanceHome.getName())) {
									stop(instanceHome.getName());
								}
							}
						}
					} else if (event.getLifecycle().getState() == LifecycleState.DESTROYED) {
						closeServerSocket();
						disconnectActiveMQ();
					}
				}
			}
		}
	}

	protected void initJdk() {
		String server = null;
		if (this.dashboardServer.endsWith("/")) {
			server = this.dashboardServer + "jdk/" + this.clientId;
		} else {
			server = this.dashboardServer + "/jdk/" + this.clientId;
		}
		LOGGER.info("server : " + server);
		GetRequest request = Unirest.get(server).basicAuth(this.token, this.secret);
		HttpResponse<String> response = request.asString();
		LOGGER.info("jdkHome " + this.jdkHome.getAbsolutePath());
		XmlMessage message = XmlMessage.fromText(response.getBody());
		for (File jdk : this.jdkHome.listFiles()) {
			if (jdk.isDirectory()) {
				FileUtils.deleteQuietly(jdk);
				LOGGER.info("deleted " + jdk.getAbsolutePath());
			}
		}
		HostJdkCommand command = (HostJdkCommand) message.getCommand();
		for (XmlJdk jdk : command.getJdk()) {
			String jdkName = jdk.getName();
			LOGGER.info("jdkName " + jdk.getName());
			File jdkFile = new File(this.jdkHome, jdkName + ".tar.gz");
			String sha = null;
			if (jdkFile.isFile()) {
				sha = Functions.sha512Hex(jdkFile);
			}

			File tmpJdk = new File(FileUtils.getTempDirectory(), System.currentTimeMillis() + "");
			tmpJdk.mkdirs();

			if (!jdk.getSha().equals(sha)) {
				FileUtils.deleteQuietly(jdkFile);
				HttpResponse<InputStream> binary = Unirest.get(jdk.getDownload()).basicAuth(this.token, this.secret)
						.asBinary();
				LOGGER.info("download - " + jdk.getName());
				FileOutputStream stream = null;
				try {
					stream = FileUtils.openOutputStream(jdkFile);
					IOUtils.copy(binary.getBody(), stream);
				} catch (IOException e) {
					LOGGER.info(e.getMessage());
				} finally {
					if (stream != null) {
						try {
							stream.close();
						} catch (IOException e) {
						}
					}
				}
			}

			try {
				Functions.unTarGz(jdkFile, tmpJdk);
			} catch (IOException e) {
				LOGGER.info(e.getMessage());
			}
			File j = tmpJdk.listFiles()[0];
			try {
				FileUtils.moveDirectory(j, new File(this.jdkHome, jdkName));
			} catch (IOException e) {
				LOGGER.info(e.getMessage());
			}

			FileUtils.deleteQuietly(tmpJdk);
		}
	}

	protected void initSystem(StandardServer server) {
		String localPort = System.getProperty("localPort");
		this.localPort = StringUtils.isNotEmpty(localPort) ? Integer.valueOf(localPort) : this.localPort;
		LOGGER.info("localPort " + this.localPort);
		this.hostHome = server.getCatalinaHome();
		this.instanceHome = new File(this.hostHome, "instance");
		this.instanceHome.mkdirs();
		this.jdkHome = new File(this.hostHome, "jdk");
		this.jdkHome.mkdirs();
		this.clientId = System.getProperty("clientId");
		this.winBash = System.getProperty("winBash");
		if (SystemUtils.IS_OS_WINDOWS) {
			LOGGER.info("win-bash " + this.winBash);
		}

		this.activemqServer = System.getProperty("activemqServer");
		this.activemqChannel = System.getProperty("activemqChannel");
		this.timeToLive = Integer.valueOf(System.getProperty("timeToLive"));
		LOGGER.info("collector channel " + this.activemqChannel);
		this.dashboardServer = System.getProperty("dashboardServer");
		this.token = System.getProperty("token");
		this.secret = System.getProperty("secret");
	}

	protected void openServerSocket() {
		try {
			this.server = new ServerSocket();
			InetSocketAddress address = new InetSocketAddress("localhost", this.localPort);
			this.server.bind(address);
			this.serverThread = new Thread(this);
			this.serverThread.start();
		} catch (IOException e) {
			LOGGER.info(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	protected void closeServerSocket() {
		try {
			if (this.server != null) {
				this.server.close();
			}
		} catch (IOException e) {
			LOGGER.info(e.getMessage());
		}
		if (this.serverThread != null && this.serverThread.isAlive()) {
			this.serverThread.interrupt();
		}
	}

	@Override
	public void run() {
		while (true) {
			Socket socket = null;
			try {
				socket = this.server.accept();
			} catch (IOException e) {
				LOGGER.info(e.getMessage());
				return;
			}
			if (socket != null) {
				new Thread(new ClientSocket(this, socket)).start();
			}
		}
	}

	protected void disconnectActiveMQ() {
		try {
			if (this.activemqProducer != null) {
				this.activemqProducer.close();
			}
		} catch (JMSException e) {
			LOGGER.info(e.getMessage());
		}
		try {
			if (this.activemqConsumer != null) {
				this.activemqConsumer.close();
			}
		} catch (JMSException e) {
			LOGGER.info(e.getMessage());
		}
		try {
			if (this.activemqSession != null) {
				this.activemqSession.close();
			}
		} catch (JMSException e) {
			LOGGER.info(e.getMessage());
		}
		try {
			if (this.activemqConnection != null) {
				this.activemqConnection.close();
			}
		} catch (JMSException e) {
			LOGGER.info(e.getMessage());
		}
	}

	protected void connectActiveMQ() {

		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(this.activemqServer);
		connectionFactory.setClientID(this.clientId);

		try {
			// Create a Connection
			this.activemqConnection = connectionFactory.createConnection(this.token, this.secret);
			this.activemqConnection.start();

			// Create a Session
			this.activemqSession = this.activemqConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			this.activemqConsumer = this.activemqSession
					.createConsumer(this.activemqSession.createQueue(this.clientId));
			this.activemqConsumer.setMessageListener(new MessageListener(this));

			Queue queue = this.activemqSession.createQueue(this.activemqChannel);
			this.activemqProducer = this.activemqSession.createProducer(queue);
			this.activemqProducer.setTimeToLive(this.timeToLive);
		} catch (JMSException e) {
			LOGGER.info(e.getMessage());
			throw new RuntimeException(e);
		}

	}

	protected Map<String, String> buildInstanceServer(String instance, String server) {

		Map<String, String> params = new HashMap<String, String>();

		// Download Tomcat File
		File instanceHome = new File(this.instanceHome, instance);
		instanceHome.mkdirs();

		File webapps = new File(instanceHome, "webapps");
		webapps.mkdirs();
		for (File vhost : webapps.listFiles()) {
			if (vhost.isDirectory()) {
				for (File war : vhost.listFiles()) {
					if (war.isDirectory()) {
						FileUtils.deleteQuietly(war);
					}
				}
			}
		}

		Map<String, File> delete = new HashMap<String, File>();
		for (File f : FileUtils.listFiles(instanceHome, null, true)) {
			String key = f.getAbsolutePath().substring(instanceHome.getAbsolutePath().length());
			delete.put(FilenameUtils.normalize(key, true), f);
		}

		GetRequest request = Unirest.get(server).basicAuth(this.token, this.secret);
		HttpResponse<String> response = request.asString();
		XmlMessage message = XmlMessage.fromText(response.getBody());

		List<String> wars = new ArrayList<String>();

		boolean window = SystemUtils.IS_OS_WINDOWS;
		window = false;

		// Download Tomcat File
		TomcatFileCommand command = (TomcatFileCommand) message.getCommand();
		if (command.getJdkName() != null && !"".equals(command.getJdkName())) {
			if (window) {
				params.put("JAVA_HOME", "set \"JAVA_HOME='"
						+ FilenameUtils.normalize(new File(this.jdkHome, command.getJdkName()).getAbsolutePath(), true)
						+ "'\"");
			} else {
				if (SystemUtils.IS_OS_WINDOWS) {
					params.put("JAVA_HOME",
							"JAVA_HOME=\""
									+ FilenameUtils.normalize(
											new File(this.jdkHome, command.getJdkName()).getAbsolutePath(), true)
									+ "\"");
				} else {
					params.put("JAVA_HOME",
							"JAVA_HOME='"
									+ FilenameUtils.normalize(
											new File(this.jdkHome, command.getJdkName()).getAbsolutePath(), true)
									+ "'");
				}
			}
		}
		XmlFile serverXml = null;
		for (XmlFile file : command.getFiles()) {
			delete.remove(file.getPath());
			if (StringUtils.upperCase(file.getPath()).endsWith(".WAR")) {
				wars.add(file.getPath());
			}
			if (file.getPath().equals("/conf/server.xml")) {
				serverXml = file;
				continue;
			}
			File tomcatFile = new File(instanceHome, file.getPath().substring(1));
			tomcatFile.getParentFile().mkdirs();
			String sha = null;
			if (tomcatFile.exists() && tomcatFile.isFile()) {
				sha = Functions.sha512Hex(tomcatFile);
			}
			if (!file.getSha().equals(sha)) {
				HttpResponse<InputStream> binary = Unirest.get(file.getDownload()).basicAuth(this.token, this.secret)
						.asBinary();
				LOGGER.info(instance + " : download - " + file.getPath());
				FileUtils.deleteQuietly(tomcatFile);
				FileOutputStream stream = null;
				try {
					stream = FileUtils.openOutputStream(tomcatFile);
					IOUtils.copy(binary.getBody(), stream);
				} catch (IOException e) {
					LOGGER.info(e.getMessage());
				} finally {
					if (stream != null) {
						try {
							stream.close();
						} catch (IOException e) {
						}
					}
				}
			}
		}

		List<String> lines = new ArrayList<>();
		File releaseNotes = new File(instanceHome, "RELEASE-NOTES");
		String tomcatVersion = "N/A";
		try {
			lines = FileUtils.readLines(releaseNotes, "UTF-8");
		} catch (IOException e) {
			LOGGER.info(e.getMessage());
		}
		for (String line : lines) {
			int i = line.indexOf("Apache Tomcat Version ");
			if (i >= 0) {
				tomcatVersion = line.substring(i + "Apache Tomcat Version ".length());
			}
		}

		// Re-Configure Tomcat File (Server.xml)
		if (serverXml != null) {
			String sha = null;
			File tomcatFile = new File(instanceHome, serverXml.getPath().substring(1));
			if (tomcatFile.exists() && tomcatFile.isFile()) {
				sha = Functions.sha512Hex(tomcatFile);
			}
			if (!serverXml.getSha().equals(sha)) {
				HttpResponse<String> binary = Unirest.get(serverXml.getDownload()).basicAuth(this.token, this.secret)
						.asString();
				LOGGER.info(instance + " : " + serverXml.getPath() + " - " + serverXml.getDownload());
				XmlServer xmlServer = XmlServer.fromText(binary.getBody());
				XmlListener xmlListener = new XmlListener();
				if (tomcatVersion.startsWith("7.0")) {
					xmlListener.setClassName(com.angkorteam.cicd.tomcat70.InstanceListener.class.getName());
				} else if (tomcatVersion.startsWith("8.0")) {
					xmlListener.setClassName(com.angkorteam.cicd.tomcat80.InstanceListener.class.getName());
				} else if (tomcatVersion.startsWith("8.5")) {
					xmlListener.setClassName(com.angkorteam.cicd.tomcat85.InstanceListener.class.getName());
				} else if (tomcatVersion.startsWith("9.0")) {
					xmlListener.setClassName(com.angkorteam.cicd.tomcat90.InstanceListener.class.getName());
				} else {
					throw new RuntimeException("Unsupprted tomcat " + tomcatVersion);
				}
				xmlServer.getListener().add(xmlListener);
				if (xmlServer.getService() != null) {
					for (XmlConnector connector : xmlServer.getService().getConnectors()) {
						for (XmlSSLHostConfig sslHostConfig : connector.getSslHostConfigs()) {
							if (sslHostConfig.getCaCertificateFile() != null
									&& !"".equals(sslHostConfig.getCaCertificateFile())) {
								sslHostConfig.setCaCertificateFile(sslHostConfig.getCaCertificateFile().substring(1));
							}
							if (sslHostConfig.getTruststoreFile() != null
									&& !"".equals(sslHostConfig.getTruststoreFile())) {
								sslHostConfig.setTruststoreFile(sslHostConfig.getTruststoreFile().substring(1));
							}
							if (sslHostConfig.getCertificate() != null) {
								if (sslHostConfig.getCertificate().getCertificateChainFile() != null
										&& !"".equals(sslHostConfig.getCertificate().getCertificateChainFile())) {
									sslHostConfig.getCertificate().setCertificateChainFile(
											sslHostConfig.getCertificate().getCertificateChainFile().substring(1));
								}
								if (sslHostConfig.getCertificate().getCertificateFile() != null
										&& !"".equals(sslHostConfig.getCertificate().getCertificateFile())) {
									sslHostConfig.getCertificate().setCertificateFile(
											sslHostConfig.getCertificate().getCertificateFile().substring(1));
								}
								if (sslHostConfig.getCertificate().getCertificateKeyFile() != null
										&& !"".equals(sslHostConfig.getCertificate().getCertificateKeyFile())) {
									sslHostConfig.getCertificate().setCertificateKeyFile(
											sslHostConfig.getCertificate().getCertificateKeyFile().substring(1));
								}
								if (sslHostConfig.getCertificate().getCertificateKeystoreFile() != null
										&& !"".equals(sslHostConfig.getCertificate().getCertificateKeystoreFile())) {
									sslHostConfig.getCertificate().setCertificateKeystoreFile(
											sslHostConfig.getCertificate().getCertificateKeystoreFile().substring(1));
								}
							}
						}
					}
					if (xmlServer.getService().getEngine() != null) {
						for (XmlHost host : xmlServer.getService().getEngine().getHosts()) {
							host.setAppBase("webapps/" + host.getName());
							// XmlValve valve = new XmlValve();
							// valve.setClassName(InstanceValveBase.class.getName());
							// host.getValves().add(valve);
						}
					}
				}
				try {
					FileUtils.writeStringToFile(tomcatFile, xmlServer.toText(), "UTF-8");
				} catch (IOException e) {
					LOGGER.info(e.getMessage());
				}
			}
		}

		// Delete
		for (Entry<String, File> f : delete.entrySet()) {
			if (f.getKey().startsWith("/logs")) {
				continue;
			}
			FileUtils.deleteQuietly(f.getValue());
			LOGGER.info(instance + " : deleted - " + f.getKey());
		}
		File temp = new File(instanceHome, "temp");
		temp.mkdirs();

		return params;
	}

	protected void buildInstanceSetenv(String instance, Map<String, String> params) {
		buildInstanceSetenv(false, instance, params);
	}

	protected void buildInstanceSetenv(boolean debug, String instance, Map<String, String> params) {
		File instanceHome = new File(this.instanceHome, instance);
		File setenv = null;

		boolean window = SystemUtils.IS_OS_WINDOWS;
		window = false;
		if (window) {
			setenv = new File(instanceHome, "/bin/setenv.bat");
		} else {
			setenv = new File(instanceHome, "/bin/setenv.sh");
		}

		List<String> lines = null;
		if (setenv.exists()) {
			try {
				lines = FileUtils.readLines(setenv, "UTF-8");
			} catch (IOException e) {
				LOGGER.info(e.getMessage());
			}
		} else {
			lines = new ArrayList<String>();

			if (window) {
				lines.add("@echo off");
			} else {
				lines.add("#!/bin/sh");
			}

			lines.add("");
		}

		if (window) {
			params.put("-DlocalPort=",
					"set \"JAVA_OPTS=%JAVA_OPTS% -DlocalPort=" + String.valueOf(this.localPort) + "\"");
			params.put("-DclientId=", "set \"JAVA_OPTS=%JAVA_OPTS% -DclientId=" + this.clientId + "\"");
			if (debug) {
				params.put("JPDA_SUSPEND", "set \"JPDA_SUSPEND=y\"");
				params.put("JPDA_ADDRESS", "set \"JPDA_ADDRESS=localhost:8000\"");
			}
		} else {
			params.put("-DlocalPort=",
					String.format("JAVA_OPTS=\"$JAVA_OPTS -DlocalPort=%s\"", String.valueOf(this.localPort)));
			params.put("-DclientId=", String.format("JAVA_OPTS=\"$JAVA_OPTS -DclientId=%s\"", this.clientId));
			if (debug) {
				params.put("JPDA_SUSPEND", "JPDA_SUSPEND=y");
				params.put("JPDA_ADDRESS", "JPDA_ADDRESS=localhost:8000");
			}
		}

		window = SystemUtils.IS_OS_WINDOWS;

		if (!window) {
			params.put("URANDOM",
					String.format("JAVA_OPTS=\"$JAVA_OPTS -Djava.security.egd=%s\"", "file:/dev/./urandom"));
		}

		// replaced
		List<String> newLines = new ArrayList<String>();
		for (String line : lines) {
			boolean replaced = false;
			String key = null;
			for (Entry<String, String> param : params.entrySet()) {
				if (line.contains(param.getKey())) {
					newLines.add(param.getValue());
					key = param.getKey();
					replaced = true;
					break;
				}
			}
			if (!replaced) {
				newLines.add(line);
			} else {
				params.remove(key);
			}
		}

		// added
		for (Entry<String, String> param : params.entrySet()) {
			newLines.add(param.getValue());
		}

		try {
			FileUtils.writeLines(setenv, newLines);
		} catch (IOException e) {
			LOGGER.info(e.getMessage());
			e.printStackTrace();
		}
	}

	protected void start(String instance, String server) {
		Map<String, String> params = buildInstanceServer(instance, server);
		buildInstanceSetenv(instance, params);
		copyInstanceLibrary(instance);
		File instanceHome = new File(this.instanceHome, instance);
		if (SystemUtils.IS_OS_WINDOWS) {
			String tomcat = FilenameUtils.normalize(instanceHome.getAbsolutePath(), true);
			{
				// start script wrapper
				List<String> lines = new ArrayList<>();
				lines.add("#!/bin/sh");
				lines.add("cd " + tomcat);
				lines.add("./bin/catalina.sh run");
				try {
					FileUtils.writeLines(new File(instanceHome, WIN_START), lines);
				} catch (IOException e) {
					LOGGER.info(e.getMessage());
				}
			}
			{
				// debug script wrapper
				List<String> lines = new ArrayList<>();
				lines.add("#!/bin/sh");
				lines.add("cd " + tomcat);
				lines.add("./bin/catalina.sh jpda run");
				try {
					FileUtils.writeLines(new File(instanceHome, WIN_DEBUG), lines);
				} catch (IOException e) {
					LOGGER.info(e.getMessage());
				}
			}
			{
				// stop script wrapper
				List<String> lines = new ArrayList<>();
				lines.add("#!/bin/sh");
				lines.add("cd " + tomcat);
				lines.add("./bin/catalina.sh stop");
				try {
					FileUtils.writeLines(new File(instanceHome, WIN_STOP), lines);
				} catch (IOException e) {
					LOGGER.info(e.getMessage());
				}
			}
		}
		try {
			File serverXmlFile = new File(instanceHome, "conf/server.xml");
			if (serverXmlFile.exists()) {
				if (instanceHome.exists() && instanceHome.isDirectory()) {
					CommandLine cmdLine = null;
					String command = null;
					if (SystemUtils.IS_OS_WINDOWS) {
						command = FilenameUtils.normalize(this.winBash) + " " + FilenameUtils
								.normalize(new File(instanceHome, "bin/catalina.sh run").getAbsolutePath());
						cmdLine = CommandLine.parse(command);
					} else {
						command = "sh bin/catalina.sh run";
						cmdLine = CommandLine.parse(command);
					}
					LOGGER.info(":/>" + command);
					InstanceExecutor executor = new InstanceExecutor();
					executor.setWorkingDirectory(instanceHome);
					executor.execute(cmdLine, new DefaultExecuteResultHandler());
					LOGGER.info("executed " + command);
					this.instanceExecutor.put(instance, executor);
				}
			}
		} catch (IOException e) {
			LOGGER.info(e.getMessage());
		}
	}

	protected void debug(String instance, String server) {
		Map<String, String> params = buildInstanceServer(instance, server);
		buildInstanceSetenv(true, instance, params);
		copyInstanceLibrary(instance);
		try {
			File instanceHome = new File(this.instanceHome, instance);
			File serverXmlFile = new File(instanceHome, "conf/server.xml");
			if (serverXmlFile.exists()) {
				if (instanceHome.exists() && instanceHome.isDirectory()) {

					String command = null;
					if (SystemUtils.IS_OS_WINDOWS) {
						command = FilenameUtils.normalize(this.winBash) + " " + FilenameUtils
								.normalize(new File(instanceHome, "bin/catalina.sh jpda run").getAbsolutePath());
					} else {
						command = "sh bin/catalina.sh jpda run";
					}
					LOGGER.info(":/>" + command);
					CommandLine cmdLine = CommandLine.parse(command);
					InstanceExecutor executor = new InstanceExecutor();
					executor.setWorkingDirectory(instanceHome);
					executor.execute(cmdLine, new DefaultExecuteResultHandler());
					LOGGER.info("executed " + command);
					this.instanceExecutor.put(instance, executor);
				}
			}
		} catch (IOException e) {
			LOGGER.info(e.getMessage());
		}
	}

	protected void copyInstanceLibrary(String instance) {
		File instanceHome = new File(this.instanceHome, instance);
		File libraries = new File(this.hostHome, "lib");
		for (File library : libraries.listFiles()) {
			String name = library.getName();
			if (name.startsWith("cicd-xml") || name.startsWith("cicd-host") || name.startsWith("cicd-tomcat")) {
				try {
					FileUtils.copyFile(library, new File(new File(instanceHome, "lib"), library.getName()));
				} catch (IOException e) {
					LOGGER.info(e.getMessage());
				}
			}
		}
	}

	protected void stop(String instance) {
		if (SystemUtils.IS_OS_WINDOWS) {
			InstanceExecutor originExecutor = this.instanceExecutor.get(instance);
			this.instanceExecutor.remove(instance);
			File instanceHome = new File(this.instanceHome, instance);
			if (instanceHome.exists() && instanceHome.isDirectory()) {
				String command = null;
				command = FilenameUtils.normalize(this.winBash) + " "
						+ FilenameUtils.normalize(new File(instanceHome, "bin/catalina.sh stop").getAbsolutePath());
				LOGGER.info(":/>" + command);
				CommandLine cmdLine = CommandLine.parse(command);
				InstanceExecutor executor = new InstanceExecutor();
				executor.setWorkingDirectory(instanceHome);
				try {
					executor.execute(cmdLine, new DefaultExecuteResultHandler());
					LOGGER.info("executed " + command);
				} catch (IOException e) {
					LOGGER.info(e.getMessage());
				}
			}
			if (originExecutor != null) {
				originExecutor.getProcess().destroy();
			}
			this.running.remove(instance);
		} else {
			InstanceExecutor originExecutor = this.instanceExecutor.get(instance);
			if (originExecutor != null) {
				originExecutor.getProcess().destroy();
				this.instanceExecutor.remove(instance);
			} else {
				File instanceHome = new File(this.instanceHome, instance);
				if (instanceHome.exists() && instanceHome.isDirectory()) {
					String command = "sh bin/catalina.sh stop";
					LOGGER.info(":/>" + command);
					CommandLine cmdLine = CommandLine.parse(command);
					InstanceExecutor executor = new InstanceExecutor();
					executor.setWorkingDirectory(instanceHome);
					try {
						executor.execute(cmdLine);
						LOGGER.info("executed " + command);
					} catch (IOException e) {
						LOGGER.info(e.getMessage());
					}
				}
			}
			long time = 0;
			while (time <= 1000 * 60 && this.running.contains(instance)) {
				try {
					Thread.sleep(100);
					time = time + 100;
				} catch (InterruptedException e) {
					LOGGER.info(e.getMessage());
				}
			}
			if (this.running.contains(instance)) {
				this.running.remove(instance);
				originExecutor.getProcess().destroyForcibly();
			}
		}
	}

	protected void kill(String instance) {
		if (!SystemUtils.IS_OS_WINDOWS) {
			if (this.instanceExecutor.get(instance) != null) {
				if (this.instanceExecutor.get(instance).getProcess() != null
						&& this.instanceExecutor.get(instance).getProcess().isAlive()) {
					this.instanceExecutor.get(instance).getProcess().destroyForcibly();
				}
				this.instanceExecutor.remove(instance);
			}
			this.running.remove(instance);
		} else {
			stop(instance);
		}
		TomcatStatusCommand command = new TomcatStatusCommand();
		command.setClientId(this.clientId);
		command.setInstance(instance);
		command.setStatus(LifecycleState.DESTROYED.name());
		try {
			TextMessage message = this.activemqSession.createTextMessage(new XmlMessage(command).toText());
			LOGGER.info("sent " + message.getText());
			this.activemqProducer.send(message);
		} catch (JMSException e) {
			LOGGER.info(e.getMessage());
		}
	}

	protected void restart(String instance, String server) {
		stop(instance);
		if (SystemUtils.IS_OS_WINDOWS) {
			if (SystemUtils.IS_OS_WINDOWS) {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					LOGGER.info(e.getMessage());
				}
			}
		}
		start(instance, server);
	}

	protected static int getProcessId(Process process) {
		int processId = -1;
		if (process != null && process.isAlive()) {
			if (process.getClass().getName().equals("java.lang.UNIXProcess")) {
				/* get the PID on unix/linux systems */
				try {
					Field field = process.getClass().getDeclaredField("pid");
					field.setAccessible(true);
					processId = field.getInt(process);
				} catch (NoSuchFieldException e) {
					LOGGER.info(e.getMessage());
				} catch (SecurityException e) {
					LOGGER.info(e.getMessage());
				} catch (IllegalArgumentException e) {
					LOGGER.info(e.getMessage());
				} catch (IllegalAccessException e) {
					LOGGER.info(e.getMessage());
				}
			}
		}
		return processId;
	}

	protected XmlMessage buildMessage(String event) {
		HostStatusCommand command = new HostStatusCommand();
		command.setClientId(this.clientId);
		command.setStatus(event);
		if (LifecycleState.STARTING.name().equals(event)) {
			command.setLocalPort(this.localPort);
			if (this.operatingSystem != null) {
				OperatingSystemVersion version = this.operatingSystem.getVersion();
				command.setOsVersion(version.getVersion() + " - " + version.getBuildNumber());
				command.setOsName(this.operatingSystem.getFamily());
			} else {
				command.setOsVersion(System.getProperty("os.version"));
				command.setOsName(System.getProperty("os.name"));
			}
			command.setOsArch(System.getProperty("os.arch"));
			command.setJvmVendor(System.getProperty("java.vm.vendor"));
			command.setJvmVersion(System.getProperty("java.runtime.version"));
			command.setHostHome(this.hostHome.getAbsolutePath());
			// command.getSsl().add(buildJsseSSL(true, JSSEImplementation.class.getName()));
			// try {
			// command.getSsl().add(buildJsseSSL(false,
			// JsseSSLImplementation.class.getName()));
			// } catch (Throwable e) {
			// LOGGER.info(e.getMessage());
			// }
			// try {
			// command.getSsl().add(buildOpenSSL(OpenSSLImplementation.class.getName()));
			// } catch (Throwable e) {
			// LOGGER.info(e.getMessage());
			// }
			// try {
			// command.getSsl().add(buildOpenSSL(com.angkorteam.cicd.tomcat.extension.openssl.OpenSSLImplementation.class.getName()));
			// } catch (Throwable e) {
			// LOGGER.info(e.getMessage());
			// }
			// try {
			// command.getSsl().add(buildGoogleSSL(GoogleSSLImplementation.class.getName()));
			// } catch (Throwable e) {
			// LOGGER.info(e.getMessage());
			// }
			// try {
			// command.getSsl().add(buildBouncySSL(BouncySSLImplementation.class.getName()));
			// } catch (Throwable e) {
			// LOGGER.info(e.getMessage());
			// }
		}
		return new XmlMessage(command);
	}

	// protected XmlSsl buildJsseSSL(boolean selected, String clazz) {
	// XmlSsl ssl = new XmlSsl();
	// ssl.setImplementation(clazz);
	// ssl.setSelected(selected);
	// for (String cipher : JsseUtil.CIPHERS) {
	// ssl.getCipher().add(cipher);
	// }
	// for (String protocol : JsseUtil.PROTOCOLS) {
	// ssl.getProtocol().add(protocol);
	// }
	// ssl.setType("JDK");
	// return ssl;
	// }

	// protected XmlSsl buildGoogleSSL(String clazz) {
	// XmlSsl ssl = new XmlSsl();
	// ssl.setImplementation(clazz);
	// for (String cipher : GoogleSSLUtil.CIPHERS) {
	// ssl.getCipher().add(cipher);
	// }
	// for (String protocol : GoogleSSLUtil.PROTOCOLS) {
	// ssl.getProtocol().add(protocol);
	// }
	// ssl.setType("OpenSSL");
	// return ssl;
	// }

	// protected XmlSsl buildBouncySSL(String clazz) {
	// XmlSsl ssl = new XmlSsl();
	// ssl.setImplementation(clazz);
	// for (String cipher : BouncySSLUtil.CIPHERS) {
	// ssl.getCipher().add(cipher);
	// }
	// for (String protocol : BouncySSLUtil.PROTOCOLS) {
	// ssl.getProtocol().add(protocol);
	// }
	// ssl.setType("OpenSSL");
	// return ssl;
	// }

	// protected XmlSsl buildOpenSSL(String clazz) {
	// XmlSsl ssl = new XmlSsl();
	// ssl.setImplementation(clazz);
	// for (String cipher : OpenSSLEngine.AVAILABLE_CIPHER_SUITES) {
	// ssl.getCipher().add(cipher);
	// }
	// for (String protocol : OpenSSLEngine.IMPLEMENTED_PROTOCOLS_SET) {
	// ssl.getProtocol().add(protocol);
	// }
	// ssl.setType("OpenSSL");
	// return ssl;
	// }

}
