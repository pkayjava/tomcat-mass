package com.angkorteam.cicd.host;

import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import com.angkorteam.cicd.xml.command.DebugCommand;
import com.angkorteam.cicd.xml.command.DeployTomcatCommand;
import com.angkorteam.cicd.xml.command.KillCommand;
import com.angkorteam.cicd.xml.command.RestartCommand;
import com.angkorteam.cicd.xml.command.RestartJdkCommand;
import com.angkorteam.cicd.xml.command.StartCommand;
import com.angkorteam.cicd.xml.command.StopCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;

public class MessageListener implements javax.jms.MessageListener {

	private final HostListener listener;

	private static final Log LOGGER = LogFactory.getLog(MessageListener.class);

	public MessageListener(HostListener listener) {
		this.listener = listener;
	}

	@Override
	public void onMessage(Message message) {
		if (message instanceof TextMessage) {
			onTextMessage((TextMessage) message);
		}
	}

	public void onTextMessage(TextMessage message) {
		try {
			XmlMessage json = XmlMessage.fromText(message.getText());
			LOGGER.info(json.getCommandClass());
			if (StartCommand.class.getName().equals(json.getCommandClass())) {
				StartCommand command = (StartCommand) json.getCommand();
				LOGGER.info(String.format("instance %s", command.getInstance()));
				LOGGER.info(String.format("server %s", command.getServer()));
				this.listener.start(command.getInstance(), command.getServer());
			} else if (DebugCommand.class.getName().equals(json.getCommandClass())) {
				DebugCommand command = (DebugCommand) json.getCommand();
				LOGGER.info(String.format("instance %s", command.getInstance()));
				LOGGER.info(String.format("server %s", command.getServer()));
				this.listener.debug(command.getInstance(), command.getServer());
			} else if (StopCommand.class.getName().equals(json.getCommandClass())) {
				StopCommand command = (StopCommand) json.getCommand();
				LOGGER.info(String.format("instance %s", command.getInstance()));
				this.listener.stop(command.getInstance());
			} else if (KillCommand.class.getName().equals(json.getCommandClass())) {
				KillCommand command = (KillCommand) json.getCommand();
				LOGGER.info(String.format("instance %s", command.getInstance()));
				this.listener.kill(command.getInstance());
			} else if (RestartCommand.class.getName().equals(json.getCommandClass())) {
				RestartCommand command = (RestartCommand) json.getCommand();
				LOGGER.info(String.format("instance %s", command.getInstance()));
				this.listener.restart(command.getInstance(), command.getServer());
			} else if (DeployTomcatCommand.class.getName().equals(json.getCommandClass())) {
				DeployTomcatCommand command = (DeployTomcatCommand) json.getCommand();
				this.listener.start(command.getInstance(), command.getServer());
			} else if (RestartJdkCommand.class.getName().equals(json.getCommandClass())) {
				this.listener.initJdk();
			}

		} catch (Throwable e) {
			LOGGER.info(e.getMessage());
			e.printStackTrace();
		}
	}

}
