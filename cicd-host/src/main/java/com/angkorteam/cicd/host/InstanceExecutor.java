package com.angkorteam.cicd.host;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;

public class InstanceExecutor extends DefaultExecutor {

	protected Process process;

	@Override
	protected Process launch(CommandLine command, Map<String, String> env, File dir) throws IOException {
		this.process = super.launch(command, env, dir);
		return this.process;
	}

	public Process getProcess() {
		return process;
	}

}
