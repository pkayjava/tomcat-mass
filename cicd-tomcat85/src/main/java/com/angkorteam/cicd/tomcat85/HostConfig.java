package com.angkorteam.cicd.tomcat85;

import java.io.File;
import java.io.IOException;

import org.apache.catalina.util.ContextName;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import com.angkorteam.cicd.xml.command.ApplicationCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;

public class HostConfig extends org.apache.catalina.startup.HostConfig {

	private static final Log LOGGER = LogFactory.getLog(HostConfig.class);

	protected final InstanceListener listener;

	public HostConfig(InstanceListener listener) {
		super();
		this.listener = listener;
	}

	@Override
	protected void deployDirectory(ContextName cn, File dir) {
		super.deployDirectory(cn, dir);
		try {
			this.listener.stream.writeUTF(buildMessage("Directory", cn, dir).toText());
		} catch (IOException e) {
			LOGGER.info(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void deployDescriptor(ContextName cn, File contextXml) {
		super.deployDescriptor(cn, contextXml);
		try {
			this.listener.stream.writeUTF(buildMessage("Descriptor", cn, contextXml).toText());
		} catch (IOException e) {
			LOGGER.info(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void deployWAR(ContextName cn, File war) {
		super.deployWAR(cn, war);
		try {
			this.listener.stream.writeUTF(buildMessage("WAR", cn, war).toText());
		} catch (IOException e) {
			LOGGER.info(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	protected XmlMessage buildMessage(String type, ContextName cn, File resource) {
		String instanceHome = this.listener.instanceHome.getAbsolutePath();
		ApplicationCommand command = new ApplicationCommand();
		command.setClientId(this.listener.clientId);
		command.setInstance(this.listener.instanceHome.getName());
		command.setResource(resource.getAbsolutePath().substring(instanceHome.length()));
		command.setStatus("Started");
		command.setType(type);
		return new XmlMessage(command);
	}

}
