package com.angkorteam.cicd.tomcat85;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;

import org.apache.catalina.Container;
import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleListener;
import org.apache.catalina.LifecycleState;
import org.apache.catalina.Service;
import org.apache.catalina.core.StandardEngine;
import org.apache.catalina.core.StandardHost;
import org.apache.catalina.core.StandardServer;
import org.apache.catalina.core.StandardService;
import org.apache.catalina.util.ServerInfo;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import com.angkorteam.cicd.xml.command.TomcatStatusCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;

public class InstanceListener implements LifecycleListener {

	private static final Log LOGGER = LogFactory.getLog(InstanceListener.class);

	protected File instanceHome;
	protected File serverFile;
	protected String clientId;

	protected Socket socket;
	protected int localPort = 5000;
	protected DataOutputStream stream;

	@Override
	public void lifecycleEvent(LifecycleEvent event) {
		if (event.getSource() != null) {
			if (event.getSource() instanceof StandardServer) {
				StandardServer server = (StandardServer) event.getSource();
				if (event.getLifecycle().getState() == LifecycleState.INITIALIZING) {
					initSystem(server);
					connectServerSocker();
					try {
						this.stream.writeUTF(buildMessage(event.getLifecycle().getStateName()).toText());
						this.stream.flush();
					} catch (IOException e) {
						LOGGER.info(e.getMessage());
						throw new RuntimeException(e);
					}
					if (serverFile.exists()) {
						initStandardServer(server);
					}
				} else {
					try {
						this.stream.writeUTF(buildMessage(event.getLifecycle().getStateName()).toText());
						this.stream.flush();
					} catch (IOException e) {
						LOGGER.info(e.getMessage());
						throw new RuntimeException(e);
					}
					if (event.getLifecycle().getState() == LifecycleState.DESTROYED) {
						disconnectServerSocker();
					}
				}
			} else if (event.getSource() instanceof StandardService) {
				StandardService service = (StandardService) event.getSource();
				if (event.getLifecycle().getState() == LifecycleState.INITIALIZING) {
					initStandardService(service);
				}
			} else if (event.getSource() instanceof StandardEngine) {
				if (event.getLifecycle().getState() == LifecycleState.INITIALIZING) {
					initStandardEngine((StandardEngine) event.getSource());
				}
			}
		}
	}

	protected void initSystem(StandardServer server) {
		this.instanceHome = server.getCatalinaHome();
		this.serverFile = new File(this.instanceHome, "conf/server.xml");
		String localPort = System.getProperty("localPort");
		this.clientId = System.getProperty("clientId");
		this.localPort = localPort != null && !"".equals(localPort) ? Integer.valueOf(localPort) : this.localPort;
		LOGGER.info("localPort " + this.localPort);
	}

	protected void connectServerSocker() {
		if (this.socket != null) {
			try {
				this.socket.close();
			} catch (IOException e) {
				LOGGER.info(e.getMessage());
				e.printStackTrace();
			}
		}
		try {
			this.socket = new Socket("localhost", this.localPort);
			this.stream = new DataOutputStream(this.socket.getOutputStream());
		} catch (IOException e) {
			LOGGER.info(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	protected void disconnectServerSocker() {
		if (this.socket != null) {
			try {
				this.socket.close();
			} catch (IOException e) {
				LOGGER.info(e.getMessage());
				e.printStackTrace();
			}
			this.socket = null;
		}
		if (this.stream != null) {
			try {
				this.stream.close();
			} catch (IOException e) {
				LOGGER.info(e.getMessage());
				e.printStackTrace();
			}
			this.stream = null;
		}
	}

	protected XmlMessage buildMessage(String event) {
		TomcatStatusCommand command = new TomcatStatusCommand();
		command.setClientId(this.clientId);
		command.setInstance(this.instanceHome.getName());
		command.setStatus(event);
		if ("INITIALIZING".equals(event)) {
			command.setTomcatVersion(ServerInfo.getServerNumber());
			command.setJvmVendor(System.getProperty("java.vm.vendor"));
			command.setJvmVersion(System.getProperty("java.runtime.version"));
		}
		return new XmlMessage(command);
	}

	protected void initStandardEngine(StandardEngine engine) {
		for (Container container : engine.findChildren()) {
			if (container instanceof StandardHost) {
				LifecycleListener[] cycles = container.findLifecycleListeners();
				if (cycles != null) {
					for (LifecycleListener cycle : cycles) {
						container.removeLifecycleListener(cycle);
					}
				}
				HostConfig hostConfig = new HostConfig(this);
				container.addLifecycleListener(hostConfig);
			}
		}
	}

	protected void initStandardService(StandardService service) {
		service.getContainer().addLifecycleListener(this);
	}

	protected void initStandardServer(StandardServer server) {
		for (Service service : server.findServices()) {
			service.addLifecycleListener(this);
		}
	}

}
