DROP TABLE IF EXISTS `tbl_host_cipher`;
CREATE TABLE `tbl_host_cipher` (
  `id`                  VARCHAR(100) NOT NULL,
  `host_ssl_id`         VARCHAR(100) NOT NULL,
  `host_id`             VARCHAR(100) NOT NULL,
  `name`                VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX tbl_host_cipher_001 ON tbl_host_cipher(host_ssl_id, name);
CREATE INDEX tbl_host_cipher_002 ON tbl_host_cipher(host_id);
