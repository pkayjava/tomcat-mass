DROP TABLE IF EXISTS `tbl_host`;
CREATE TABLE `tbl_host` (
  `id`                  VARCHAR(100) NOT NULL,
  `user_id`             VARCHAR(100) NOT NULL,
  `client_id`           VARCHAR(255) NOT NULL,
  `status`              VARCHAR(100) NULL,
  `os_name`             VARCHAR(255) NULL,
  `os_version`          VARCHAR(255) NULL,
  `os_arch`             VARCHAR(255) NULL,
  `host_home`           VARCHAR(255) NULL,
  `connected`           BIT(1) NOT NULL,
  `local_port`          INT(11) NULL,
  `ip_address`          VARCHAR(255) NULL,
  PRIMARY KEY (`id`)
);

CREATE INDEX tbl_host_001 ON tbl_host(user_id);
CREATE UNIQUE INDEX tbl_host_002 ON tbl_host(client_id);
CREATE INDEX tbl_host_003 ON tbl_host(status);
CREATE INDEX tbl_host_004 ON tbl_host(os_name);
CREATE INDEX tbl_host_005 ON tbl_host(os_version);
CREATE INDEX tbl_host_006 ON tbl_host(os_arch);
CREATE INDEX tbl_host_007 ON tbl_host(host_home);
CREATE INDEX tbl_host_008 ON tbl_host(connected);
CREATE INDEX tbl_host_009 ON tbl_host(local_port);
CREATE INDEX tbl_host_010 ON tbl_host(ip_address);
