DROP TABLE IF EXISTS `tbl_file`;
CREATE TABLE `tbl_file` (
  `id`                   VARCHAR(100) NOT NULL,
  `user_id`              VARCHAR(100) NOT NULL,
  `type`                 VARCHAR(10) NOT NULL,
  `name`                 VARCHAR(255) NOT NULL,
  `path`                 VARCHAR(255) NOT NULL,
  `file_sha`             VARCHAR(255) NOT NULL,
  `file_size`            VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE INDEX tbl_file_001 ON tbl_file(user_id);
CREATE INDEX tbl_file_002 ON tbl_file(type);
CREATE INDEX tbl_file_003 ON tbl_file(name);
CREATE UNIQUE INDEX tbl_file_004 ON tbl_file(path);
CREATE INDEX tbl_file_005 ON tbl_file(file_size);
