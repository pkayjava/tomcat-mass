DROP TABLE IF EXISTS `tbl_tomcat_vhost_port`;
CREATE TABLE `tbl_tomcat_vhost_port` (
  `id`                  VARCHAR(100) NOT NULL,
  `tomcat_id`           VARCHAR(100) NOT NULL,
  `tomcat_vhost_id`     VARCHAR(100) NOT NULL,
  `port`                INT(11) NOT NULL,
  `tls_mode`            VARCHAR(50) NULL,
  `tls`                 BIT(1) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX tbl_tomcat_vhost_port_001 ON tbl_tomcat_vhost_port(tomcat_vhost_id, port);
CREATE INDEX tbl_tomcat_vhost_port_002 ON tbl_tomcat_vhost_port(tls_mode);
CREATE INDEX tbl_tomcat_vhost_port_003 ON tbl_tomcat_vhost_port(tomcat_id);
CREATE INDEX tbl_tomcat_vhost_port_004 ON tbl_tomcat_vhost_port(tls);
