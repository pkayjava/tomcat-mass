DROP TABLE IF EXISTS `tbl_token`;
CREATE TABLE `tbl_token` (
  `id`                   VARCHAR(100) NOT NULL,
  `user_id`              VARCHAR(100) NOT NULL,
  `token_id`             VARCHAR(255) NOT NULL,
  `token_secret`         VARCHAR(255) NOT NULL,
  `revoked`              BIT(1) NOT NULL,
  `issued_datetime`      DATETIME NOT NULL,
  `revoked_datetime`     DATETIME NULL,
  PRIMARY KEY (`id`)
);

CREATE INDEX tbl_token_001 ON tbl_token(user_id);
CREATE UNIQUE INDEX tbl_token_002 ON tbl_token(token_id);
CREATE INDEX tbl_token_003 ON tbl_token(token_secret);
CREATE INDEX tbl_token_004 ON tbl_token(revoked);
CREATE INDEX tbl_token_005 ON tbl_token(issued_datetime);
CREATE INDEX tbl_token_006 ON tbl_token(revoked_datetime);
