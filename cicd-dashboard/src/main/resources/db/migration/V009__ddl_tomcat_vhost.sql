DROP TABLE IF EXISTS `tbl_tomcat_vhost`;
CREATE TABLE `tbl_tomcat_vhost` (
  `id`                  VARCHAR(100) NOT NULL,
  `tomcat_id`           VARCHAR(100) NOT NULL,
  `name`                VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX tbl_tomcat_vhost_001 ON tbl_tomcat_vhost(tomcat_id, name);
