DROP TABLE IF EXISTS `tbl_tomcat_vhost_cipher`;
CREATE TABLE `tbl_tomcat_vhost_cipher` (
  `id`                  VARCHAR(100) NOT NULL,
  `tomcat_id`           VARCHAR(100) NOT NULL,
  `tomcat_vhost_id`     VARCHAR(100) NOT NULL,
  `name`                VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX tbl_tomcat_vhost_cipher_001 ON tbl_tomcat_vhost_cipher(tomcat_vhost_id, name);
CREATE INDEX tbl_tomcat_vhost_cipher_002 ON tbl_tomcat_vhost_cipher(tomcat_id);
