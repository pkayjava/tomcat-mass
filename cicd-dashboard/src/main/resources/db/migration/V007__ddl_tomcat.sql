DROP TABLE IF EXISTS `tbl_tomcat`;
CREATE TABLE `tbl_tomcat` (
  `id`                  VARCHAR(100) NOT NULL,
  `host_id`             VARCHAR(100) NOT NULL,
  `user_id`             VARCHAR(100) NOT NULL,
  `instance`            VARCHAR(255) NOT NULL,
  `status`              VARCHAR(100) NOT NULL,
  `shutdown_port`       INT(11) NULL,
  `tomcat_version`      VARCHAR(100) NULL,
  `jvm_version`         VARCHAR(255) NULL,
  `jvm_vendor`          VARCHAR(255) NULL,
  `configuration`       TEXT NOT NULL,
  `jdk_id`              VARCHAR(100) NULL,
  `port_error`          BIT(1) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX tbl_tomcat_001 ON tbl_tomcat(host_id, instance);
CREATE INDEX tbl_tomcat_002 ON tbl_tomcat(status);
CREATE INDEX tbl_tomcat_003 ON tbl_tomcat(jvm_version);
CREATE INDEX tbl_tomcat_004 ON tbl_tomcat(jvm_vendor);
CREATE INDEX tbl_tomcat_005 ON tbl_tomcat(tomcat_version);
CREATE INDEX tbl_tomcat_006 ON tbl_tomcat(shutdown_port);
CREATE INDEX tbl_tomcat_007 ON tbl_tomcat(jdk_id);
CREATE INDEX tbl_tomcat_008 ON tbl_tomcat(user_id);
CREATE INDEX tbl_tomcat_009 ON tbl_tomcat(port_error);
