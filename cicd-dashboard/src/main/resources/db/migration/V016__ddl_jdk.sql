DROP TABLE IF EXISTS `tbl_jdk`;
CREATE TABLE `tbl_jdk` (
  `id`                  VARCHAR(100) NOT NULL,
  `user_id`             VARCHAR(100) NOT NULL,
  `host_id`             VARCHAR(100) NOT NULL,
  `jdk_name`            VARCHAR(200) NOT NULL,
  `file_id`             VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX tbl_jdk_001 ON tbl_jdk(host_id, jdk_name);
CREATE INDEX tbl_jdk_002 ON tbl_jdk(file_id);
CREATE INDEX tbl_jdk_003 ON tbl_jdk(user_id);
