DROP TABLE IF EXISTS `tbl_host_ssl`;
CREATE TABLE `tbl_host_ssl` (
  `id`                  VARCHAR(100) NOT NULL,
  `host_id`             VARCHAR(100) NOT NULL,
  `name`                VARCHAR(255) NOT NULL,
  `type`                VARCHAR(10) NOT NULL,
  `selected`            BIT(1) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX tbl_host_ssl_001 ON tbl_host_ssl(host_id, name);
CREATE INDEX tbl_host_ssl_002 ON tbl_host_ssl(type);
CREATE INDEX tbl_host_ssl_003 ON tbl_host_ssl(selected);
