DROP TABLE IF EXISTS `tbl_host_protocol`;
CREATE TABLE `tbl_host_protocol` (
  `id`                  VARCHAR(100) NOT NULL,
  `host_ssl_id`         VARCHAR(100) NOT NULL,
  `host_id`             VARCHAR(100) NOT NULL,
  `name`                VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX tbl_host_protocol_001 ON tbl_host_protocol(host_ssl_id, name);
CREATE INDEX tbl_host_protocol_002 ON tbl_host_protocol(host_id);
