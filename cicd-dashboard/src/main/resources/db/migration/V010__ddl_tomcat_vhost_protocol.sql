DROP TABLE IF EXISTS `tbl_tomcat_vhost_protocol`;
CREATE TABLE `tbl_tomcat_vhost_protocol` (
  `id`                  VARCHAR(100) NOT NULL,
  `tomcat_id`           VARCHAR(100) NOT NULL,
  `tomcat_vhost_id`     VARCHAR(100) NOT NULL,
  `name`                VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX tbl_tomcat_vhost_protocol_001 ON tbl_tomcat_vhost_protocol(tomcat_vhost_id, name);
CREATE INDEX tbl_tomcat_vhost_protocol_002 ON tbl_tomcat_vhost_protocol(tomcat_id);
