DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id`              VARCHAR(100) NOT NULL,
  `first_name`      VARCHAR(255) NOT NULL,
  `last_name`       VARCHAR(255) NOT NULL,
  `email_address`   VARCHAR(255) NOT NULL,
  `login`           VARCHAR(255) NOT NULL,
  `password`        VARCHAR(255) NOT NULL,
  `enabled`         BIT(1) NOT NULL,
  `confirmed`       BIT(1) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE INDEX tbl_user_001 ON tbl_user(first_name);
CREATE INDEX tbl_user_002 ON tbl_user(last_name);
CREATE UNIQUE INDEX tbl_user_003 ON tbl_user(email_address);
CREATE UNIQUE INDEX tbl_user_004 ON tbl_user(login);
CREATE INDEX tbl_user_005 ON tbl_user(password);
CREATE INDEX tbl_user_006 ON tbl_user(enabled);
CREATE INDEX tbl_user_007 ON tbl_user(confirmed);
