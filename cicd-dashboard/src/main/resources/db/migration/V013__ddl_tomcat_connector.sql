DROP TABLE IF EXISTS `tbl_tomcat_connector`;
CREATE TABLE `tbl_tomcat_connector` (
  `id`                       VARCHAR(100) NOT NULL,
  `tomcat_id`                VARCHAR(100) NOT NULL,
  `port`                     INT(11) NOT NULL,
  `protocol`                 VARCHAR(255) NOT NULL,
  `uri_encoding`             VARCHAR(10) NULL,
  `max_thread`               INT(11) NULL,
  `ssl_enabled`              BIT(1) NULL,
  `ssl_implementation_name`  VARCHAR(255) NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX tbl_tomcat_connector_001 ON tbl_tomcat_connector(tomcat_id, port);
CREATE INDEX tbl_tomcat_connector_002 ON tbl_tomcat_connector(protocol);
CREATE INDEX tbl_tomcat_connector_003 ON tbl_tomcat_connector(uri_encoding);
CREATE INDEX tbl_tomcat_connector_004 ON tbl_tomcat_connector(max_thread);
CREATE INDEX tbl_tomcat_connector_005 ON tbl_tomcat_connector(ssl_enabled);
CREATE INDEX tbl_tomcat_connector_006 ON tbl_tomcat_connector(ssl_implementation_name);
