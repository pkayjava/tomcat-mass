DROP TABLE IF EXISTS `tbl_host_port`;
CREATE TABLE `tbl_host_port` (
  `id`                  VARCHAR(100) NOT NULL,
  `user_id`             VARCHAR(100) NOT NULL,
  `host_id`             VARCHAR(100) NOT NULL,
  `tomcat_id`           VARCHAR(100) NOT NULL,
  `port`                INT(11) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE INDEX tbl_host_port_001 ON tbl_host_port(host_id);
CREATE INDEX tbl_host_port_002 ON tbl_host_port(user_id);
CREATE INDEX tbl_host_port_003 ON tbl_host_port(tomcat_id);
CREATE INDEX tbl_host_port_004 ON tbl_host_port(port);
