DROP TABLE IF EXISTS `tbl_white`;
CREATE TABLE `tbl_white` (
  `id`                    VARCHAR(100) NOT NULL,
  `client_id`             VARCHAR(100) NOT NULL,
  `revoked`               BIT(1) NOT NULL,
  `connected`             BIT(1) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX tbl_white_001 ON tbl_white(client_id);
CREATE INDEX tbl_white_002 ON tbl_white(revoked);
CREATE INDEX tbl_white_003 ON tbl_white(connected);

INSERT INTO tbl_white(id, client_id, revoked, connected) VALUES(uuid(), 'DASHBOARD', false, false);
INSERT INTO tbl_white(id, client_id, revoked, connected) VALUES(uuid(), 'COLLECTOR', false, false);
