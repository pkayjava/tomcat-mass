DROP TABLE IF EXISTS `tbl_tomcat_file`;
CREATE TABLE `tbl_tomcat_file` (
  `id`                  VARCHAR(100) NOT NULL,
  `tomcat_id`           VARCHAR(100) NOT NULL,
  `file_id`             VARCHAR(100) NOT NULL,
  `path`                VARCHAR(100) NOT NULL,
  `vhost`               VARCHAR(255) NULL,
  `last_modified`       DATETIME NOT NULL,
  `type`                VARCHAR(10)  NOT NULL,
  `status`              VARCHAR(20)  NOT NULL,
  `readonly`            BIT(1) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX tbl_tomcat_file_001 ON tbl_tomcat_file(tomcat_id, file_id);
CREATE UNIQUE INDEX tbl_tomcat_file_002 ON tbl_tomcat_file(tomcat_id, path);
CREATE INDEX tbl_tomcat_file_003 ON tbl_tomcat_file(type);
CREATE INDEX tbl_tomcat_file_004 ON tbl_tomcat_file(readonly);
CREATE INDEX tbl_tomcat_file_005 ON tbl_tomcat_file(status);
CREATE INDEX tbl_tomcat_file_006 ON tbl_tomcat_file(last_modified);
CREATE INDEX tbl_tomcat_file_007 ON tbl_tomcat_file(vhost);
