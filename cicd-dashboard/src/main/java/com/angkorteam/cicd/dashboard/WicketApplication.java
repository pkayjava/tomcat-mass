package com.angkorteam.cicd.dashboard;

import org.apache.commons.configuration.XMLPropertiesConfiguration;
import org.apache.wicket.Page;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.core.request.mapper.CryptoMapper;
import org.apache.wicket.markup.html.IPackageResourceGuard;
import org.apache.wicket.markup.html.SecurePackageResourceGuard;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.request.IRequestMapper;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.apache.wicket.validation.ValidationError;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.angkorteam.cicd.dashboard.pages.HomePage;
import com.angkorteam.cicd.dashboard.pages.host.HostBrowsePage;
import com.angkorteam.cicd.dashboard.pages.host.HostCreatePage;
import com.angkorteam.cicd.dashboard.pages.token.TokenBrowsePage;
import com.angkorteam.cicd.dashboard.pages.tomcat.TomcatBrowsePage;
import com.angkorteam.cicd.dashboard.pages.tomcat.TomcatCreatePage;
import com.angkorteam.cicd.dashboard.pages.tomcat.TomcatPreviewPage;
import com.angkorteam.framework.ReferenceUtilities;
import com.angkorteam.framework.ResourceScope;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.models.Login;
import com.angkorteam.framework.wicket.LoginPage;
import com.angkorteam.framework.wicket.LoginPage.ILogin;

public class WicketApplication extends AuthenticatedWebApplication implements ILogin {

	@Override
	protected void init() {
		super.init();
		XMLPropertiesConfiguration configuration = SpringBean.getBean(XMLPropertiesConfiguration.class);

		IPackageResourceGuard packageResourceGuard = this.getResourceSettings().getPackageResourceGuard();
		if (packageResourceGuard instanceof SecurePackageResourceGuard) {
			SecurePackageResourceGuard guard = (SecurePackageResourceGuard) packageResourceGuard;
			guard.addPattern("+*.ogg");
			guard.addPattern("+*.mp3");
		}
		getJavaScriptLibrarySettings().setJQueryReference(new PackageResourceReference(ResourceScope.class, ReferenceUtilities.J_QUERY_JS));

		RuntimeConfigurationType configurationType = RuntimeConfigurationType.valueOf(configuration.getString("wicket"));

		if (configurationType == RuntimeConfigurationType.DEPLOYMENT) {
			IRequestMapper cryptoMapper = new CryptoMapper(getRootRequestMapper(), this);
			setRootRequestMapper(cryptoMapper);
		}

		setMetaData(LoginPage.LOGIN, this);

		getMarkupSettings().setStripWicketTags(true);

		mountPage("/", HomePage.class);
		mountPage("/login", LoginPage.class);
		mountPage("/token/browse", TokenBrowsePage.class);
		mountPage("/tomcat/browse", TomcatBrowsePage.class);
		mountPage("/tomcat/deploy", TomcatCreatePage.class);
		mountPage("/tomcat/preview", TomcatPreviewPage.class);
		mountPage("/host/browse", HostBrowsePage.class);
		mountPage("/host/create", HostCreatePage.class);
	}

	@Override
	public void login(org.apache.wicket.Session session, Page page, Login login, TextField<String> loginField, TextField<String> passwordField) {
		if (session instanceof WicketSession) {
			boolean valid = ((WicketSession) session).signIn(login.getLogin(), login.getPassword());
			if (!valid) {
				loginField.error(new ValidationError("invalid"));
				passwordField.error(new ValidationError("invalid"));
			} else {
				page.setResponsePage(getHomePage());
			}
		}
	}

	@Override
	public Class<? extends Page> getHomePage() {
		return HomePage.class;
	}

	@Override
	public RuntimeConfigurationType getConfigurationType() {
		ApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		XMLPropertiesConfiguration configuration = applicationContext.getBean(XMLPropertiesConfiguration.class);
		return RuntimeConfigurationType.valueOf(configuration.getString("wicket"));
	}

	@Override
	protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
		return WicketSession.class;
	}

	@Override
	protected Class<? extends WebPage> getSignInPageClass() {
		return LoginPage.class;
	}

}
