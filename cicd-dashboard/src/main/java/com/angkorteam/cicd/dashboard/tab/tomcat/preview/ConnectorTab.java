package com.angkorteam.cicd.dashboard.tab.tomcat.preview;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.angkorteam.cicd.dashboard.pages.tomcat.TomcatPreviewPage;
import com.angkorteam.framework.wicket.extensions.markup.html.tabs.ITab;

public class ConnectorTab extends ITab {

	protected Page itemPage;

	public ConnectorTab(Page itemPage) {
		this.itemPage = itemPage;
	}

	@Override
	public IModel<String> getTitle() {
		return Model.of((TomcatPreviewPage.TAB_CONNECTOR + 1) + ". Connector");
	}

	@Override
	public WebMarkupContainer getPanel(String containerId) {
		return new ConnectorPanel(containerId, this.itemPage);
	}

	@Override
	public boolean isVisible() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
