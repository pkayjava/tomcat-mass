package com.angkorteam.cicd.dashboard.pages.host;

import java.util.List;

import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import com.angkorteam.cicd.dashboard.pages.Page;
import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.InsertQuery;
import com.angkorteam.framework.models.PageBreadcrumb;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.angkorteam.framework.wicket.markup.html.form.Button;
import com.angkorteam.framework.wicket.markup.html.form.Form;
import com.google.common.collect.Lists;

public class HostCreatePage extends Page {

	protected Form<Void> form;
	protected Button saveButton;
	protected BookmarkablePageLink<Void> closeLink;

	protected UIRow row1;

	protected UIBlock nameBlock;
	protected UIContainer nameIContainer;
	protected String nameValue;
	protected TextField<String> nameField;

	protected UIBlock row1Block1;

	@Override
	public IModel<List<PageBreadcrumb>> buildPageBreadcrumb() {
		List<PageBreadcrumb> BREADCRUMB = Lists.newArrayList();
		{
			BREADCRUMB.add(new PageBreadcrumb.Builder().withLabel(Model.of("Host")).build());
		}
		return Model.ofList(BREADCRUMB);
	}

	@Override
	protected void initData() {
	}

	@Override
	protected void initComponent() {
		this.form = new Form<>("form");
		add(this.form);

		this.saveButton = new Button("saveButton");
		this.saveButton.setOnSubmit(this::saveButtonSubmit);
		this.form.add(this.saveButton);

		this.closeLink = new BookmarkablePageLink<>("closeLink", HostBrowsePage.class);
		this.form.add(this.closeLink);

		this.row1 = UIRow.newUIRow("row1", this.form);

		this.nameBlock = this.row1.newUIBlock("nameBlock", Size.Six_6);
		this.nameIContainer = this.nameBlock.newUIContainer("nameIContainer");
		this.nameField = new TextField<>("nameField", new PropertyModel<>(this, "nameValue"));
		this.nameIContainer.add(this.nameField);
		this.nameIContainer.newFeedback("nameFeedback", this.nameField);

		this.row1Block1 = this.row1.newUIBlock("row1Block1", Size.Six_6);
	}

	@Override
	protected void configureMetaData() {
	}

	public void saveButtonSubmit(Button button) {

		JdbcNamed jdbcNamed = SpringBean.getBean(JdbcNamed.class);

		InsertQuery insertQuery = new InsertQuery(Host.NAME);
		insertQuery.addValue(Host.Field.ID + " = uuid()");
		insertQuery.addValue(Host.Field.CLIENT_ID + " = :client_id", this.nameValue);
		insertQuery.addValue(Host.Field.CONNECTED + " = false");
		insertQuery.addValue(Host.Field.USER_ID + " = :user_id", getSession().getUserId());

		jdbcNamed.update(insertQuery.toSQL(), insertQuery.getParam());

		setResponsePage(HostBrowsePage.class);
	}

}
