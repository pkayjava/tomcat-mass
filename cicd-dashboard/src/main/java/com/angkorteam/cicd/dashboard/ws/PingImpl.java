package com.angkorteam.cicd.dashboard.ws;

import java.util.Date;

import javax.jws.WebService;

import org.apache.commons.lang3.time.DateFormatUtils;

@WebService
public class PingImpl implements Ping {

	@Override
	public String server() {
		return DateFormatUtils.ISO_8601_EXTENDED_DATETIME_TIME_ZONE_FORMAT.format(new Date());
	}

}