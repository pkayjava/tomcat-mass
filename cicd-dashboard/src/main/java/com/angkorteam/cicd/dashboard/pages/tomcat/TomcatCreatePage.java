package com.angkorteam.cicd.dashboard.pages.tomcat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.configuration.XMLPropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.ValidationError;
import org.springframework.jdbc.core.JdbcTemplate;

import com.angkorteam.cicd.dashboard.Functions;
import com.angkorteam.cicd.dashboard.Repository;
import com.angkorteam.cicd.dashboard.pages.Page;
import com.angkorteam.cicd.dashboard.provider.HostProvider;
import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.cicd.ddl.HostPort;
import com.angkorteam.cicd.ddl.Tomcat;
import com.angkorteam.cicd.ddl.TomcatConnector;
import com.angkorteam.cicd.ddl.TomcatFile;
import com.angkorteam.cicd.ddl.TomcatVHost;
import com.angkorteam.cicd.ddl.TomcatVHostCipher;
import com.angkorteam.cicd.ddl.TomcatVHostPort;
import com.angkorteam.cicd.ddl.TomcatVHostProtocol;
import com.angkorteam.cicd.xml.tomcat.XmlConnector;
import com.angkorteam.cicd.xml.tomcat.XmlHost;
import com.angkorteam.cicd.xml.tomcat.XmlSSLHostConfig;
import com.angkorteam.cicd.xml.tomcat.XmlServer;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.InsertQuery;
import com.angkorteam.framework.models.PageBreadcrumb;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.angkorteam.framework.wicket.markup.html.form.Button;
import com.angkorteam.framework.wicket.markup.html.form.Form;
import com.angkorteam.framework.wicket.markup.html.form.select2.Option;
import com.angkorteam.framework.wicket.markup.html.form.select2.Select2MultipleChoice;
import com.google.common.collect.Lists;

public class TomcatCreatePage extends Page {

	protected Form<Void> form;
	protected Button saveButton;
	protected BookmarkablePageLink<Void> closeLink;

	protected UIRow row1;

	protected UIBlock nameBlock;
	protected UIContainer nameIContainer;
	protected String nameValue;
	protected TextField<String> nameField;

	protected UIBlock tomcatBlock;
	protected UIContainer tomcatIContainer;
	protected List<FileUpload> tomcatValue;
	protected FileUploadField tomcatField;

	protected UIRow row2;

	protected HostProvider hostProvider;
	protected UIBlock hostBlock;
	protected UIContainer hostIContainer;
	protected List<Option> hostValue;
	protected Select2MultipleChoice<Option> hostField;

	@Override
	public IModel<List<PageBreadcrumb>> buildPageBreadcrumb() {
		List<PageBreadcrumb> BREADCRUMB = Lists.newArrayList();
		{
			BREADCRUMB.add(new PageBreadcrumb.Builder().withLabel(Model.of("Server")).build());
		}
		return Model.ofList(BREADCRUMB);
	}

	@Override
	protected void initData() {
		this.hostProvider = new HostProvider(getSession().getUserId());
	}

	@Override
	protected void initComponent() {
		this.form = new Form<>("form");
		add(this.form);

		this.saveButton = new Button("saveButton");
		this.saveButton.setOnSubmit(this::saveButtonSubmit);
		this.form.add(this.saveButton);

		this.closeLink = new BookmarkablePageLink<>("closeLink", TomcatBrowsePage.class);
		this.form.add(this.closeLink);

		this.row1 = UIRow.newUIRow("row1", this.form);

		this.nameBlock = this.row1.newUIBlock("nameBlock", Size.Six_6);
		this.nameIContainer = this.nameBlock.newUIContainer("nameIContainer");
		this.nameField = new TextField<>("nameField", new PropertyModel<>(this, "nameValue"));
		this.nameIContainer.add(this.nameField);
		this.nameIContainer.newFeedback("nameFeedback", this.nameField);

		this.tomcatBlock = this.row1.newUIBlock("tomcatBlock", Size.Six_6);
		this.tomcatIContainer = this.tomcatBlock.newUIContainer("tomcatIContainer");
		this.tomcatField = new FileUploadField("tomcatField", new PropertyModel<>(this, "tomcatValue"));
		this.tomcatIContainer.add(this.tomcatField);
		this.tomcatIContainer.newFeedback("tomcatFeedback", this.tomcatField);

		this.row2 = UIRow.newUIRow("row2", this.form);

		this.hostBlock = this.row2.newUIBlock("hostBlock", Size.Twelve_12);
		this.hostIContainer = this.hostBlock.newUIContainer("hostIContainer");
		this.hostField = new Select2MultipleChoice<>("hostField", new PropertyModel<>(this, "hostValue"), this.hostProvider);
		this.hostIContainer.add(this.hostField);
		this.hostIContainer.newFeedback("hostFeedback", this.hostField);
	}

	@Override
	protected void configureMetaData() {
		this.nameField.setRequired(true);
		this.nameField.add(this::nameValidation);

		this.tomcatField.setRequired(true);
		this.tomcatField.add(this::tomcatValidation);

		this.hostField.setRequired(true);
	}

	protected void tomcatValidation(IValidatable<List<FileUpload>> validatable) {
		List<FileUpload> values = validatable.getValue();
		if (values != null && !values.isEmpty()) {
			FileUpload value = values.get(0);
			String clientFileName = StringUtils.lowerCase(value.getClientFileName());
			if (!clientFileName.endsWith(".tar.gz")) {
				validatable.error(new ValidationError("accept only tar.gz"));
			}
		}
	}

	protected void nameValidation(IValidatable<String> validatable) {
		String value = validatable.getValue();
		if (value != null && !"".equals(value)) {
			String lvalue = StringUtils.lowerCase(value);
			if (!lvalue.matches("[-_.a-z0-9]+")) {
				validatable.error(new ValidationError("invalid name, only a to z, underscore(_) and dot(.) are allow"));
			} else {
				if (this.hostValue != null) {
					JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
					for (Option host : this.hostValue) {
						if (jdbcTemplate.queryForObject("SELECT COUNT(*) FROM " + Tomcat.NAME + " WHERE " + Tomcat.Field.INSTANCE + " = ? AND " + Tomcat.Field.HOST_ID + " = ?", Boolean.class, this.nameValue, host.getId())) {
							validatable.error(new ValidationError("duplicated name"));
							break;
						}
					}
				}
			}
		}
	}

	public void saveButtonSubmit(Button button) {

		XMLPropertiesConfiguration configuration = SpringBean.getBean(XMLPropertiesConfiguration.class);
		String resourceRepo = configuration.getString("resource.repo");
		File resourceRepoFile = new File(resourceRepo);
		resourceRepoFile.mkdirs();
		String pattern = DateFormatUtils.format(new Date(), "yyyy/MM/dd");
		File patternFolder = new File(resourceRepoFile, pattern);
		patternFolder.mkdirs();

		this.nameValue = StringUtils.lowerCase(this.nameValue);

		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);
		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		File tempFolder = new File(FileUtils.getTempDirectory(), System.currentTimeMillis() + "_" + this.nameValue);
		tempFolder.mkdirs();

		File tempFile = new File(tempFolder, this.nameValue + ".tar.gz");
		File untarFile = new File(tempFolder, this.nameValue);

		try {
			this.tomcatField.getFileUpload().writeTo(tempFile);
		} catch (Exception e) {
			throw new WicketRuntimeException(e);
		}

		try {
			Functions.unTarGz(tempFile, untarFile);
		} catch (IOException e) {
			throw new WicketRuntimeException(e);
		}
		File tomcatFile = untarFile.listFiles()[0];
		String tomcatFilePath = tomcatFile.getAbsolutePath();

		File releaseNotes = new File(tomcatFile, "RELEASE-NOTES");
		String tomcatVersion = "N/A";
		List<String> lines = new ArrayList<>();
		try {
			lines = FileUtils.readLines(releaseNotes, "UTF-8");
		} catch (IOException e1) {
		}
		for (String line : lines) {
			int i = line.indexOf("Apache Tomcat Version ");
			if (i >= 0) {
				tomcatVersion = line.substring(i + "Apache Tomcat Version ".length());
				break;
			}
		}
		if (tomcatVersion.startsWith("7.0") || tomcatVersion.startsWith("8.0") || tomcatVersion.startsWith("8.5") || tomcatVersion.startsWith("9.0")) {
		} else {
			this.tomcatField.error(new ValidationError(tomcatVersion + " is not supported"));
			return;
		}

		File serverFile = new File(tomcatFile, "conf/server.xml");

		String serverValue = null;
		try {
			serverValue = FileUtils.readFileToString(serverFile, "UTF-8");
		} catch (IOException e) {
			throw new WicketRuntimeException(e);
		}

		XmlServer xmlServer = XmlServer.fromText(serverValue);

		// insert data

		InsertQuery insertQuery = null;

		List<Integer> ports = new ArrayList<>();

		for (Option hostValue : this.hostValue) {
			String tomcatId = jdbcTemplate.queryForObject("SELECT uuid() FROM DUAL", String.class);

			for (XmlConnector connector : xmlServer.getService().getConnectors()) {
				insertQuery = new InsertQuery(TomcatConnector.NAME);
				insertQuery.addValue(TomcatConnector.Field.ID + " = uuid()");
				insertQuery.addValue(TomcatConnector.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
				insertQuery.addValue(TomcatConnector.Field.MAX_THREAD + " = :max_thread", connector.getMaxThreads());
				insertQuery.addValue(TomcatConnector.Field.PORT + " = :port", connector.getPort());
				insertQuery.addValue(TomcatConnector.Field.PROTOCOL + " = :protocol", connector.getProtocol());
				insertQuery.addValue(TomcatConnector.Field.URI_ENCODING + " = :uri_encoding", connector.getUriEncoding());
				insertQuery.addValue(TomcatConnector.Field.SSL_ENABLED + " = :ssl_enabled", connector.getSSLEnabled());
				insertQuery.addValue(TomcatConnector.Field.SSL_IMPLEMENTATION_NAME + " = :ssl_implementation_name", connector.getSslImplementationName());
				named.update(insertQuery.toSQL(), insertQuery.getParam());
				ports.add(connector.getPort());
			}

			Map<Integer, Map<String, Map<String, String>>> hosts = findConnectorHost(xmlServer);

			for (XmlHost host : xmlServer.getService().getEngine().getHosts()) {
				String vhostId = jdbcTemplate.queryForObject("SELECT uuid() FROM DUAL", String.class);
				insertQuery = new InsertQuery(TomcatVHost.NAME);
				insertQuery.addValue(TomcatVHost.Field.ID + " = :id", vhostId);
				insertQuery.addValue(TomcatVHost.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
				insertQuery.addValue(TomcatVHost.Field.NAME + " = :name", host.getName());
				named.update(insertQuery.toSQL(), insertQuery.getParam());
				for (Entry<Integer, Map<String, Map<String, String>>> port : hosts.entrySet()) {
					Map<String, String> info = null;
					if (port.getValue().containsKey(host.getName())) {
						info = port.getValue().get(host.getName());
					} else {
						info = port.getValue().get("_default_");
					}
					if (info != null) {
						String ciphers = info.get("ciphers");
						if (ciphers != null && !"".equals(ciphers)) {
							for (String cipher : StringUtils.split(ciphers, ",")) {
								insertQuery = new InsertQuery(TomcatVHostCipher.NAME);
								insertQuery.addValue(TomcatVHostCipher.Field.ID + " = uuid()");
								insertQuery.addValue(TomcatVHostCipher.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
								insertQuery.addValue(TomcatVHostCipher.Field.TOMCAT_VHOST_ID + " = :vhostId", vhostId);
								insertQuery.addValue(TomcatVHostCipher.Field.NAME + " = :name", cipher);
								named.update(insertQuery.toSQL(), insertQuery.getParam());
							}
						}
						String protocols = info.get("protocols");
						if (protocols != null & !"".equals(protocols)) {
							for (String protocol : StringUtils.split(protocols, ",")) {
								insertQuery = new InsertQuery(TomcatVHostProtocol.NAME);
								insertQuery.addValue(TomcatVHostProtocol.Field.ID + " = uuid()");
								insertQuery.addValue(TomcatVHostProtocol.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
								insertQuery.addValue(TomcatVHostProtocol.Field.TOMCAT_VHOST_ID + " = :vhostId", vhostId);
								insertQuery.addValue(TomcatVHostProtocol.Field.NAME + " = :name", protocol);
								named.update(insertQuery.toSQL(), insertQuery.getParam());
							}
						}
					}
					String certificateVerification = null;
					if (info != null) {
						certificateVerification = info.get("certificateVerification");
					}
					Integer portNumber = port.getKey();
					String tlsMode = null;
					if ("required".equals(certificateVerification)) {
						tlsMode = "2Way Strict";
					} else if ("optionalNoCA".equals(certificateVerification)) {
						tlsMode = "2Way Welcome";
					} else if ("optional".equals(certificateVerification)) {
						tlsMode = "2Way Verified";
					} else {
						tlsMode = "1Way";
					}
					insertQuery = new InsertQuery(TomcatVHostPort.NAME);
					insertQuery.addValue(TomcatVHostPort.Field.ID + " = uuid()");
					insertQuery.addValue(TomcatVHostPort.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
					insertQuery.addValue(TomcatVHostPort.Field.TOMCAT_VHOST_ID + " = :vhostId", vhostId);
					insertQuery.addValue(TomcatVHostPort.Field.PORT + " = :port", portNumber);
					if (info != null) {
						insertQuery.addValue(TomcatVHostPort.Field.TLS + " = :tls", true);
						insertQuery.addValue(TomcatVHostPort.Field.TLS_MODE + " = :tls_mode", tlsMode);
					} else {
						insertQuery.addValue(TomcatVHostPort.Field.TLS + " = :tls", false);
					}
					named.update(insertQuery.toSQL(), insertQuery.getParam());
				}
			}

			for (File srcFile : FileUtils.listFiles(tomcatFile, null, true)) {
				String path = srcFile.getAbsolutePath().substring(tomcatFilePath.length());
				if (path.startsWith("/bin") || path.startsWith("/lib") || path.startsWith("/conf") || path.equals("/RELEASE-NOTES")) {
					String fileId = Repository.saveFile(srcFile);

					insertQuery = new InsertQuery(TomcatFile.NAME);
					insertQuery.addValue(TomcatFile.Field.ID + " = uuid()");
					insertQuery.addValue(TomcatFile.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
					insertQuery.addValue(TomcatFile.Field.FILE_ID + " = :file_id", fileId);
					insertQuery.addValue(TomcatFile.Field.LAST_MODIFIED + " = :last_modified", new Date());
					insertQuery.addValue(TomcatFile.Field.PATH + " = :path", path);
					insertQuery.addValue(TomcatFile.Field.STATUS + " = :status", TomcatFile.STATUS_NA);
					if (path.equals("/conf/server.xml")) {
						insertQuery.addValue(TomcatFile.Field.READONLY + " = :readonly", false);
					} else {
						insertQuery.addValue(TomcatFile.Field.READONLY + " = :readonly", true);
					}
					if (path.startsWith("/lib")) {
						insertQuery.addValue(TomcatFile.Field.TYPE + " = :type", TomcatFile.TYPE_LIBRARY);
					} else {
						insertQuery.addValue(TomcatFile.Field.TYPE + " = :type", TomcatFile.TYPE_BASE);
					}
					named.update(insertQuery.toSQL(), insertQuery.getParam());
				}
			}

			Integer hostLocalPort = jdbcTemplate.queryForObject("SELECT " + Host.Field.LOCAL_PORT + " FROM " + Host.NAME + " WHERE " + Host.Field.ID + " = ?", Integer.class, hostValue.getId());
			List<Integer> hostPort = jdbcTemplate.queryForList("SELECT " + HostPort.Field.PORT + " FROM " + HostPort.NAME + " WHERE " + HostPort.Field.HOST_ID + " = ?", Integer.class, hostValue.getId());
			if (hostLocalPort != null) {
				hostPort.add(hostLocalPort);
			}
			boolean error = false;
			for (Integer port : ports) {
				if (hostPort.contains(port)) {
					error = true;
					break;
				}
			}

			insertQuery = new InsertQuery(Tomcat.NAME);
			insertQuery.addValue(Tomcat.Field.ID + " = :id", tomcatId);
			insertQuery.addValue(Tomcat.Field.USER_ID + " = :user_id", getSession().getUserId());
			insertQuery.addValue(Tomcat.Field.HOST_ID + " = :host_id", hostValue.getId());
			if (xmlServer.getPort() != null) {
				insertQuery.addValue(Tomcat.Field.SHUTDOWN_PORT + " = :shutdown_port", xmlServer.getPort());
				ports.add(xmlServer.getPort());
			} else {
				insertQuery.addValue(Tomcat.Field.SHUTDOWN_PORT + " = :shutdown_port", -1);
			}
			insertQuery.addValue(Tomcat.Field.INSTANCE + " = :instance", this.nameValue);
			insertQuery.addValue(Tomcat.Field.CONFIGURATION + " = :configuration", serverValue);
			insertQuery.addValue(Tomcat.Field.STATUS + " = :status", "DESTROYED");
			insertQuery.addValue(Tomcat.Field.PORT_ERROR + " = :port_error", error);
			named.update(insertQuery.toSQL(), insertQuery.getParam());

			if (!error) {
				for (Integer port : ports) {
					insertQuery = new InsertQuery(HostPort.NAME);
					insertQuery.addValue(HostPort.Field.ID + " = uuid()");
					insertQuery.addValue(HostPort.Field.HOST_ID + " = :host_id", hostValue.getId());
					insertQuery.addValue(HostPort.Field.USER_ID + " = :user_id", getSession().getUserId());
					insertQuery.addValue(HostPort.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
					insertQuery.addValue(HostPort.Field.PORT + " = :port", port);
					named.update(insertQuery.toSQL(), insertQuery.getParam());
				}
			}
		}

		FileUtils.deleteQuietly(tempFolder);

		setResponsePage(TomcatBrowsePage.class);
	}

	public Map<Integer, Map<String, Map<String, String>>> findConnectorHost(XmlServer xmlServer) {
		Map<Integer, Map<String, Map<String, String>>> hosts = new HashMap<>();
		for (XmlConnector connector : xmlServer.getService().getConnectors()) {
			if (!hosts.containsKey(connector.getPort())) {
				hosts.put(connector.getPort(), new HashMap<>());
			}
			if (connector.getSslHostConfigs() != null) {
				for (XmlSSLHostConfig sslHostConfig : connector.getSslHostConfigs()) {
					Map<String, String> host = new HashMap<>();
					host.put("ciphers", sslHostConfig.getCiphers());
					host.put("protocols", sslHostConfig.getProtocols());
					host.put("certificateVerification", sslHostConfig.getCertificateVerification());
					hosts.get(connector.getPort()).put(sslHostConfig.getHostName(), host);
				}
			}
		}
		return hosts;
	}

}
