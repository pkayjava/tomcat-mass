package com.angkorteam.cicd.dashboard.spring;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import com.angkorteam.framework.spring.Delegate;

public class AuthenticationProviderFactoryBean implements FactoryBean<org.springframework.security.authentication.AuthenticationProvider>, InitializingBean {

	private Delegate delegate;

	private org.springframework.security.authentication.AuthenticationProvider authenticationProvider;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.authenticationProvider = new AuthenticationProvider(this.delegate);
	}

	@Override
	public org.springframework.security.authentication.AuthenticationProvider getObject() throws Exception {
		return this.authenticationProvider;
	}

	@Override
	public Class<?> getObjectType() {
		return org.springframework.security.authentication.AuthenticationProvider.class;
	}

	public void setDelegate(Delegate delegate) {
		this.delegate = delegate;
	}

}
