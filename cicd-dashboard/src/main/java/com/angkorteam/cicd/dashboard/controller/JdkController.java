package com.angkorteam.cicd.dashboard.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.angkorteam.cicd.ddl.File;
import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.cicd.ddl.Jdk;
import com.angkorteam.cicd.xml.XmlJdk;
import com.angkorteam.cicd.xml.command.HostJdkCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;
import com.angkorteam.framework.Function;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.SelectQuery;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.spring.JdbcTemplate;

@Controller
public class JdkController {

	@RequestMapping(path = "/jdk/{clientId}")
	public void jdk(Principal principal, @PathVariable("clientId") String clientId, HttpServletRequest request, HttpServletResponse response) throws IOException {
		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);
		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		Map<String, Object> hostObject = jdbcTemplate.queryForMap("SELECT " + Host.Field.ID + ", " + Host.Field.USER_ID + " FROM " + Host.NAME + " WHERE " + Host.Field.CLIENT_ID + " = ?", clientId);
		if (hostObject == null || !principal.getName().equals(hostObject.get(Host.Field.USER_ID))) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}

		SelectQuery selectQuery = new SelectQuery(Jdk.NAME);
		selectQuery.addJoin("INNER JOIN " + File.NAME + " ON " + File.NAME + "." + File.Field.ID + " = " + Jdk.NAME + "." + Jdk.Field.FILE_ID);
		selectQuery.addWhere(Jdk.NAME + "." + Jdk.Field.HOST_ID + " = :host_id", hostObject.get(Host.Field.ID));
		selectQuery.addField(Jdk.NAME + "." + Jdk.Field.JDK_NAME);
		selectQuery.addField(Jdk.NAME + "." + Jdk.Field.FILE_ID);
		selectQuery.addField(File.NAME + "." + File.Field.FILE_SHA);

		List<Map<String, Object>> jdkObjects = named.queryForList(selectQuery.toSQL(), selectQuery.getParam());

		HostJdkCommand command = new HostJdkCommand();

		String httpAddress = Function.getHttpAddress(request);

		for (Map<String, Object> jdkObject : jdkObjects) {
			XmlJdk java = new XmlJdk();
			java.setName((String) jdkObject.get(Jdk.Field.JDK_NAME));
			java.setSha((String) jdkObject.get(File.Field.FILE_SHA));
			java.setDownload(httpAddress + "/api/file/" + (String) jdkObject.get(Jdk.Field.FILE_ID));
			command.getJdk().add(java);
		}

		response.getWriter().write(new XmlMessage(command).toText());
	}

}
