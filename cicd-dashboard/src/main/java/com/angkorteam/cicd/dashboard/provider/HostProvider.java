package com.angkorteam.cicd.dashboard.provider;

import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.wicket.markup.html.form.select2.OptionMultipleChoiceProvider;

public class HostProvider extends OptionMultipleChoiceProvider {

	public HostProvider(String userId) {
		super(Host.NAME, Host.Field.ID, Host.Field.CLIENT_ID);
		applyWhere("user", Host.Field.USER_ID + " = '" + userId + "'");
	}

	@Override
	protected JdbcNamed getNamed() {
		return SpringBean.getBean(JdbcNamed.class);
	}

}
