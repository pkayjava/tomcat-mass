package com.angkorteam.cicd.dashboard.pages;

import java.util.List;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.angkorteam.framework.models.PageBreadcrumb;
import com.google.common.collect.Lists;

public class LogoutPage extends Page {

	@Override
	protected void initData() {
	}

	@Override
	protected void initComponent() {

	}

	@Override
	protected void configureMetaData() {
		getSession().invalidate();
		setResponsePage(getApplication().getHomePage());
	}

	@Override
	protected IModel<List<PageBreadcrumb>> buildPageBreadcrumb() {
		List<PageBreadcrumb> BREADCRUMB = Lists.newArrayList();
		return Model.ofList(BREADCRUMB);
	}

}
