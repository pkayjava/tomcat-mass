package com.angkorteam.cicd.dashboard;

import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.request.Request;

import com.angkorteam.cicd.ddl.User;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.SelectQuery;
import com.angkorteam.framework.spring.JdbcNamed;

public class WicketSession extends AuthenticatedWebSession {

	public static final String ALL_FUNCTION = "ALL_FUNCTION";

	private Roles roles;

	private String userId;

	public WicketSession(Request request) {
		super(request);
		this.roles = new Roles();
	}

	@Override
	protected boolean authenticate(String username, String password) {
		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);
		SelectQuery query = new SelectQuery(User.NAME);
		query.addField(User.Field.ID);
		query.addWhere(User.Field.LOGIN + " = :login", username);
		query.addWhere(User.Field.PASSWORD + " = md5(:password)", "password", password);
		this.userId = named.queryForObject(query.toSQL(), query.getParam(), String.class);
		if (StringUtils.isNotEmpty(this.userId)) {
			this.roles.add(ALL_FUNCTION);
		}
		return StringUtils.isNotEmpty(this.userId);
	}

	@Override
	public Roles getRoles() {
		return this.roles;
	}

	public String getUserId() {
		return this.userId;
	}

}
