package com.angkorteam.cicd.dashboard.pages;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import com.angkorteam.cicd.dashboard.WicketSession;
import com.angkorteam.framework.models.PageBreadcrumb;
import com.angkorteam.framework.wicket.chart.chartjs.BarChart;
import com.angkorteam.framework.wicket.chart.chartjs.BarChart.BarDataset;
import com.angkorteam.framework.wicket.chart.chartjs.BarChart.BarItem;
import com.angkorteam.framework.wicket.chart.chartjs.DonutChart;
import com.angkorteam.framework.wicket.chart.chartjs.DonutChart.DonutItem;
import com.angkorteam.framework.wicket.chart.chartjs.LineChart;
import com.angkorteam.framework.wicket.chart.chartjs.LineChart.LineDataset;
import com.angkorteam.framework.wicket.chart.chartjs.LineChart.LineItem;
import com.angkorteam.framework.wicket.chart.chartjs.PieChart;
import com.angkorteam.framework.wicket.chart.chartjs.PieChart.PieItem;
import com.angkorteam.framework.wicket.chart.morris.AreaChart;
import com.angkorteam.framework.wicket.chart.morris.AreaChart.AreaDataset;
import com.google.common.collect.Lists;

@AuthorizeInstantiation(WicketSession.ALL_FUNCTION)
public class HomePage extends Page {

	protected List<PieItem> pieChartDataset;

	protected List<DonutItem> donutChartDataset;

	protected BarDataset barDataset;

	protected LineDataset lineDataset;

	protected AreaDataset areaDataset;

	@Override
	protected void initData() {
		this.pieChartDataset = new LinkedList<>();
		{
			PieItem item = new PieItem();
			item.setValue(100);
			item.setLabel("Window");
			item.setHighlight("#d2d6de");
			item.setColor("#d2d6de");
			this.pieChartDataset.add(item);
		}
		{
			PieItem item = new PieItem();
			item.setValue(80);
			item.setLabel("Linux");
			item.setHighlight("#3c8dbc");
			item.setColor("#3c8dbc");
			this.pieChartDataset.add(item);
		}
		{
			PieItem item = new PieItem();
			item.setValue(60);
			item.setLabel("MacOS");
			item.setHighlight("#00c0ef");
			item.setColor("#00c0ef");
			this.pieChartDataset.add(item);
		}

		this.donutChartDataset = new LinkedList<>();
		{
			DonutItem item = new DonutItem();
			item.setValue(100);
			item.setLabel("Window");
			item.setHighlight("#d2d6de");
			item.setColor("#d2d6de");
			this.donutChartDataset.add(item);
		}
		{
			DonutItem item = new DonutItem();
			item.setValue(80);
			item.setLabel("Linux");
			item.setHighlight("#3c8dbc");
			item.setColor("#3c8dbc");
			this.donutChartDataset.add(item);
		}
		{
			DonutItem item = new DonutItem();
			item.setValue(60);
			item.setLabel("MacOS");
			item.setHighlight("#00c0ef");
			item.setColor("#00c0ef");
			this.donutChartDataset.add(item);
		}

		this.barDataset = new BarDataset();
		this.barDataset.getLabels().add("Window");
		this.barDataset.getLabels().add("Linux");
		this.barDataset.getLabels().add("MacOS");
		BarItem barItem = new BarItem();
		this.barDataset.getDatasets().add(barItem);

		barItem.setFillColor("#3c8dbc");
		barItem.setHighlightFill("#d2d6de");
		barItem.setHighlightStroke("#d2d6de");
		barItem.setStrokeColor("#00c0ef");
		barItem.getData().add(1);
		barItem.getData().add(3);
		barItem.getData().add(2);

		this.lineDataset = new LineDataset();
		this.lineDataset.getLabels().add("Window");
		this.lineDataset.getLabels().add("Linux");
		this.lineDataset.getLabels().add("MacOS");
		LineItem lineItem = new LineItem();
		this.lineDataset.getDatasets().add(lineItem);

		lineItem.setFillColor("#3c8dbc");
		lineItem.setPointColor("#d2d6de");
		lineItem.setPointHighlightFill("#d2d6de");
		lineItem.setPointHighlightStroke("#01c0ef");
		lineItem.setPointStrokeColor("#d2d62e");
		lineItem.setStrokeColor("#d2c6de");
		lineItem.getData().add(1);
		lineItem.getData().add(3);
		lineItem.getData().add(2);

		this.areaDataset = new AreaDataset();
		{
			Map<String, Object> item = new LinkedHashMap<>();
			item.put("y", "2011 Q1");
			item.put("item1", 2666);
			item.put("item2", 2666);
			this.areaDataset.getData().add(item);
		}
		{
			Map<String, Object> item = new LinkedHashMap<>();
			item.put("y", "2011 Q2");
			item.put("item1", 1000);
			item.put("item2", 2000);
			this.areaDataset.getData().add(item);
		}
		this.areaDataset.setXkey("y");
		this.areaDataset.getLabels().add("Item 1");
		this.areaDataset.getLabels().add("Item 2");
		this.areaDataset.getYkeys().add("item1");
		this.areaDataset.getYkeys().add("item2");
		this.areaDataset.getLineColors().add("#a0d0e0");
		this.areaDataset.getLineColors().add("#3c8dbc");
	}

	@Override
	protected void initComponent() {
		LineChart lineChart = new LineChart("lineChart", new PropertyModel<>(this, "lineDataset"));
		add(lineChart);

		PieChart pieChart = new PieChart("pieChart", new PropertyModel<>(this, "pieChartDataset"));
		add(pieChart);

		DonutChart donutChart = new DonutChart("donutChart", new PropertyModel<>(this, "donutChartDataset"));
		add(donutChart);

		BarChart barChart = new BarChart("barChart", new PropertyModel<>(this, "barDataset"));
		add(barChart);

		AreaChart areaChart = new AreaChart("areaChart", new PropertyModel<>(this, "areaDataset"));
		add(areaChart);
	}

	@Override
	protected void configureMetaData() {

	}

	@Override
	protected IModel<List<PageBreadcrumb>> buildPageBreadcrumb() {
		List<PageBreadcrumb> BREADCRUMB = Lists.newArrayList();
		{
			BREADCRUMB.add(new PageBreadcrumb.Builder().withLabel(Model.of("sss")).withPage(HomePage.class).build());
		}
		return Model.ofList(BREADCRUMB);
	}

}
