package com.angkorteam.cicd.dashboard.provider;

import com.angkorteam.cicd.ddl.Jdk;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.wicket.markup.html.form.select2.OptionSingleChoiceProvider;

public class JdkProvider extends OptionSingleChoiceProvider {

	public JdkProvider(String hostId) {
		super(Jdk.NAME, Jdk.Field.ID, Jdk.Field.JDK_NAME);
		applyWhere("host", Jdk.Field.HOST_ID + " = '" + hostId + "'");
	}

	@Override
	protected JdbcNamed getNamed() {
		return SpringBean.getBean(JdbcNamed.class);
	}

}
