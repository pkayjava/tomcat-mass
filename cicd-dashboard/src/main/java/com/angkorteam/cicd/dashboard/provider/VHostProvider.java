package com.angkorteam.cicd.dashboard.provider;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.angkorteam.cicd.ddl.TomcatVHost;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.spring.JdbcTemplate;
import com.angkorteam.framework.wicket.markup.html.form.SmartTextProvider;

public class VHostProvider implements SmartTextProvider {

	private String tomcatId;

	public VHostProvider(String tomcatId) {
		this.tomcatId = tomcatId;
	}

	@Override
	public List<String> toChoices(String input) {
		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
		return jdbcTemplate.queryForList("SELECT " + TomcatVHost.Field.NAME + " FROM " + TomcatVHost.NAME + " WHERE " + TomcatVHost.Field.NAME + " LIKE ? AND " + TomcatVHost.Field.TOMCAT_ID + " = ?", String.class, StringUtils.lowerCase(input + "%"), this.tomcatId);
	}

}
