package com.angkorteam.cicd.dashboard.pages.jdk;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.ValidationError;

import com.angkorteam.cicd.dashboard.Functions;
import com.angkorteam.cicd.dashboard.Repository;
import com.angkorteam.cicd.dashboard.WicketSession;
import com.angkorteam.cicd.dashboard.pages.Page;
import com.angkorteam.cicd.dashboard.provider.HostProvider;
import com.angkorteam.cicd.dashboard.spring.JmsMessageProducer;
import com.angkorteam.cicd.dashboard.spring.JmsSession;
import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.cicd.ddl.Jdk;
import com.angkorteam.cicd.xml.command.RestartJdkCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.InsertQuery;
import com.angkorteam.framework.models.PageBreadcrumb;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.spring.JdbcTemplate;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.angkorteam.framework.wicket.markup.html.form.Button;
import com.angkorteam.framework.wicket.markup.html.form.Form;
import com.angkorteam.framework.wicket.markup.html.form.select2.Option;
import com.angkorteam.framework.wicket.markup.html.form.select2.Select2MultipleChoice;
import com.google.common.collect.Lists;

@AuthorizeInstantiation(WicketSession.ALL_FUNCTION)
public class JdkCreatePage extends Page {

	protected Form<Void> form;
	protected Button saveButton;
	protected BookmarkablePageLink<Void> closeLink;

	protected UIRow row1;

	protected UIBlock jdkBlock;
	protected UIContainer jdkIContainer;
	protected List<FileUpload> jdkValue;
	protected FileUploadField jdkField;

	protected HostProvider hostProvider;
	protected UIBlock hostBlock;
	protected UIContainer hostIContainer;
	protected List<Option> hostValue;
	protected Select2MultipleChoice<Option> hostField;

	@Override
	public IModel<List<PageBreadcrumb>> buildPageBreadcrumb() {
		List<PageBreadcrumb> BREADCRUMB = Lists.newArrayList();
		{
			BREADCRUMB.add(new PageBreadcrumb.Builder().withLabel(Model.of("Jdk")).build());
		}
		return Model.ofList(BREADCRUMB);
	}

	@Override
	protected void initData() {
		this.hostProvider = new HostProvider(getSession().getUserId());
	}

	@Override
	protected void initComponent() {
		this.form = new Form<>("form");
		add(this.form);

		this.saveButton = new Button("saveButton");
		this.saveButton.setOnSubmit(this::saveButtonSubmit);
		this.form.add(this.saveButton);

		this.closeLink = new BookmarkablePageLink<>("closeLink", JdkBrowsePage.class);
		this.form.add(this.closeLink);

		this.row1 = UIRow.newUIRow("row1", this.form);

		this.jdkBlock = this.row1.newUIBlock("jdkBlock", Size.Six_6);
		this.jdkIContainer = this.jdkBlock.newUIContainer("jdkIContainer");
		this.jdkField = new FileUploadField("jdkField", new PropertyModel<>(this, "jdkValue"));
		this.jdkIContainer.add(this.jdkField);
		this.jdkIContainer.newFeedback("jdkFeedback", this.jdkField);

		this.hostBlock = this.row1.newUIBlock("hostBlock", Size.Six_6);
		this.hostIContainer = this.hostBlock.newUIContainer("hostIContainer");
		this.hostField = new Select2MultipleChoice<>("hostField", new PropertyModel<>(this, "hostValue"), this.hostProvider);
		this.hostIContainer.add(this.hostField);
		this.hostIContainer.newFeedback("hostFeedback", this.hostField);

	}

	@Override
	protected void configureMetaData() {
		this.jdkField.setRequired(true);
		this.jdkField.add(this::jdkValidation);

		this.hostField.setRequired(true);
	}

	protected void jdkValidation(IValidatable<List<FileUpload>> validatable) {
		List<FileUpload> values = validatable.getValue();
		if (values != null && !values.isEmpty()) {
			FileUpload value = values.get(0);
			String clientFileName = StringUtils.lowerCase(value.getClientFileName());
			if (!clientFileName.endsWith(".tar.gz")) {
				validatable.error(new ValidationError("accept only tar.gz"));
			}
		}
	}

	public void saveButtonSubmit(Button button) {
		File tmp = new File(FileUtils.getTempDirectory(), System.currentTimeMillis() + "");

		try {

			JdbcNamed jdbcNamed = SpringBean.getBean(JdbcNamed.class);

			String clientFileName = this.jdkField.getFileUpload().getClientFileName();

			tmp.mkdirs();
			File jdkTemp = new File(tmp, this.jdkField.getFileUpload().getClientFileName());
			this.jdkField.getFileUpload().writeTo(jdkTemp);
			File untar = new File(tmp, "untar");
			untar.mkdirs();
			Functions.unTarGz(jdkTemp, untar);
			Properties prop = new Properties();
			try (FileInputStream stream = new FileInputStream(new File(untar.listFiles()[0], "release"))) {
				prop.load(stream);
			}

			String javaVersion = prop.getProperty("JAVA_VERSION", "N/A");
			if (javaVersion.startsWith("\"1.7.0") || javaVersion.startsWith("1.7.0") || javaVersion.startsWith("\"1.8.0") || javaVersion.startsWith("1.8.0")) {
			} else {
				this.jdkField.error(new ValidationError(javaVersion + " is not supported"));
				return;
			}

			String fileId = Repository.saveFile("tar.gz", jdkTemp);

			for (Option host : this.hostValue) {
				InsertQuery insertQuery = new InsertQuery(Jdk.NAME);
				insertQuery.addValue(Jdk.Field.ID + " = uuid()");
				insertQuery.addValue(Jdk.Field.HOST_ID + " = :host_id", host.getId());
				insertQuery.addValue(Jdk.Field.JDK_NAME + " = :jdk_name", clientFileName.substring(0, clientFileName.length() - 7));
				insertQuery.addValue(Jdk.Field.FILE_ID + " = :file_id", fileId);
				insertQuery.addValue(Jdk.Field.USER_ID + " = :user_id", getSession().getUserId());
				jdbcNamed.update(insertQuery.toSQL(), insertQuery.getParam());
			}

			JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
			jdbcTemplate.getDataSource().getConnection().commit();

			JmsSession session = SpringBean.getBean(JmsSession.class);

			for (Option host : this.hostValue) {
				String clientId = jdbcTemplate.queryForObject("SELECT " + Host.Field.CLIENT_ID + " FROM " + Host.NAME + " WHERE " + Host.Field.ID + " = ?", String.class, host.getId());

				try (JmsMessageProducer producer = session.createProducer(clientId)) {
					RestartJdkCommand command = new RestartJdkCommand();
					producer.send(new XmlMessage(command).toText());
				} catch (IOException e) {
					throw new WicketRuntimeException(e);
				}
			}

			setResponsePage(JdkBrowsePage.class);
		} catch (Throwable e) {
			throw new WicketRuntimeException(e);
		} finally {
			FileUtils.deleteQuietly(tmp);
		}
	}

}
