package com.angkorteam.cicd.dashboard.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.configuration.XMLPropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.angkorteam.cicd.dashboard.spring.JmsMessageProducer;
import com.angkorteam.cicd.dashboard.spring.JmsSession;
import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.cicd.ddl.Tomcat;
import com.angkorteam.cicd.ddl.TomcatFile;
import com.angkorteam.cicd.xml.command.RestartCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;
import com.angkorteam.framework.Function;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.InsertQuery;
import com.angkorteam.framework.jdbc.SelectQuery;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.spring.JdbcTemplate;

@Controller
public class DeployController {

	@RequestMapping(path = "/deploy/war/{clientId}/{instance}", method = RequestMethod.POST)
	public void deployWar(Principal principal, @PathVariable("clientId") String clientId, @PathVariable("instance") String instance, @RequestParam("vhost") String vhost, @RequestPart("warFile") MultipartFile file, HttpServletRequest request, HttpServletResponse response) throws IOException {
		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);
		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		Map<String, Object> hostObject = jdbcTemplate.queryForMap("SELECT " + Host.Field.ID + ", " + Host.Field.USER_ID + " FROM " + Host.NAME + " WHERE " + Host.Field.CLIENT_ID + " = ?", clientId);
		if (hostObject == null || !principal.getName().equals(hostObject.get(Host.Field.USER_ID))) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}

		SelectQuery selectQuery = new SelectQuery(Tomcat.NAME);

		selectQuery.addWhere(Tomcat.Field.HOST_ID + " = :hostId", hostObject.get(Host.Field.ID));
		selectQuery.addWhere(Tomcat.Field.INSTANCE + " = :instanceId", instance);

		selectQuery.addField(Tomcat.Field.ID);

		String tomcatId = named.queryForObject(selectQuery.toSQL(), selectQuery.getParam(), String.class);
		if (tomcatId == null || "".equals(tomcatId)) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}

		XMLPropertiesConfiguration configuration = SpringBean.getBean(XMLPropertiesConfiguration.class);
		String resourceRepo = configuration.getString("resource.repo");
		java.io.File resourceRepoFile = new java.io.File(resourceRepo);
		resourceRepoFile.mkdirs();
		String pattern = DateFormatUtils.format(new Date(), "yyyy/MM/dd");
		java.io.File patternFolder = new java.io.File(resourceRepoFile, pattern);
		patternFolder.mkdirs();

		String fileId = jdbcTemplate.queryForObject("SELECT uuid() FROM DUAL", String.class);

		String clientFileName = file.getOriginalFilename();

		java.io.File warFile = new java.io.File(patternFolder, fileId + "_" + clientFileName);

		file.transferTo(warFile);

		String fileSha = null;

		try (FileInputStream stream = new FileInputStream(warFile)) {
			fileSha = DigestUtils.sha512Hex(stream);
		}

		String type = FilenameUtils.getExtension(warFile.getName());

		jdbcTemplate.update("DELETE FROM " + TomcatFile.NAME + " WHERE " + TomcatFile.Field.TOMCAT_ID + " = ? AND " + TomcatFile.Field.PATH + " = ?", tomcatId, "/webapps/" + vhost + "/" + clientFileName);

		InsertQuery insertQuery = null;
		insertQuery = new InsertQuery(com.angkorteam.cicd.ddl.File.NAME);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.ID + " = :id", fileId);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.USER_ID + " = :user_id", principal.getName());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.PATH + " = :path", warFile.getAbsolutePath());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.NAME + " = :name", warFile.getName());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.TYPE + " = :type", type);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.FILE_SIZE + " = :file_size", FileUtils.byteCountToDisplaySize(warFile.length()));
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.FILE_SHA + " = :file_sha", fileSha);
		named.update(insertQuery.toSQL(), insertQuery.getParam());

		insertQuery = new InsertQuery(TomcatFile.NAME);
		insertQuery.addValue(TomcatFile.Field.ID + " = uuid()");
		insertQuery.addValue(TomcatFile.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
		insertQuery.addValue(TomcatFile.Field.FILE_ID + " = :file_id", fileId);
		insertQuery.addValue(TomcatFile.Field.PATH + " = :path", "/webapps/" + vhost + "/" + clientFileName);
		insertQuery.addValue(TomcatFile.Field.LAST_MODIFIED + " = :last_modified", new Date());
		insertQuery.addValue(TomcatFile.Field.VHOST + " = :vhost", vhost);
		insertQuery.addValue(TomcatFile.Field.TYPE + " = :type", TomcatFile.TYPE_WAR);
		insertQuery.addValue(TomcatFile.Field.READONLY + " = :readonly", false);
		insertQuery.addValue(TomcatFile.Field.STATUS + " = :status", TomcatFile.STATUS_STOPPED);
		named.update(insertQuery.toSQL(), insertQuery.getParam());

		JmsSession session = SpringBean.getBean(JmsSession.class);

		try (JmsMessageProducer producer = session.createProducer(clientId)) {
			RestartCommand command = new RestartCommand();
			command.setInstance(instance);
			command.setServer(Function.getHttpAddress(request) + String.format("/api/server/%s/%s", clientId, instance));
			producer.send(new XmlMessage(command).toText());
		}

		response.getWriter().println("Successfully");

	}

	@RequestMapping(path = "/deploy/library/{clientId}/{instance}", method = RequestMethod.POST)
	public void deployLibrary(Principal principal, @PathVariable("clientId") String clientId, @PathVariable("instance") String instance, @RequestPart("jarFile") MultipartFile file, HttpServletRequest request, HttpServletResponse response) throws IOException {
		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);
		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		Map<String, Object> hostObject = jdbcTemplate.queryForMap("SELECT " + Host.Field.ID + ", " + Host.Field.USER_ID + " FROM " + Host.NAME + " WHERE " + Host.Field.CLIENT_ID + " = ?", clientId);
		if (hostObject == null || !principal.getName().equals(hostObject.get(Host.Field.USER_ID))) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}

		SelectQuery selectQuery = new SelectQuery(Tomcat.NAME);

		selectQuery.addWhere(Tomcat.Field.HOST_ID + " = :hostId", hostObject.get(Host.Field.ID));
		selectQuery.addWhere(Tomcat.Field.INSTANCE + " = :instanceId", instance);

		selectQuery.addField(Tomcat.Field.ID);

		String tomcatId = named.queryForObject(selectQuery.toSQL(), selectQuery.getParam(), String.class);
		if (tomcatId == null || "".equals(tomcatId)) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}

		XMLPropertiesConfiguration configuration = SpringBean.getBean(XMLPropertiesConfiguration.class);
		String resourceRepo = configuration.getString("resource.repo");
		java.io.File resourceRepoFile = new java.io.File(resourceRepo);
		resourceRepoFile.mkdirs();
		String pattern = DateFormatUtils.format(new Date(), "yyyy/MM/dd");
		java.io.File patternFolder = new java.io.File(resourceRepoFile, pattern);
		patternFolder.mkdirs();

		String fileId = jdbcTemplate.queryForObject("SELECT uuid() FROM DUAL", String.class);

		String clientFileName = file.getOriginalFilename();

		java.io.File warFile = new java.io.File(patternFolder, fileId + "_" + clientFileName);

		file.transferTo(warFile);

		String fileSha = null;

		try (FileInputStream stream = new FileInputStream(warFile)) {
			fileSha = DigestUtils.sha512Hex(stream);
		}

		String type = FilenameUtils.getExtension(warFile.getName());

		jdbcTemplate.update("DELETE FROM " + TomcatFile.NAME + " WHERE " + TomcatFile.Field.TOMCAT_ID + " = ? AND " + TomcatFile.Field.PATH + " = ?", tomcatId, "/lib/" + clientFileName);

		InsertQuery insertQuery = null;
		insertQuery = new InsertQuery(com.angkorteam.cicd.ddl.File.NAME);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.ID + " = :id", fileId);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.USER_ID + " = :user_id", principal.getName());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.PATH + " = :path", warFile.getAbsolutePath());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.NAME + " = :name", warFile.getName());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.TYPE + " = :type", type);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.FILE_SIZE + " = :file_size", FileUtils.byteCountToDisplaySize(warFile.length()));
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.FILE_SHA + " = :file_sha", fileSha);
		named.update(insertQuery.toSQL(), insertQuery.getParam());

		insertQuery = new InsertQuery(TomcatFile.NAME);
		insertQuery.addValue(TomcatFile.Field.ID + " = uuid()");
		insertQuery.addValue(TomcatFile.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
		insertQuery.addValue(TomcatFile.Field.FILE_ID + " = :file_id", fileId);
		insertQuery.addValue(TomcatFile.Field.PATH + " = :path", "/lib/" + clientFileName);
		insertQuery.addValue(TomcatFile.Field.LAST_MODIFIED + " = :last_modified", new Date());
		insertQuery.addValue(TomcatFile.Field.TYPE + " = :type", TomcatFile.TYPE_WAR);
		insertQuery.addValue(TomcatFile.Field.READONLY + " = :readonly", false);
		insertQuery.addValue(TomcatFile.Field.STATUS + " = :status", TomcatFile.STATUS_STOPPED);
		named.update(insertQuery.toSQL(), insertQuery.getParam());

		JmsSession session = SpringBean.getBean(JmsSession.class);

		try (JmsMessageProducer producer = session.createProducer(clientId)) {
			RestartCommand command = new RestartCommand();
			command.setInstance(instance);
			command.setServer(Function.getHttpAddress(request) + String.format("/api/server/%s/%s", clientId, instance));
			producer.send(new XmlMessage(command).toText());
		}

		response.getWriter().println("Successfully");

	}

}
