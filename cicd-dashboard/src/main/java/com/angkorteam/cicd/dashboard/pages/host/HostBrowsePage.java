package com.angkorteam.cicd.dashboard.pages.host;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.angkorteam.cicd.dashboard.WicketSession;
import com.angkorteam.cicd.dashboard.pages.Page;
import com.angkorteam.cicd.dashboard.pages.tomcat.TomcatBrowsePage;
import com.angkorteam.cicd.dashboard.provider.JdbcProvider;
import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.framework.models.PageBreadcrumb;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.cell.TextCell;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.FilterToolbar;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemClass;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemPanel;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.TextFilterColumn;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.google.common.collect.Lists;

@AuthorizeInstantiation(WicketSession.ALL_FUNCTION)
public class HostBrowsePage extends Page {

	protected FilterForm<Map<String, String>> form;

	protected UIRow row1;

	protected UIBlock dataBlock;
	protected UIContainer dataIContainer;
	protected DataTable<Map<String, Object>, String> dataTable;
	protected JdbcProvider dataProvider;
	protected List<IColumn<Map<String, Object>, String>> dataColumn;

	protected BookmarkablePageLink<Void> createLink;

	protected BookmarkablePageLink<Void> tomcatLink;

	@Override
	public IModel<List<PageBreadcrumb>> buildPageBreadcrumb() {
		List<PageBreadcrumb> BREADCRUMB = Lists.newArrayList();
		{
			BREADCRUMB.add(new PageBreadcrumb.Builder().withLabel(Model.of("Host")).build());
		}
		return Model.ofList(BREADCRUMB);
	}

	@Override
	protected void initComponent() {
		this.createLink = new BookmarkablePageLink<>("createLink", HostCreatePage.class);
		add(this.createLink);

		this.tomcatLink = new BookmarkablePageLink<>("tomcatLink", TomcatBrowsePage.class);
		add(this.tomcatLink);

		this.form = new FilterForm<>("form", this.dataProvider);
		add(this.form);

		this.row1 = UIRow.newUIRow("row1", this.form);

		this.dataBlock = this.row1.newUIBlock("dataBlock", Size.Twelve_12);
		this.dataIContainer = this.dataBlock.newUIContainer("dataIContainer");
		this.dataTable = new DefaultDataTable<>("dataTable", this.dataColumn, this.dataProvider, 20);
		this.dataTable.addTopToolbar(new FilterToolbar(this.dataTable, this.form));
		this.dataIContainer.add(this.dataTable);
	}

	@Override
	protected void configureMetaData() {
	}

	@Override
	protected void initData() {
		this.dataProvider = new JdbcProvider(Host.NAME);
		this.dataProvider.boardField(Host.Field.ID, "id", String.class);
		this.dataProvider.boardField(Host.Field.CLIENT_ID, "name", String.class);
		this.dataProvider.boardField(Host.Field.IP_ADDRESS, "ip", String.class);
		this.dataProvider.boardField(Host.Field.OS_NAME, "os_name", String.class);
		this.dataProvider.boardField(Host.Field.OS_VERSION, "os_version", String.class);
		this.dataProvider.boardField(Host.Field.OS_ARCH, "os_arch", String.class);
		this.dataProvider.boardField(Host.Field.CONNECTED, "connected", String.class);

		this.dataProvider.selectField("id", String.class);

		this.dataProvider.applyWhere("user_id", Host.Field.USER_ID + " = '" + getSession().getUserId() + "'");

		this.dataColumn = new ArrayList<>();
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Name"), "name", "name", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("OS"), "os_name", "os_name", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Version"), "os_version", "os_version", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Arch"), "os_arch", "os_arch", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Connected"), "connected", "connected", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("IP"), "ip", "ip", this::dataColumn));
	}

	protected ItemPanel dataColumn(String column, IModel<String> display, Map<String, Object> model) {
		if ("os_name".equals(column) || "os_version".equals(column) || "ip".equals(column) || "os_arch".equals(column) || "name".equals(column)) {
			String value = (String) model.get(column);
			return new TextCell(value);
		} else if ("connected".equals(column)) {
			Boolean value = (Boolean) model.get(column);
			return new TextCell(value);
		}
		throw new WicketRuntimeException("Unknown " + column);
	}

}
