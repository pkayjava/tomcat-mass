package com.angkorteam.cicd.dashboard.spring;

import java.sql.SQLException;

import org.apache.commons.configuration.XMLPropertiesConfiguration;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.WicketRuntimeException;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import com.angkorteam.framework.HttpServletRequestDataSource;
import com.angkorteam.framework.spring.Delegate;

public class FlywayFactoryBean implements FactoryBean<Flyway>, InitializingBean {

	private Delegate delegate;

	private Flyway flyway;

	private XMLPropertiesConfiguration configuration;

	@Override
	public Flyway getObject() throws Exception {
		return this.flyway;
	}

	@Override
	public Class<?> getObjectType() {
		return Flyway.class;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		RuntimeConfigurationType type = RuntimeConfigurationType.valueOf(this.configuration.getString("wicket"));

		HttpServletRequestDataSource dataSource = new HttpServletRequestDataSource(this.delegate);

		// Create the Flyway instance
		this.flyway = new Flyway();
		flyway.setBaselineOnMigrate(true);
		if (type == RuntimeConfigurationType.DEVELOPMENT) {
			flyway.setCleanOnValidationError(true);
		}

		// Point it to the database
		flyway.setDataSource(dataSource);

		// Start the migration
		flyway.migrate();

		try {
			dataSource.getConnection().close();
		} catch (SQLException e) {
			throw new WicketRuntimeException(e);
		}
	}

	public void setDelegate(Delegate delegate) {
		this.delegate = delegate;
	}

	public void setConfiguration(XMLPropertiesConfiguration configuration) {
		this.configuration = configuration;
	}

}
