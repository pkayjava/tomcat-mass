package com.angkorteam.cicd.dashboard.tab.tomcat.preview;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.Page;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import com.angkorteam.cicd.dashboard.provider.JdbcProvider;
import com.angkorteam.cicd.ddl.Tomcat;
import com.angkorteam.cicd.ddl.TomcatVHost;
import com.angkorteam.cicd.ddl.TomcatVHostProtocol;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.spring.JdbcTemplate;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.cell.TextCell;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.FilterToolbar;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemClass;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemPanel;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.TextFilterColumn;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.angkorteam.framework.wicket.markup.html.panel.Panel;

public class ProtocolPanel extends Panel {

	protected String hostId;
	protected String tomcatId;
	protected String instance;

	protected Page itemPage;

	protected FilterForm<Map<String, String>> form;

	protected UIRow row1;

	protected UIBlock dataBlock;
	protected UIContainer dataIContainer;
	protected DataTable<Map<String, Object>, String> dataTable;
	protected JdbcProvider dataProvider;
	protected List<IColumn<Map<String, Object>, String>> dataColumn;

	public ProtocolPanel(String id, Page itemPage) {
		super(id);
		this.itemPage = itemPage;
	}

	@Override
	protected void initComponent() {
		this.form = new FilterForm<>("form", this.dataProvider);
		add(this.form);

		this.row1 = UIRow.newUIRow("row1", this.form);

		this.dataBlock = this.row1.newUIBlock("dataBlock", Size.Twelve_12);
		this.dataIContainer = this.dataBlock.newUIContainer("dataIContainer");
		this.dataTable = new DefaultDataTable<>("dataTable", this.dataColumn, this.dataProvider, 20);
		this.dataTable.addTopToolbar(new FilterToolbar(this.dataTable, this.form));
		this.dataIContainer.add(this.dataTable);
	}

	@Override
	protected void configureMetaData() {
	}

	@Override
	protected void initData() {
		this.tomcatId = new PropertyModel<String>(this.itemPage, "tomcatId").getObject();

		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		Map<String, Object> tomcatObject = jdbcTemplate.queryForMap("SELECT * FROM " + Tomcat.NAME + " WHERE " + Tomcat.Field.ID + " = ?", this.tomcatId);
		this.hostId = (String) tomcatObject.get(Tomcat.Field.HOST_ID);
		this.instance = (String) tomcatObject.get(Tomcat.Field.INSTANCE);

		this.dataProvider = new JdbcProvider(TomcatVHostProtocol.NAME);
		this.dataProvider.applyJoin("vhost", "INNER JOIN " + TomcatVHost.NAME + " ON " + TomcatVHostProtocol.NAME + "." + TomcatVHostProtocol.Field.TOMCAT_VHOST_ID + " = " + TomcatVHost.NAME + "." + TomcatVHost.Field.ID);
		this.dataProvider.boardField(TomcatVHost.NAME + "." + TomcatVHost.Field.NAME, "vhost", String.class);
		this.dataProvider.boardField(TomcatVHostProtocol.NAME + "." + TomcatVHostProtocol.Field.NAME, "protocol", String.class);

		this.dataProvider.selectField("id", String.class);

		this.dataProvider.applyWhere("tomcat_id", TomcatVHost.NAME + "." + TomcatVHost.Field.TOMCAT_ID + " = '" + this.tomcatId + "'");

		this.dataColumn = new ArrayList<>();
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("VHost"), "vhost", "vhost", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Protocol"), "protocol", "protocol", this::dataColumn));
	}

	protected ItemPanel dataColumn(String column, IModel<String> display, Map<String, Object> model) {
		if ("vhost".equals(column) || "protocol".equals(column)) {
			String value = (String) model.get(column);
			return new TextCell(value);
		}
		throw new WicketRuntimeException("Unknown " + column);
	}

}
