package com.angkorteam.cicd.dashboard;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.configuration.XMLPropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.form.upload.FileUpload;

import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.InsertQuery;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.spring.JdbcTemplate;

public class Repository {

	public static String saveFile(FileUpload upload) {
		return saveFile(FilenameUtils.getExtension(upload.getClientFileName()), upload);
	}

	public static String saveFile(String extension, FileUpload upload) {
		XMLPropertiesConfiguration configuration = SpringBean.getBean(XMLPropertiesConfiguration.class);
		String resourceRepo = configuration.getString("resource.repo");
		File resourceRepoFile = new File(resourceRepo);
		resourceRepoFile.mkdirs();
		String pattern = DateFormatUtils.format(new Date(), "yyyy/MM/dd");
		File patternFolder = new File(resourceRepoFile, pattern);
		patternFolder.mkdirs();

		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);

		String fileId = jdbcTemplate.queryForObject("SELECT uuid() FROM DUAL", String.class);

		File destFile = new File(patternFolder, fileId + "_" + upload.getClientFileName());

		try {
			upload.writeTo(destFile);
		} catch (Exception e) {
			throw new WicketRuntimeException(e);
		}

		String fileSha = sha512Hex(destFile);

		InsertQuery insertQuery = new InsertQuery(com.angkorteam.cicd.ddl.File.NAME);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.ID + " = :id", fileId);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.USER_ID + " = :user_id", ((WicketSession) WicketSession.get()).getUserId());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.PATH + " = :path", destFile.getAbsolutePath());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.NAME + " = :name", upload.getClientFileName());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.TYPE + " = :type", extension);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.FILE_SIZE + " = :file_size", FileUtils.byteCountToDisplaySize(destFile.length()));
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.FILE_SHA + " = :file_sha", fileSha);
		named.update(insertQuery.toSQL(), insertQuery.getParam());

		return fileId;
	}

	public static String saveFile(String name, String content) {
		XMLPropertiesConfiguration configuration = SpringBean.getBean(XMLPropertiesConfiguration.class);
		String resourceRepo = configuration.getString("resource.repo");
		File resourceRepoFile = new File(resourceRepo);
		resourceRepoFile.mkdirs();
		String pattern = DateFormatUtils.format(new Date(), "yyyy/MM/dd");
		File patternFolder = new File(resourceRepoFile, pattern);
		patternFolder.mkdirs();

		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);

		String fileId = jdbcTemplate.queryForObject("SELECT uuid() FROM DUAL", String.class);

		File destFile = new File(patternFolder, fileId + "_" + name);

		try {
			FileUtils.writeStringToFile(destFile, content, "UTF-8");
		} catch (Exception e) {
			throw new WicketRuntimeException(e);
		}

		String fileSha = sha512Hex(destFile);

		InsertQuery insertQuery = new InsertQuery(com.angkorteam.cicd.ddl.File.NAME);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.ID + " = :id", fileId);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.USER_ID + " = :user_id", ((WicketSession) WicketSession.get()).getUserId());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.PATH + " = :path", destFile.getAbsolutePath());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.NAME + " = :name", name);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.TYPE + " = :type", FilenameUtils.getExtension(destFile.getName()));
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.FILE_SIZE + " = :file_size", FileUtils.byteCountToDisplaySize(destFile.length()));
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.FILE_SHA + " = :file_sha", fileSha);
		named.update(insertQuery.toSQL(), insertQuery.getParam());

		return fileId;
	}

	public static String saveFile(File upload) {
		return saveFile(FilenameUtils.getExtension(upload.getName()), upload);
	}

	public static String saveFile(String extension, File upload) {
		XMLPropertiesConfiguration configuration = SpringBean.getBean(XMLPropertiesConfiguration.class);
		String resourceRepo = configuration.getString("resource.repo");
		File resourceRepoFile = new File(resourceRepo);
		resourceRepoFile.mkdirs();
		String pattern = DateFormatUtils.format(new Date(), "yyyy/MM/dd");
		File patternFolder = new File(resourceRepoFile, pattern);
		patternFolder.mkdirs();

		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);

		String fileId = jdbcTemplate.queryForObject("SELECT uuid() FROM DUAL", String.class);

		File destFile = new File(patternFolder, fileId + "_" + upload.getName());

		try {
			FileUtils.copyFile(upload, destFile);
		} catch (Exception e) {
			throw new WicketRuntimeException(e);
		}

		String fileSha = sha512Hex(destFile);

		InsertQuery insertQuery = new InsertQuery(com.angkorteam.cicd.ddl.File.NAME);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.ID + " = :id", fileId);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.USER_ID + " = :user_id", ((WicketSession) WicketSession.get()).getUserId());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.PATH + " = :path", destFile.getAbsolutePath());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.NAME + " = :name", upload.getName());
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.TYPE + " = :type", extension);
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.FILE_SIZE + " = :file_size", FileUtils.byteCountToDisplaySize(destFile.length()));
		insertQuery.addValue(com.angkorteam.cicd.ddl.File.Field.FILE_SHA + " = :file_sha", fileSha);
		named.update(insertQuery.toSQL(), insertQuery.getParam());

		return fileId;
	}

	public static String sha512Hex(File file) {
		try (FileInputStream stream = new FileInputStream(file)) {
			return DigestUtils.sha512Hex(stream);
		} catch (IOException e) {
			return null;
		}
	}

}
