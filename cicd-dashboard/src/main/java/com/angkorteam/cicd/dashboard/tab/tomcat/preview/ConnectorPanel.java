package com.angkorteam.cicd.dashboard.tab.tomcat.preview;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.Page;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import com.angkorteam.cicd.dashboard.provider.JdbcProvider;
import com.angkorteam.cicd.ddl.Tomcat;
import com.angkorteam.cicd.ddl.TomcatConnector;
import com.angkorteam.cicd.ddl.TomcatVHost;
import com.angkorteam.cicd.ddl.TomcatVHostPort;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.spring.JdbcTemplate;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.cell.TextCell;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.FilterToolbar;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemClass;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemPanel;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.TextFilterColumn;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.angkorteam.framework.wicket.markup.html.panel.Panel;

public class ConnectorPanel extends Panel {

	protected String hostId;
	protected String tomcatId;
	protected String instance;

	protected Page itemPage;

	protected FilterForm<Map<String, String>> form;

	protected UIRow row1;

	protected UIBlock dataBlock;
	protected UIContainer dataIContainer;
	protected DataTable<Map<String, Object>, String> dataTable;
	protected JdbcProvider dataProvider;
	protected List<IColumn<Map<String, Object>, String>> dataColumn;

	public ConnectorPanel(String id, Page itemPage) {
		super(id);
		this.itemPage = itemPage;
	}

	@Override
	protected void initComponent() {
		this.form = new FilterForm<>("form", this.dataProvider);
		add(this.form);

		this.row1 = UIRow.newUIRow("row1", this.form);

		this.dataBlock = this.row1.newUIBlock("dataBlock", Size.Twelve_12);
		this.dataIContainer = this.dataBlock.newUIContainer("dataIContainer");
		this.dataTable = new DefaultDataTable<>("dataTable", this.dataColumn, this.dataProvider, 20);
		this.dataTable.addTopToolbar(new FilterToolbar(this.dataTable, this.form));
		this.dataIContainer.add(this.dataTable);
	}

	@Override
	protected void configureMetaData() {
	}

	@Override
	protected void initData() {
		this.tomcatId = new PropertyModel<String>(this.itemPage, "tomcatId").getObject();

		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		Map<String, Object> tomcatObject = jdbcTemplate.queryForMap("SELECT * FROM " + Tomcat.NAME + " WHERE " + Tomcat.Field.ID + " = ?", this.tomcatId);
		this.hostId = (String) tomcatObject.get(Tomcat.Field.HOST_ID);
		this.instance = (String) tomcatObject.get(Tomcat.Field.INSTANCE);

		this.dataProvider = new JdbcProvider(TomcatVHostPort.NAME);
		this.dataProvider.applyJoin("vhost", "INNER JOIN " + TomcatVHost.NAME + " ON " + TomcatVHostPort.NAME + "." + TomcatVHostPort.Field.TOMCAT_VHOST_ID + " = " + TomcatVHost.NAME + "." + TomcatVHost.Field.ID);
		this.dataProvider.applyJoin("connector", "INNER JOIN " + TomcatConnector.NAME + " ON " + TomcatConnector.NAME + "." + TomcatConnector.Field.PORT + " = " + TomcatVHostPort.NAME + "." + TomcatVHostPort.Field.PORT);
		this.dataProvider.boardField(TomcatVHost.NAME + "." + TomcatVHost.Field.NAME, "vhost", String.class);
		this.dataProvider.boardField(TomcatConnector.NAME + "." + TomcatConnector.Field.PROTOCOL, "protocol", String.class);
		this.dataProvider.boardField(TomcatConnector.NAME + "." + TomcatConnector.Field.SSL_IMPLEMENTATION_NAME, "ssl_factory", String.class);
		this.dataProvider.boardField(TomcatVHostPort.NAME + "." + TomcatVHostPort.Field.PORT, "port", Long.class);
		this.dataProvider.boardField(TomcatVHostPort.NAME + "." + TomcatVHostPort.Field.TLS, "tls", Boolean.class);
		this.dataProvider.boardField(TomcatVHostPort.NAME + "." + TomcatVHostPort.Field.TLS_MODE, "tls_mode", String.class);

		this.dataProvider.selectField("id", String.class);

		this.dataProvider.applyWhere("tomcat_id", TomcatVHost.NAME + "." + TomcatVHost.Field.TOMCAT_ID + " = '" + this.tomcatId + "'");

		this.dataColumn = new ArrayList<>();
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("VHost"), "vhost", "vhost", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Protocol"), "protocol", "protocol", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("SSL Factory"), "ssl_factory", "ssl_factory", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.Long, Model.of("Port"), "port", "port", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.Boolean, Model.of("Secure"), "tls", "tls", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("TLS Mode"), "tls_mode", "tls_mode", this::dataColumn));
	}

	protected ItemPanel dataColumn(String column, IModel<String> display, Map<String, Object> model) {
		if ("vhost".equals(column) || "tls_mode".equals(column)) {
			String value = (String) model.get(column);
			return new TextCell(value);
		} else if ("ssl_factory".equals(column) || "protocol".equals(column)) {
			String value = (String) model.get(column);
			if (value != null && !"".equals(value) && value.indexOf('.') >= 0 && !value.contains("/")) {
				value = value.substring(value.lastIndexOf('.') + 1);
			}
			return new TextCell(value);
		} else if ("tls".equals(column)) {
			Boolean value = (Boolean) model.get(column);
			return new TextCell(value);
		} else if ("port".equals(column)) {
			Long value = (Long) model.get(column);
			return new TextCell(value);
		}
		throw new WicketRuntimeException("Unknown " + column);
	}

}
