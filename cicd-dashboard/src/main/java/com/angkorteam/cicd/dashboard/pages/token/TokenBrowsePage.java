package com.angkorteam.cicd.dashboard.pages.token;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.SortOrder;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.angkorteam.cicd.dashboard.WicketSession;
import com.angkorteam.cicd.ddl.Token;
import com.angkorteam.cicd.dashboard.pages.Page;
import com.angkorteam.cicd.dashboard.provider.JdbcProvider;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.InsertQuery;
import com.angkorteam.framework.jdbc.UpdateQuery;
import com.angkorteam.framework.models.PageBreadcrumb;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.cell.TextCell;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ActionFilterColumn;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ActionItem;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.Calendar;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.FilterToolbar;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemClass;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemCss;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemPanel;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.TextFilterColumn;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.angkorteam.framework.wicket.markup.html.link.Link;
import com.google.common.collect.Lists;

@AuthorizeInstantiation(WicketSession.ALL_FUNCTION)
public class TokenBrowsePage extends Page {

	protected FilterForm<Map<String, String>> form;

	protected UIRow row1;

	protected UIBlock dataBlock;
	protected UIContainer dataIContainer;
	protected DataTable<Map<String, Object>, String> dataTable;
	protected JdbcProvider dataProvider;
	protected List<IColumn<Map<String, Object>, String>> dataColumn;

	protected Link<Void> issueLink;

	@Override
	public IModel<List<PageBreadcrumb>> buildPageBreadcrumb() {
		List<PageBreadcrumb> BREADCRUMB = Lists.newArrayList();
		{
			BREADCRUMB.add(new PageBreadcrumb.Builder().withLabel(Model.of("Token")).build());
		}
		return Model.ofList(BREADCRUMB);
	}

	@Override
	protected void initComponent() {
		this.issueLink = new Link<>("issueLink");
		this.issueLink.setOnClick(this::issueLinkClick);
		add(this.issueLink);

		this.form = new FilterForm<>("form", this.dataProvider);
		add(this.form);

		this.row1 = UIRow.newUIRow("row1", this.form);

		this.dataBlock = this.row1.newUIBlock("dataBlock", Size.Twelve_12);
		this.dataIContainer = this.dataBlock.newUIContainer("dataIContainer");
		this.dataTable = new DefaultDataTable<>("dataTable", this.dataColumn, this.dataProvider, 20);
		this.dataTable.addTopToolbar(new FilterToolbar(this.dataTable, this.form));
		this.dataIContainer.add(this.dataTable);
	}

	@Override
	protected void configureMetaData() {
	}

	@Override
	protected void initData() {
		this.dataProvider = new JdbcProvider(Token.NAME);
		this.dataProvider.boardField(Token.Field.ID, "id", String.class);
		this.dataProvider.boardField(Token.Field.TOKEN_ID, "token_id", String.class);
		this.dataProvider.boardField(Token.Field.ISSUED_DATETIME, "issued_datetime", Calendar.DateTime);
		this.dataProvider.boardField(Token.Field.REVOKED, "revoked", Boolean.class);
		this.dataProvider.boardField(Token.Field.REVOKED_DATETIME, "revoked_datetime", Calendar.DateTime);
		this.dataProvider.selectField("id", String.class);

		this.dataProvider.setSort("issued_datetime", SortOrder.DESCENDING);

		this.dataColumn = new ArrayList<>();
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Token ID"), "token_id", "token_id", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.DateTime, Model.of("Issued Date"), "issued_datetime", "issued_datetime", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.Boolean, Model.of("Revoked"), "revoked", "revoked", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Revoked Date"), "revoked_datetime", "revoked_datetime", this::dataColumn));
		this.dataColumn.add(new ActionFilterColumn<>(Model.of("Action"), this::dataAction, this::dataClick));
	}

	protected void dataClick(String column, Map<String, Object> model, AjaxRequestTarget target) {
		if ("revoke".equals(column)) {
			String id = (String) model.get("id");
			UpdateQuery query = new UpdateQuery(Token.NAME);
			query.addValue(Token.Field.REVOKED + " = true");
			query.addValue(Token.Field.REVOKED_DATETIME + " = now()");
			query.addWhere(Token.Field.ID + " = :id", id);
			JdbcNamed named = SpringBean.getBean(JdbcNamed.class);
			named.update(query.toSQL(), query.getParam());
		}
		if (target != null) {
			target.add(this.dataTable);
		}
	}

	protected List<ActionItem> dataAction(String column, Map<String, Object> model) {
		List<ActionItem> actions = Lists.newArrayList();
		Boolean revoked = (Boolean) model.get("revoked");
		if (revoked == null || !revoked) {
			actions.add(new ActionItem("revoke", Model.of("Revoke"), ItemCss.PRIMARY));
		}
		return actions;
	}

	protected ItemPanel dataColumn(String column, IModel<String> display, Map<String, Object> model) {
		if ("token_id".equals(column)) {
			String value = (String) model.get(column);
			return new TextCell(value);
		} else if ("issued_datetime".equals(column) || "revoked_datetime".equals(column)) {
			Date value = (Date) model.get(column);
			return new TextCell(value, "yyyy-MM-dd'T'HH:mm:ssZZ");
		} else if ("revoked".equals(column)) {
			Boolean value = (Boolean) model.get(column);
			return new TextCell(value);
		}
		throw new WicketRuntimeException("Unknown " + column);
	}

	protected void issueLinkClick(Link<Void> link) {

		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);

		String tokenId = named.queryForObject("select uuid() from dual", String.class);
		String tokenSecret = named.queryForObject("select uuid() from dual", String.class);

		InsertQuery query = new InsertQuery(Token.NAME);
		query.addValue(Token.Field.ID + " = uuid()");
		query.addValue(Token.Field.ISSUED_DATETIME + " = now()");
		query.addValue(Token.Field.REVOKED + " = false");
		query.addValue(Token.Field.TOKEN_ID + " = :token_id", tokenId);
		query.addValue(Token.Field.TOKEN_SECRET + " = md5(:token_secret)", "token_secret", tokenSecret);
		query.addValue(Token.Field.USER_ID + " = :user_id", getSession().getUserId());

		named.update(query.toSQL(), query.getParam());
		TokenBrowsePage page = new TokenBrowsePage();
		page.info(String.format("token id %s", tokenId));
		page.info(String.format("token secret %s", tokenSecret));
		setResponsePage(page);
	}
}
