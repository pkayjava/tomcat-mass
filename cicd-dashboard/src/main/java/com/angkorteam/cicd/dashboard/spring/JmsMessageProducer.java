package com.angkorteam.cicd.dashboard.spring;

import java.io.Closeable;
import java.io.IOException;

import javax.jms.JMSException;
import javax.jms.Session;

public class JmsMessageProducer implements Closeable {

	private javax.jms.MessageProducer producer;

	private Session session;

	public JmsMessageProducer(Session session, javax.jms.MessageProducer producer) {
		this.producer = producer;
		this.session = session;
	}

	@Override
	public void close() throws IOException {
		if (this.producer != null) {
			try {
				this.producer.close();
			} catch (JMSException e) {
				throw new IOException(e);
			}
		}
	}

	public void send(String message) throws IOException {
		try {
			this.producer.send(this.session.createTextMessage(message));
		} catch (JMSException e) {
			throw new IOException(e);
		}
	}

}
