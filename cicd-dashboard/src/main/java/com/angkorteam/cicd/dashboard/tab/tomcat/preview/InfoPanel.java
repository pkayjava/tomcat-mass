package com.angkorteam.cicd.dashboard.tab.tomcat.preview;

import java.util.Map;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.angkorteam.cicd.dashboard.pages.tomcat.TomcatBrowsePage;
import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.cicd.ddl.Tomcat;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.spring.JdbcTemplate;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.angkorteam.framework.wicket.markup.html.panel.Panel;
import com.angkorteam.framework.wicket.widget.ReadOnlyView;

public class InfoPanel extends Panel {

	protected String hostId;
	protected String tomcatId;

	protected Page itemPage;

	protected BookmarkablePageLink<Void> closeLink;

	protected UIRow row1;

	protected UIBlock infoNameBlock;
	protected UIContainer infoNameVContainer;
	protected String infoNameValue;
	protected ReadOnlyView infoNameView;

	protected UIBlock infoVersionBlock;
	protected UIContainer infoVersionVContainer;
	protected String infoVersionValue;
	protected ReadOnlyView infoVersionView;

	protected UIBlock infoStatusBlock;
	protected UIContainer infoStatusVContainer;
	protected String infoStatusValue;
	protected ReadOnlyView infoStatusView;

	protected UIRow row2;

	protected UIBlock infoJvmBlock;
	protected UIContainer infoJvmVContainer;
	protected String infoJvmValue;
	protected ReadOnlyView infoJvmView;

	protected UIBlock infoVendorBlock;
	protected UIContainer infoVendorVContainer;
	protected String infoVendorValue;
	protected ReadOnlyView infoVendorView;

	protected UIBlock infoIpAddressBlock;
	protected UIContainer infoIpAddressVContainer;
	protected String infoIpAddressValue;
	protected ReadOnlyView infoIpAddressView;

	protected UIRow row3;

	protected UIBlock infoOsVersionBlock;
	protected UIContainer infoOsVersionVContainer;
	protected String infoOsVersionValue;
	protected ReadOnlyView infoOsVersionView;

	protected UIBlock infoOsNameBlock;
	protected UIContainer infoOsNameVContainer;
	protected String infoOsNameValue;
	protected ReadOnlyView infoOsNameView;

	protected UIBlock infoOsArchBlock;
	protected UIContainer infoOsArchVContainer;
	protected String infoOsArchValue;
	protected ReadOnlyView infoOsArchView;

	public InfoPanel(String id, Page itemPage) {
		super(id);
		this.itemPage = itemPage;
	}

	@Override
	protected void initData() {
		this.tomcatId = new PropertyModel<String>(this.itemPage, "tomcatId").getObject();

		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
		Map<String, Object> tomcatObject = jdbcTemplate.queryForMap("SELECT * FROM " + Tomcat.NAME + " WHERE " + Tomcat.Field.ID + " = ?", this.tomcatId);

		this.hostId = (String) tomcatObject.get(Tomcat.Field.HOST_ID);

		Map<String, Object> hostObject = jdbcTemplate.queryForMap("SELECT * FROM " + Host.NAME + " WHERE " + Host.Field.ID + " = ?", this.hostId);

		this.infoNameValue = (String) tomcatObject.get(Tomcat.Field.INSTANCE);
		this.infoVersionValue = (String) tomcatObject.get(Tomcat.Field.TOMCAT_VERSION);
		this.infoJvmValue = (String) tomcatObject.get(Tomcat.Field.JVM_VERSION);
		this.infoVendorValue = (String) tomcatObject.get(Tomcat.Field.JVM_VENDOR);
		this.infoStatusValue = (String) tomcatObject.get(Tomcat.Field.STATUS);

		this.infoIpAddressValue = (String) hostObject.get(Host.Field.IP_ADDRESS);
		this.infoOsVersionValue = (String) hostObject.get(Host.Field.OS_VERSION);
		this.infoOsNameValue = (String) hostObject.get(Host.Field.OS_NAME);
		this.infoOsArchValue = (String) hostObject.get(Host.Field.OS_ARCH);

	}

	@Override
	protected void initComponent() {
		PageParameters parameters = new PageParameters();
		parameters.add("hostId", this.hostId);
		this.closeLink = new BookmarkablePageLink<>("closeLink", TomcatBrowsePage.class, parameters);
		add(this.closeLink);

		this.row1 = UIRow.newUIRow("row1", this);

		this.infoNameBlock = this.row1.newUIBlock("infoNameBlock", Size.Four_4);
		this.infoNameVContainer = this.infoNameBlock.newUIContainer("infoNameVContainer");
		this.infoNameView = new ReadOnlyView("infoNameView", new PropertyModel<>(this, "infoNameValue"));
		this.infoNameVContainer.add(this.infoNameView);

		this.infoVersionBlock = this.row1.newUIBlock("infoVersionBlock", Size.Four_4);
		this.infoVersionVContainer = this.infoVersionBlock.newUIContainer("infoVersionVContainer");
		this.infoVersionView = new ReadOnlyView("infoVersionView", new PropertyModel<>(this, "infoVersionValue"));
		this.infoVersionVContainer.add(this.infoVersionView);

		this.infoStatusBlock = this.row1.newUIBlock("infoStatusBlock", Size.Four_4);
		this.infoStatusVContainer = this.infoStatusBlock.newUIContainer("infoStatusVContainer");
		this.infoStatusView = new ReadOnlyView("infoStatusView", new PropertyModel<>(this, "infoStatusValue"));
		this.infoStatusVContainer.add(this.infoStatusView);

		this.row2 = UIRow.newUIRow("row2", this);

		this.infoJvmBlock = this.row2.newUIBlock("infoJvmBlock", Size.Four_4);
		this.infoJvmVContainer = this.infoJvmBlock.newUIContainer("infoJvmVContainer");
		this.infoJvmView = new ReadOnlyView("infoJvmView", new PropertyModel<>(this, "infoJvmValue"));
		this.infoJvmVContainer.add(this.infoJvmView);

		this.infoVendorBlock = this.row2.newUIBlock("infoVendorBlock", Size.Four_4);
		this.infoVendorVContainer = this.infoVendorBlock.newUIContainer("infoVendorVContainer");
		this.infoVendorView = new ReadOnlyView("infoVendorView", new PropertyModel<>(this, "infoVendorValue"));
		this.infoVendorVContainer.add(this.infoVendorView);

		this.infoIpAddressBlock = this.row2.newUIBlock("infoIpAddressBlock", Size.Four_4);
		this.infoIpAddressVContainer = this.infoIpAddressBlock.newUIContainer("infoIpAddressVContainer");
		this.infoIpAddressView = new ReadOnlyView("infoIpAddressView", new PropertyModel<>(this, "infoIpAddressValue"));
		this.infoIpAddressVContainer.add(this.infoIpAddressView);

		this.row3 = UIRow.newUIRow("row3", this);

		this.infoOsNameBlock = this.row3.newUIBlock("infoOsNameBlock", Size.Four_4);
		this.infoOsNameVContainer = this.infoOsNameBlock.newUIContainer("infoOsNameVContainer");
		this.infoOsNameView = new ReadOnlyView("infoOsNameView", new PropertyModel<>(this, "infoOsNameValue"));
		this.infoOsNameVContainer.add(this.infoOsNameView);

		this.infoOsArchBlock = this.row3.newUIBlock("infoOsArchBlock", Size.Four_4);
		this.infoOsArchVContainer = this.infoOsArchBlock.newUIContainer("infoOsArchVContainer");
		this.infoOsArchView = new ReadOnlyView("infoOsArchView", new PropertyModel<>(this, "infoOsArchValue"));
		this.infoOsArchVContainer.add(this.infoOsArchView);

		this.infoOsVersionBlock = this.row3.newUIBlock("infoOsVersionBlock", Size.Four_4);
		this.infoOsVersionVContainer = this.infoOsVersionBlock.newUIContainer("infoOsVersionVContainer");
		this.infoOsVersionView = new ReadOnlyView("infoOsVersionView", new PropertyModel<>(this, "infoOsVersionValue"));
		this.infoOsVersionVContainer.add(this.infoOsVersionView);

	}

	@Override
	protected void configureMetaData() {
	}

}
