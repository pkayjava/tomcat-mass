package com.angkorteam.cicd.dashboard.tab.tomcat.preview;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.coyote.ajp.AjpAprProtocol;
import org.apache.coyote.ajp.AjpNio2Protocol;
import org.apache.coyote.ajp.AjpNioProtocol;
import org.apache.coyote.http11.Http11AprProtocol;
import org.apache.coyote.http11.Http11Nio2Protocol;
import org.apache.coyote.http11.Http11NioProtocol;
import org.apache.coyote.http2.Http2Protocol;
import org.apache.wicket.Page;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.ValidationError;

import com.angkorteam.cicd.dashboard.Repository;
import com.angkorteam.cicd.dashboard.WicketSession;
import com.angkorteam.cicd.dashboard.pages.tomcat.TomcatBrowsePage;
import com.angkorteam.cicd.dashboard.provider.JdkProvider;
import com.angkorteam.cicd.dashboard.spring.JmsMessageProducer;
import com.angkorteam.cicd.dashboard.spring.JmsSession;
import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.cicd.ddl.HostPort;
import com.angkorteam.cicd.ddl.HostSsl;
import com.angkorteam.cicd.ddl.Jdk;
import com.angkorteam.cicd.ddl.Tomcat;
import com.angkorteam.cicd.ddl.TomcatConnector;
import com.angkorteam.cicd.ddl.TomcatFile;
import com.angkorteam.cicd.ddl.TomcatVHost;
import com.angkorteam.cicd.ddl.TomcatVHostCipher;
import com.angkorteam.cicd.ddl.TomcatVHostPort;
import com.angkorteam.cicd.ddl.TomcatVHostProtocol;
import com.angkorteam.cicd.xml.command.RestartCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;
import com.angkorteam.cicd.xml.tomcat.XmlConnector;
import com.angkorteam.cicd.xml.tomcat.XmlHost;
import com.angkorteam.cicd.xml.tomcat.XmlSSLHostConfig;
import com.angkorteam.cicd.xml.tomcat.XmlServer;
import com.angkorteam.framework.Function;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.DeleteQuery;
import com.angkorteam.framework.jdbc.InsertQuery;
import com.angkorteam.framework.jdbc.UpdateQuery;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.spring.JdbcTemplate;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.angkorteam.framework.wicket.markup.html.form.Button;
import com.angkorteam.framework.wicket.markup.html.form.Form;
import com.angkorteam.framework.wicket.markup.html.form.JavascriptTextArea;
import com.angkorteam.framework.wicket.markup.html.form.select2.Option;
import com.angkorteam.framework.wicket.markup.html.form.select2.Select2SingleChoice;
import com.angkorteam.framework.wicket.markup.html.panel.Panel;

public class ConfigurationPanel extends Panel {

	protected String hostId;
	protected String tomcatId;
	protected String instance;
	protected List<Integer> hostPort;

	protected Page itemPage;

	protected Form<Void> form;
	protected Button saveButton;

	protected UIRow row2;

	protected JdkProvider jdkProvider;
	protected UIBlock jdkBlock;
	protected UIContainer jdkIContainer;
	protected Option jdkValue;
	protected Select2SingleChoice<Option> jdkField;

	protected UIBlock row2Block1;

	protected UIRow row1;

	protected UIBlock serverBlock;
	protected UIContainer serverIContainer;
	protected String serverValue;
	protected JavascriptTextArea serverField;

	public ConfigurationPanel(String id, Page itemPage) {
		super(id);
		this.itemPage = itemPage;
	}

	@Override
	protected void initComponent() {
		this.form = new Form<>("form");
		add(this.form);

		this.saveButton = new Button("saveButton");
		this.saveButton.setOnSubmit(this::saveButtonSubmit);
		this.form.add(this.saveButton);

		this.row1 = UIRow.newUIRow("row1", this.form);

		this.serverBlock = this.row1.newUIBlock("serverBlock", Size.Twelve_12);
		this.serverIContainer = this.serverBlock.newUIContainer("serverIContainer");
		this.serverField = new JavascriptTextArea("serverField", new PropertyModel<>(this, "serverValue"));
		this.serverIContainer.add(this.serverField);
		this.serverIContainer.newFeedback("serverFeedback", this.serverField);

		this.row2 = UIRow.newUIRow("row2", this.form);

		this.jdkBlock = this.row2.newUIBlock("jdkBlock", Size.Six_6);
		this.jdkIContainer = this.jdkBlock.newUIContainer("jdkIContainer");
		this.jdkField = new Select2SingleChoice<>("jdkField", new PropertyModel<>(this, "jdkValue"), this.jdkProvider);
		this.jdkIContainer.add(this.jdkField);
		this.jdkIContainer.newFeedback("jdkFeedback", this.jdkField);

		this.row2Block1 = this.row2.newUIBlock("row2Block1", Size.Six_6);
	}

	@Override
	protected void configureMetaData() {
		this.serverField.setRequired(true);
		this.serverField.add(this::serverValidation);
	}

	@Override
	protected void initData() {
		this.tomcatId = new PropertyModel<String>(this.itemPage, "tomcatId").getObject();
		this.hostPort = new ArrayList<>();

		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		Map<String, Object> tomcatObject = jdbcTemplate
				.queryForMap("SELECT * FROM " + Tomcat.NAME + " WHERE " + Tomcat.Field.ID + " = ?", this.tomcatId);
		if (tomcatObject != null) {
			this.hostId = (String) tomcatObject.get(Tomcat.Field.HOST_ID);
			this.instance = (String) tomcatObject.get(Tomcat.Field.INSTANCE);
			this.serverValue = (String) tomcatObject.get(Tomcat.Field.CONFIGURATION);
		}

		Map<String, Object> hostObject = jdbcTemplate
				.queryForMap("SELECT * FROM " + Host.NAME + " WHERE " + Host.Field.ID + " = ?", this.hostId);
		if (hostObject != null && hostObject.get(Host.Field.LOCAL_PORT) != null) {
			this.hostPort.add(((Long) hostObject.get(Host.Field.LOCAL_PORT)).intValue());
		}
		List<Integer> ports = jdbcTemplate
				.queryForList(
						"SELECT " + HostPort.Field.PORT + " FROM " + HostPort.NAME + " WHERE " + HostPort.Field.HOST_ID
								+ " = ? AND " + HostPort.Field.TOMCAT_ID + " != ?",
						Integer.class, this.hostId, this.tomcatId);
		if (ports != null) {
			for (Integer port : ports) {
				this.hostPort.add(port);
			}
		}

		this.jdkProvider = new JdkProvider(this.hostId);
		if (tomcatObject != null) {
			this.jdkValue = jdbcTemplate
					.queryForObject(
							"SELECT " + Jdk.Field.ID + ", " + Jdk.Field.JDK_NAME + " as text FROM " + Jdk.NAME
									+ " WHERE " + Jdk.Field.ID + " = ?",
							Option.MAPPER, tomcatObject.get(Tomcat.Field.JDK_ID));
		}
	}

	protected void serverValidation(IValidatable<String> validatable) {
		String value = validatable.getValue();
		if (value != null && !"".equals(value)) {
			try {
				List<Integer> ports = new ArrayList<>();
				ports.addAll(this.hostPort);
				XmlServer xmlServer = XmlServer.fromText(value);
				if (xmlServer.getPort() != null) {
					if (xmlServer.getPort() > 0 && xmlServer.getPort() < 65535) {
						if (ports.contains(xmlServer.getPort())) {
							validatable.error(new ValidationError("Server::port is invalid"));
						} else {
							ports.add(xmlServer.getPort());
						}
					} else {
						validatable.error(new ValidationError("Server::port is invalid"));
					}
				}
				if (xmlServer.getShutdown() == null || "".equals(xmlServer.getShutdown())) {
					validatable.error(new ValidationError("Server::shutdown is required"));
				}
				if (xmlServer.getService() == null) {
					validatable.error(new ValidationError("Server > Service is required"));
				} else {
					if (xmlServer.getService().getConnectors() == null
							|| xmlServer.getService().getConnectors().isEmpty()) {
						validatable.error(new ValidationError("Server > Service > Connector is required"));
					} else {

						List<String> connectorHosts = validateConnector(validatable, ports, xmlServer);
						List<String> vhosts = new ArrayList<>();

						String defaultHost = null;
						if (xmlServer.getService().getEngine() == null) {
							validatable.error(new ValidationError("Server > Service > Engine is required"));
						} else {
							if (xmlServer.getService().getEngine().getDefaultHost() == null
									|| "".equals(xmlServer.getService().getEngine().getDefaultHost())) {
								validatable.error(
										new ValidationError("Server > Service > Engine::defaultHost is required"));
							} else {
								defaultHost = xmlServer.getService().getEngine().getDefaultHost();
							}
							if (xmlServer.getService().getEngine().getHosts() == null
									|| xmlServer.getService().getEngine().getHosts().isEmpty()) {
								validatable.error(new ValidationError("Server > Service > Engine > Host is required"));
							} else {
								for (XmlHost host : xmlServer.getService().getEngine().getHosts()) {
									if (host.getName() == null || "".equals(host.getName())) {
										validatable.error(new ValidationError(
												"Server > Service > Engine > Host::name is required"));
									} else {
										if (vhosts.contains(host.getName())) {
											validatable.error(new ValidationError(
													"Server > Service > Engine > Host::name is duplicated"));
											break;
										}
										vhosts.add(host.getName());
									}
								}
								if (!vhosts.contains(defaultHost)) {
									validatable.error(
											new ValidationError("Server > Service > Engine::defaultHost is invalid"));
								}
							}
						}
					}
				}
			} catch (RuntimeException e) {
				validatable.error(new ValidationError("bad server configuration " + e.getMessage()));
				e.printStackTrace();
			}
		}
	}

	protected List<String> validateConnector(IValidatable<String> validatable, List<Integer> ports,
			XmlServer xmlServer) {
		List<String> hosts = new ArrayList<>();
		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
		for (XmlConnector connector : xmlServer.getService().getConnectors()) {
			if (connector.getPort() == null) {
				validatable.error(new ValidationError("Server > Service > Connector::port is required"));
			} else {
				if (connector.getPort() > 1 && connector.getPort() < 65535) {
					if (ports.contains(connector.getPort())) {
						validatable.error(new ValidationError("Server > Service > Connector::port is duplicated"));
					} else {
						ports.add(connector.getPort());
					}
				} else {
					validatable.error(new ValidationError("Server > Service > Connector::port is invalid"));
				}
			}

			if (connector.getUpgradeProtocol() != null
					&& !Http2Protocol.class.getName().equals(connector.getUpgradeProtocol().getClassName())) {
				validatable.error(
						new ValidationError("Server > Service > Connector > UpgradeProtocol::className is invalid"));
			}
			if (connector.getProtocol() == null || "".equals(connector.getProtocol())) {
				validatable.error(new ValidationError("Server > Service > Connector::protocol is required"));
			} else {
				if (!"AJP/1.3".equals(connector.getProtocol()) && !"HTTP/1.1".equals(connector.getProtocol())
						&& !Http11AprProtocol.class.getName().equals(connector.getProtocol())
						&& !Http11NioProtocol.class.getName().equals(connector.getProtocol())
						&& !Http11Nio2Protocol.class.getName().equals(connector.getProtocol())
						&& !AjpAprProtocol.class.getName().equals(connector.getProtocol())
						&& !AjpNioProtocol.class.getName().equals(connector.getProtocol())
						&& !AjpNio2Protocol.class.getName().equals(connector.getProtocol())) {
					validatable.error(new ValidationError("Server > Service > Connector::protocol is invalid"));
				}
			}
			if (connector.getSSLEnabled() != null && connector.getSSLEnabled()) {
				Map<String, Object> sslObject = null;
				if (connector.getSslImplementationName() != null && !"".equals(connector.getSslImplementationName())) {
					sslObject = jdbcTemplate
							.queryForMap(
									"SELECT * FROM " + HostSsl.NAME + " WHERE " + HostSsl.Field.HOST_ID + " = ? AND "
											+ HostSsl.Field.NAME + " = ?",
									this.hostId, connector.getSslImplementationName());
					if (sslObject == null) {
						validatable.error(
								new ValidationError("Server > Service > Connector::sslImplementationName is invalid"));
					}
				} else {
					sslObject = jdbcTemplate.queryForMap("SELECT * FROM " + HostSsl.NAME + " WHERE "
							+ HostSsl.Field.HOST_ID + " = ? AND " + HostSsl.Field.SELECTED + " = TRUE", this.hostId);
				}

				if (connector.getSslHostConfigs() == null || connector.getSslHostConfigs().isEmpty()) {
					validatable.error(
							new ValidationError("Server > Service > Connector > SSLHostConfig element is required"));
				} else {
					List<String> hostNames = new ArrayList<>();
					for (XmlSSLHostConfig sslHostConfig : connector.getSslHostConfigs()) {
						String type = (String) sslObject.get(HostSsl.Field.TYPE);
						if ("optional".equals(sslHostConfig.getCertificateVerification())
								|| "required".equals(sslHostConfig.getCertificateVerification())) {
							if (HostSsl.TYPE_JDK.equals(type)) {
								if (sslHostConfig.getTruststoreFile() == null
										|| "".equals(sslHostConfig.getTruststoreFile())) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig::truststoreFile is required"));
								} else if (!sslHostConfig.getTruststoreFile().startsWith("/ssl")) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig::truststoreFile must start with /ssl"));
								} else if (!jdbcTemplate.queryForObject(
										"SELECT COUNT(*) FROM " + TomcatFile.NAME + " WHERE "
												+ TomcatFile.Field.TOMCAT_ID + " = ? AND " + TomcatFile.Field.PATH
												+ " = ?",
										Boolean.class, this.tomcatId, sslHostConfig.getTruststoreFile())) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig::truststoreFile is not uploaded"));
								}
								if (sslHostConfig.getTruststorePassword() == null
										|| "".equals(sslHostConfig.getTruststorePassword())) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig::truststorePassword is required"));
								}
							} else if (HostSsl.TYPE_OpenSSL.equals(type)) {
								if (sslHostConfig.getCaCertificateFile() == null
										|| "".equals(sslHostConfig.getCaCertificateFile())) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig::caCertificateFile is required"));
								} else if (!sslHostConfig.getCaCertificateFile().startsWith("/ssl")) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig::caCertificateFile must start with /ssl"));
								} else if (!jdbcTemplate.queryForObject(
										"SELECT COUNT(*) FROM " + TomcatFile.NAME + " WHERE "
												+ TomcatFile.Field.TOMCAT_ID + " = ? AND " + TomcatFile.Field.PATH
												+ " = ?",
										Boolean.class, this.tomcatId, sslHostConfig.getCaCertificateFile())) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig::caCertificateFile is not uploaded"));
								}
							}
						}

						if (sslHostConfig.getCertificate() == null) {
							validatable.error(new ValidationError(
									"Server > Service > Connector > SSLHostConfig > Certificate element is required"));
						} else {
							if (HostSsl.TYPE_JDK.equals(type)) {
								if (sslHostConfig.getCertificate().getCertificateKeystoreFile() == null
										|| "".equals(sslHostConfig.getCertificate().getCertificateKeystoreFile())) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig > Certificate::certificateKeystoreFile is required"));
								} else if (!sslHostConfig.getCertificate().getCertificateKeystoreFile()
										.startsWith("/ssl")) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig > Certificate::certificateKeystoreFile must start with /ssl"));
								} else if (!jdbcTemplate.queryForObject(
										"SELECT COUNT(*) FROM " + TomcatFile.NAME + " WHERE "
												+ TomcatFile.Field.TOMCAT_ID + " = ? AND " + TomcatFile.Field.PATH
												+ " = ?",
										Boolean.class, this.tomcatId,
										sslHostConfig.getCertificate().getCertificateKeystoreFile())) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig > Certificate::certificateKeystoreFile is not uploaded"));
								}
								if (sslHostConfig.getCertificate().getCertificateKeystorePassword() == null
										|| "".equals(sslHostConfig.getCertificate().getCertificateKeystorePassword())) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig > Certificate::certificateKeystorePassword is required"));
								}
							} else if (HostSsl.TYPE_OpenSSL.equals(type)) {
								if (sslHostConfig.getCertificate().getCertificateFile() == null
										|| "".equals(sslHostConfig.getCertificate().getCertificateFile())) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig > Certificate::certificateFile is required"));
								} else if (!sslHostConfig.getCertificate().getCertificateFile().startsWith("/ssl")) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig > Certificate::certificateFile must start with /ssl"));
								} else if (!jdbcTemplate.queryForObject(
										"SELECT COUNT(*) FROM " + TomcatFile.NAME + " WHERE "
												+ TomcatFile.Field.TOMCAT_ID + " = ? AND " + TomcatFile.Field.PATH
												+ " = ?",
										Boolean.class, this.tomcatId,
										sslHostConfig.getCertificate().getCertificateFile())) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig > Certificate::certificateFile is not uploaded"));
								}
								if (sslHostConfig.getCertificate().getCertificateKeyFile() == null
										|| "".equals(sslHostConfig.getCertificate().getCertificateKeyFile())) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig > Certificate::certificateKeyFile is required"));
								} else if (!sslHostConfig.getCertificate().getCertificateKeyFile().startsWith("/ssl")) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig > Certificate::certificateKeyFile must start with /ssl"));
								} else if (!jdbcTemplate.queryForObject(
										"SELECT COUNT(*) FROM " + TomcatFile.NAME + " WHERE "
												+ TomcatFile.Field.TOMCAT_ID + " = ? AND " + TomcatFile.Field.PATH
												+ " = ?",
										Boolean.class, this.tomcatId,
										sslHostConfig.getCertificate().getCertificateKeyFile())) {
									validatable.error(new ValidationError(
											"Server > Service > Connector > SSLHostConfig > Certificate::certificateKeyFile is not uploaded"));
								}
							}
						}

						if (sslHostConfig.getHostName() != null && !"".equals(sslHostConfig.getHostName())
								&& !"_default_".equals(sslHostConfig.getHostName())) {
							if (!hosts.contains(sslHostConfig.getHostName())) {
								hosts.add(sslHostConfig.getHostName());
							}
						}
						if (hostNames.contains(sslHostConfig.getHostName())) {
							validatable.error(new ValidationError(
									"Server > Service > Connector > SSLHostConfig::hostName is duplicated"));
							break;
						}
						hostNames.add(sslHostConfig.getHostName());
					}
					if (!hostNames.contains("_default_")) {
						validatable.error(new ValidationError(
								"Server > Service > Connector > SSLHostConfig _default_ hostName is required"));
					}
				}
			}
		}
		return hosts;
	}

	public void saveButtonSubmit(Button button) {

		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);
		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		XmlServer xmlServer = XmlServer.fromText(this.serverValue);

		// clear data

		DeleteQuery deleteQuery;

		deleteQuery = new DeleteQuery(TomcatConnector.NAME);
		deleteQuery.addWhere(TomcatConnector.Field.TOMCAT_ID + " = :tomcat_id", this.tomcatId);
		named.update(deleteQuery.toSQL(), deleteQuery.getParam());

		deleteQuery = new DeleteQuery(TomcatVHost.NAME);
		deleteQuery.addWhere(TomcatVHost.Field.TOMCAT_ID + " = :tomcat_id", this.tomcatId);
		named.update(deleteQuery.toSQL(), deleteQuery.getParam());

		deleteQuery = new DeleteQuery(TomcatFile.NAME);
		deleteQuery.addWhere(TomcatFile.Field.TOMCAT_ID + " = :tomcat_id", this.tomcatId);
		deleteQuery.addWhere(TomcatFile.Field.PATH + " = :path", "/conf/server.xml");
		named.update(deleteQuery.toSQL(), deleteQuery.getParam());

		deleteQuery = new DeleteQuery(TomcatVHostCipher.NAME);
		deleteQuery.addWhere(TomcatVHostCipher.Field.TOMCAT_ID + " = :tomcat_id", this.tomcatId);
		named.update(deleteQuery.toSQL(), deleteQuery.getParam());

		deleteQuery = new DeleteQuery(TomcatVHostProtocol.NAME);
		deleteQuery.addWhere(TomcatVHostProtocol.Field.TOMCAT_ID + " = :tomcat_id", this.tomcatId);
		named.update(deleteQuery.toSQL(), deleteQuery.getParam());

		deleteQuery = new DeleteQuery(TomcatVHostPort.NAME);
		deleteQuery.addWhere(TomcatVHostPort.Field.TOMCAT_ID + " = :tomcat_id", this.tomcatId);
		named.update(deleteQuery.toSQL(), deleteQuery.getParam());

		deleteQuery = new DeleteQuery(HostPort.NAME);
		deleteQuery.addWhere(HostPort.Field.TOMCAT_ID + " = :tomcat_id", this.tomcatId);
		named.update(deleteQuery.toSQL(), deleteQuery.getParam());

		// insert data

		UpdateQuery updateQuery = new UpdateQuery(Tomcat.NAME);

		updateQuery.addValue(Tomcat.Field.CONFIGURATION + " = :configuration", this.serverValue);
		if (this.jdkValue != null) {
			updateQuery.addValue(Tomcat.Field.JDK_ID + " = :jdk_id", this.jdkValue.getId());
		} else {
			updateQuery.addValue(Tomcat.Field.JDK_ID + " = NULL");
		}
		updateQuery.addValue(Tomcat.Field.PORT_ERROR + " = false");

		updateQuery.addWhere(Tomcat.Field.ID + " = :id", this.tomcatId);

		named.update(updateQuery.toSQL(), updateQuery.getParam());

		InsertQuery insertQuery = null;

		for (XmlConnector connector : xmlServer.getService().getConnectors()) {
			insertQuery = new InsertQuery(TomcatConnector.NAME);
			insertQuery.addValue(TomcatConnector.Field.ID + " = uuid()");
			insertQuery.addValue(TomcatConnector.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
			insertQuery.addValue(TomcatConnector.Field.MAX_THREAD + " = :max_thread", connector.getMaxThreads());
			insertQuery.addValue(TomcatConnector.Field.PORT + " = :port", connector.getPort());
			insertQuery.addValue(TomcatConnector.Field.PROTOCOL + " = :protocol", connector.getProtocol());
			insertQuery.addValue(TomcatConnector.Field.URI_ENCODING + " = :uri_encoding", connector.getUriEncoding());
			insertQuery.addValue(TomcatConnector.Field.SSL_ENABLED + " = :ssl_enabled", connector.getSSLEnabled());
			insertQuery.addValue(TomcatConnector.Field.SSL_IMPLEMENTATION_NAME + " = :ssl_implementation_name",
					connector.getSslImplementationName());
			named.update(insertQuery.toSQL(), insertQuery.getParam());
		}

		insertQuery = new InsertQuery(HostPort.NAME);
		insertQuery.addValue(HostPort.Field.ID + " = uuid()");
		insertQuery.addValue(HostPort.Field.HOST_ID + " = :host_id", this.hostId);
		insertQuery.addValue(HostPort.Field.USER_ID + " = :user_id", ((WicketSession) getSession()).getUserId());
		insertQuery.addValue(HostPort.Field.TOMCAT_ID + " = :tomcat_id", this.tomcatId);
		insertQuery.addValue(HostPort.Field.PORT + " = :port", xmlServer.getPort());
		named.update(insertQuery.toSQL(), insertQuery.getParam());

		Map<Integer, Map<String, Map<String, String>>> hosts = findConnectorHost(xmlServer);

		for (XmlHost host : xmlServer.getService().getEngine().getHosts()) {
			String vhostId = jdbcTemplate.queryForObject("SELECT uuid() FROM DUAL", String.class);
			insertQuery = new InsertQuery(TomcatVHost.NAME);
			insertQuery.addValue(TomcatVHost.Field.ID + " = :id", vhostId);
			insertQuery.addValue(TomcatVHost.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
			insertQuery.addValue(TomcatVHost.Field.NAME + " = :name", host.getName());
			named.update(insertQuery.toSQL(), insertQuery.getParam());
			for (Entry<Integer, Map<String, Map<String, String>>> port : hosts.entrySet()) {
				Map<String, String> info = null;
				if (port.getValue().containsKey(host.getName())) {
					info = port.getValue().get(host.getName());
				} else {
					info = port.getValue().get("_default_");
				}
				if (info != null) {
					String ciphers = info.get("ciphers");
					if (ciphers != null && !"".equals(ciphers)) {
						for (String cipher : StringUtils.split(ciphers, ",")) {
							insertQuery = new InsertQuery(TomcatVHostCipher.NAME);
							insertQuery.addValue(TomcatVHostCipher.Field.ID + " = uuid()");
							insertQuery.addValue(TomcatVHostCipher.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
							insertQuery.addValue(TomcatVHostCipher.Field.TOMCAT_VHOST_ID + " = :vhostId", vhostId);
							insertQuery.addValue(TomcatVHostCipher.Field.NAME + " = :name", cipher);
							named.update(insertQuery.toSQL(), insertQuery.getParam());
						}
					}
					String protocols = info.get("protocols");
					if (protocols != null & !"".equals(protocols)) {
						for (String protocol : StringUtils.split(protocols, ",")) {
							insertQuery = new InsertQuery(TomcatVHostProtocol.NAME);
							insertQuery.addValue(TomcatVHostProtocol.Field.ID + " = uuid()");
							insertQuery.addValue(TomcatVHostProtocol.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
							insertQuery.addValue(TomcatVHostProtocol.Field.TOMCAT_VHOST_ID + " = :vhostId", vhostId);
							insertQuery.addValue(TomcatVHostProtocol.Field.NAME + " = :name", protocol);
							named.update(insertQuery.toSQL(), insertQuery.getParam());
						}
					}
				}

				insertQuery = new InsertQuery(HostPort.NAME);
				insertQuery.addValue(HostPort.Field.ID + " = uuid()");
				insertQuery.addValue(HostPort.Field.HOST_ID + " = :host_id", this.hostId);
				insertQuery.addValue(HostPort.Field.USER_ID + " = :user_id",
						((WicketSession) getSession()).getUserId());
				insertQuery.addValue(HostPort.Field.TOMCAT_ID + " = :tomcat_id", this.tomcatId);
				insertQuery.addValue(HostPort.Field.PORT + " = :port", port.getKey());
				named.update(insertQuery.toSQL(), insertQuery.getParam());

				String certificateVerification = null;
				if (info != null) {
					certificateVerification = info.get("certificateVerification");
				}
				Integer portNumber = port.getKey();
				String tlsMode = null;
				if ("required".equals(certificateVerification)) {
					tlsMode = "2Way Strict";
				} else if ("optionalNoCA".equals(certificateVerification)) {
					tlsMode = "2Way Welcome";
				} else if ("optional".equals(certificateVerification)) {
					tlsMode = "2Way Verified";
				} else {
					tlsMode = "1Way";
				}
				insertQuery = new InsertQuery(TomcatVHostPort.NAME);
				insertQuery.addValue(TomcatVHostPort.Field.ID + " = uuid()");
				insertQuery.addValue(TomcatVHostPort.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
				insertQuery.addValue(TomcatVHostPort.Field.TOMCAT_VHOST_ID + " = :vhostId", vhostId);
				insertQuery.addValue(TomcatVHostPort.Field.PORT + " = :port", portNumber);
				if (info != null) {
					insertQuery.addValue(TomcatVHostPort.Field.TLS + " = :tls", true);
					insertQuery.addValue(TomcatVHostPort.Field.TLS_MODE + " = :tls_mode", tlsMode);
				} else {
					insertQuery.addValue(TomcatVHostPort.Field.TLS + " = :tls", false);
				}
				named.update(insertQuery.toSQL(), insertQuery.getParam());
			}
		}

		HttpServletRequest request = (HttpServletRequest) getRequest().getContainerRequest();

		{
			String fileId = Repository.saveFile("server.xml", this.serverValue);

			insertQuery = new InsertQuery(TomcatFile.NAME);
			insertQuery.addValue(TomcatFile.Field.ID + " = uuid()");
			insertQuery.addValue(TomcatFile.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
			insertQuery.addValue(TomcatFile.Field.FILE_ID + " = :file_id", fileId);
			insertQuery.addValue(TomcatFile.Field.PATH + " = :path", "/conf/server.xml");
			insertQuery.addValue(TomcatFile.Field.LAST_MODIFIED + " = :last_modified", new Date());
			insertQuery.addValue(TomcatFile.Field.STATUS + " = :status", TomcatFile.STATUS_NA);
			insertQuery.addValue(TomcatFile.Field.READONLY + " = :readonly", false);
			insertQuery.addValue(TomcatFile.Field.TYPE + " = :type", TomcatFile.TYPE_SERVER);
			named.update(insertQuery.toSQL(), insertQuery.getParam());
		}

		JmsSession session = SpringBean.getBean(JmsSession.class);

		String clientId = jdbcTemplate.queryForObject(
				"SELECT " + Host.Field.CLIENT_ID + " FROM " + Host.NAME + " WHERE " + Host.Field.ID + " = ?",
				String.class, this.hostId);

		try (JmsMessageProducer producer = session.createProducer(clientId)) {
			RestartCommand command = new RestartCommand();
			command.setInstance(this.instance);
			command.setServer(
					Function.getHttpAddress(request) + String.format("/api/server/%s/%s", clientId, this.instance));
			producer.send(new XmlMessage(command).toText());
		} catch (IOException e) {
			throw new WicketRuntimeException(e);
		}

		setResponsePage(TomcatBrowsePage.class);
	}

	public Map<Integer, Map<String, Map<String, String>>> findConnectorHost(XmlServer xmlServer) {
		Map<Integer, Map<String, Map<String, String>>> hosts = new HashMap<>();
		for (XmlConnector connector : xmlServer.getService().getConnectors()) {
			if (!hosts.containsKey(connector.getPort())) {
				hosts.put(connector.getPort(), new HashMap<>());
			}
			if (connector.getSslHostConfigs() != null) {
				for (XmlSSLHostConfig sslHostConfig : connector.getSslHostConfigs()) {
					Map<String, String> host = new HashMap<>();
					host.put("ciphers", sslHostConfig.getCiphers());
					host.put("protocols", sslHostConfig.getProtocols());
					host.put("certificateVerification", sslHostConfig.getCertificateVerification());
					hosts.get(connector.getPort()).put(sslHostConfig.getHostName(), host);
				}
			}
		}
		return hosts;
	}

	public String sha512Hex(File file) {
		try (FileInputStream stream = new FileInputStream(file)) {
			return DigestUtils.sha512Hex(stream);
		} catch (IOException e) {
			return null;
		}
	}

}
