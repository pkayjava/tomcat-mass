package com.angkorteam.cicd.dashboard.spring;

import java.util.Arrays;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.angkorteam.cicd.ddl.Token;
import com.angkorteam.cicd.ddl.User;
import com.angkorteam.framework.spring.Delegate;
import com.angkorteam.framework.spring.JdbcTemplate;

public class AuthenticationProvider implements org.springframework.security.authentication.AuthenticationProvider {

	private Delegate delegate;

	public AuthenticationProvider(Delegate delegate) {
		super();
		this.delegate = delegate;
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = (String) authentication.getName();
		String password = (String) authentication.getCredentials();
		if (username == null || "".equals(username) || password == null || "".equals(password)) {
			throw new UsernameNotFoundException("invalid");
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(this.delegate);
		Map<String, Object> tokenObject = jdbcTemplate.queryForMap("SELECT " + Token.Field.ID + ", " + Token.Field.USER_ID + ", " + Token.Field.REVOKED + " FROM " + Token.NAME + " WHERE " + Token.Field.TOKEN_ID + " = ? AND " + Token.Field.TOKEN_SECRET + " = MD5(?)", username, password);

		if (tokenObject == null || (boolean) tokenObject.get(Token.Field.REVOKED)) {
			throw new BadCredentialsException("token is not valid");
		}

		String userId = (String) tokenObject.get(Token.Field.USER_ID);
		Map<String, Object> userObject = null;
		try {
			userObject = jdbcTemplate.queryForMap("SELECT " + User.Field.CONFIRMED + ", " + User.Field.ENABLED + " FROM " + User.NAME + " WHERE " + User.Field.ID + " = ?", userId);
		} catch (DataAccessException e) {
			throw new SecurityException("token is not valid");
		}
		if (userObject == null || !((boolean) userObject.get(User.Field.CONFIRMED)) || !((boolean) userObject.get(User.Field.ENABLED))) {
			throw new SecurityException("token is not valid");
		}
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userId, password, Arrays.asList());
		return token;
	}

	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}

}
