package com.angkorteam.cicd.dashboard.pages;

import java.util.List;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.angkorteam.cicd.dashboard.WicketSession;
import com.angkorteam.cicd.dashboard.pages.host.HostBrowsePage;
import com.angkorteam.cicd.dashboard.pages.jdk.JdkBrowsePage;
import com.angkorteam.cicd.dashboard.pages.token.TokenBrowsePage;
import com.angkorteam.cicd.dashboard.pages.tomcat.TomcatBrowsePage;
import com.angkorteam.framework.Emoji;
import com.angkorteam.framework.models.NavBarMenu;
import com.angkorteam.framework.models.PageFooter;
import com.angkorteam.framework.models.PageHeader;
import com.angkorteam.framework.models.PageLogo;
import com.angkorteam.framework.models.SideMenu;
import com.angkorteam.framework.models.UserInfo;
import com.angkorteam.framework.wicket.DashboardPage;
import com.google.common.collect.Lists;

public abstract class Page extends DashboardPage {

	@Override
	protected IModel<PageLogo> buildPageLogo() {
		PageLogo.Builder builder = new PageLogo.Builder();
		builder.with(Model.of("<b>AG</b>"), Model.of("<b>Dashboard</b>"));
		return Model.of(builder.build());
	}

	@Override
	protected IModel<PageFooter> buildPageFooter() {
		PageFooter.Builder buider = new PageFooter.Builder();
		buider.withCompany(Model.of("POC"));
		return Model.of(buider.build());
	}

	@Override
	protected IModel<PageHeader> buildPageHeader() {
		PageHeader.Builder builder = new PageHeader.Builder();
		builder.withTitle(Model.of(""));
		builder.withDescription(Model.of(""));
		return Model.of(builder.build());
	}

	@Override
	protected IModel<List<SideMenu>> buildSideMenu() {
		// List<SideMenu> menus = Lists.newArrayList();
		// menus.add(new SideMenu.Builder().withMenu(Emoji.fa_500px, Model.of("Home"),
		// HomePage.class).build());
		// return Model.ofList(menus);
		return null;
	}

	@Override
	protected IModel<List<NavBarMenu>> buildNavBarMenu() {
		List<NavBarMenu> menus = Lists.newArrayList();
		menus.add(new NavBarMenu.Builder().withIcon(Emoji.fa_500px, Model.of("Token"), TokenBrowsePage.class).build());
		menus.add(new NavBarMenu.Builder().withIcon(Emoji.fa_500px, Model.of("Host"), HostBrowsePage.class).build());
		menus.add(new NavBarMenu.Builder().withIcon(Emoji.fa_500px, Model.of("Tomcat"), TomcatBrowsePage.class).build());
		menus.add(new NavBarMenu.Builder().withIcon(Emoji.fa_500px, Model.of("JDK"), JdkBrowsePage.class).build());
		if (getSession().isSignedIn()) {
			menus.add(new NavBarMenu.Builder().withIcon(Emoji.fa_500px, Model.of("Logout"), LogoutPage.class).build());
		}
		return Model.ofList(menus);
	}

	@Override
	public WicketSession getSession() {
		return (WicketSession) super.getSession();
	}

	@Override
	protected IModel<UserInfo> buildUserInfo() {
		return null;
	}

	@Override
	protected IModel<Boolean> hasSearchForm() {
		return null;
	}

	@Override
	protected void onSearchClick(String searchValue) {
	}

}
