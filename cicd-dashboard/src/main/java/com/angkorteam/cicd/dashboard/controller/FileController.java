package com.angkorteam.cicd.dashboard.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.angkorteam.cicd.ddl.File;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.SelectQuery;
import com.angkorteam.framework.spring.JdbcNamed;

@Controller
public class FileController {

	@RequestMapping(path = "/file/{fileId}")
	public void file(Principal principal, @PathVariable("fileId") String fileId, HttpServletResponse response) throws IOException {

		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);

		SelectQuery selectQuery = new SelectQuery(File.NAME);
		selectQuery.addField(File.Field.PATH);
		selectQuery.addField(File.Field.USER_ID);
		selectQuery.addWhere(File.Field.ID + " = :id", fileId);

		Map<String, Object> fileObject = named.queryForMap(selectQuery.toSQL(), selectQuery.getParam());
		if (fileObject == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		if (!principal.getName().equals(fileObject.get(File.Field.USER_ID))) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		String path = (String) fileObject.get(File.Field.PATH);
		try (FileInputStream stream = new FileInputStream(path)) {
			IOUtils.copy(stream, response.getOutputStream());
		}
	}

}
