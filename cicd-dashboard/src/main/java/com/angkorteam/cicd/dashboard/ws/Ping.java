package com.angkorteam.cicd.dashboard.ws;

import javax.jws.WebService;

public interface Ping {

	String server();

}