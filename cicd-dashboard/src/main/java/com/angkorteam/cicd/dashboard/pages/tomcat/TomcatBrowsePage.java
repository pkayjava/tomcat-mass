package com.angkorteam.cicd.dashboard.pages.tomcat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.angkorteam.cicd.dashboard.WicketSession;
import com.angkorteam.cicd.dashboard.pages.Page;
import com.angkorteam.cicd.dashboard.pages.host.HostBrowsePage;
import com.angkorteam.cicd.dashboard.provider.JdbcProvider;
import com.angkorteam.cicd.dashboard.spring.JmsMessageProducer;
import com.angkorteam.cicd.dashboard.spring.JmsSession;
import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.cicd.ddl.HostPort;
import com.angkorteam.cicd.ddl.Tomcat;
import com.angkorteam.cicd.xml.command.DebugCommand;
import com.angkorteam.cicd.xml.command.KillCommand;
import com.angkorteam.cicd.xml.command.RestartCommand;
import com.angkorteam.cicd.xml.command.StartCommand;
import com.angkorteam.cicd.xml.command.StopCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;
import com.angkorteam.framework.Function;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.models.PageBreadcrumb;
import com.angkorteam.framework.spring.JdbcTemplate;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.cell.TextCell;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ActionFilterColumn;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ActionItem;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.FilterToolbar;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemClass;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemCss;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemPanel;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.TextFilterColumn;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.google.common.collect.Lists;

@AuthorizeInstantiation(WicketSession.ALL_FUNCTION)
public class TomcatBrowsePage extends Page {

	protected FilterForm<Map<String, String>> form;

	protected UIRow row1;

	protected UIBlock dataBlock;
	protected UIContainer dataIContainer;
	protected DataTable<Map<String, Object>, String> dataTable;
	protected JdbcProvider dataProvider;
	protected List<IColumn<Map<String, Object>, String>> dataColumn;

	protected BookmarkablePageLink<Void> createLink;

	protected BookmarkablePageLink<Void> hostLink;

	@Override
	public IModel<List<PageBreadcrumb>> buildPageBreadcrumb() {
		List<PageBreadcrumb> BREADCRUMB = Lists.newArrayList();
		{
			BREADCRUMB.add(new PageBreadcrumb.Builder().withLabel(Model.of("Tomcat")).build());
		}
		return Model.ofList(BREADCRUMB);
	}

	@Override
	protected void initComponent() {
		this.createLink = new BookmarkablePageLink<>("createLink", TomcatCreatePage.class);
		add(this.createLink);

		this.hostLink = new BookmarkablePageLink<>("hostLink", HostBrowsePage.class);
		add(this.hostLink);

		this.form = new FilterForm<>("form", this.dataProvider);
		add(this.form);

		this.row1 = UIRow.newUIRow("row1", this.form);

		this.dataBlock = this.row1.newUIBlock("dataBlock", Size.Twelve_12);
		this.dataIContainer = this.dataBlock.newUIContainer("dataIContainer");
		this.dataTable = new DefaultDataTable<>("dataTable", this.dataColumn, this.dataProvider, 20);
		this.dataTable.addTopToolbar(new FilterToolbar(this.dataTable, this.form));
		this.dataIContainer.add(this.dataTable);
	}

	@Override
	protected void configureMetaData() {
	}

	@Override
	protected void initData() {
		this.dataProvider = new JdbcProvider(Tomcat.NAME);
		this.dataProvider.applyJoin("host", "INNER JOIN " + Host.NAME + " ON " + Host.NAME + "." + Host.Field.ID + " = " + Tomcat.NAME + "." + Tomcat.Field.HOST_ID);

		this.dataProvider.boardField(Tomcat.NAME + "." + Tomcat.Field.ID, "id", String.class);
		this.dataProvider.boardField(Host.NAME + "." + Host.Field.ID, "host_id", String.class);
		this.dataProvider.boardField(Host.NAME + "." + Host.Field.CLIENT_ID, "host", String.class);
		this.dataProvider.boardField(Tomcat.NAME + "." + Tomcat.Field.INSTANCE, "tomcat", String.class);
		this.dataProvider.boardField(Tomcat.NAME + "." + Tomcat.Field.TOMCAT_VERSION, "tomcat_version", String.class);
		this.dataProvider.boardField(Tomcat.NAME + "." + Tomcat.Field.JVM_VENDOR, "jvm_vendor", String.class);
		this.dataProvider.boardField(Tomcat.NAME + "." + Tomcat.Field.JVM_VERSION, "jvm_version", String.class);
		this.dataProvider.boardField(Host.NAME + "." + Host.Field.OS_NAME, "os_name", String.class);
		this.dataProvider.boardField(Host.NAME + "." + Host.Field.OS_VERSION, "os_version", String.class);
		this.dataProvider.boardField(Host.NAME + "." + Host.Field.OS_ARCH, "os_arch", String.class);
		this.dataProvider.boardField(Tomcat.NAME + "." + Tomcat.Field.PORT_ERROR, "port_error", Boolean.class);
		this.dataProvider.boardField(Tomcat.NAME + "." + Tomcat.Field.STATUS, "status", String.class);

		this.dataProvider.selectField("id", String.class);
		this.dataProvider.selectField("host_id", String.class);

		this.dataProvider.applyWhere("user", Tomcat.NAME + "." + Tomcat.Field.USER_ID + " = '" + getSession().getUserId() + "'");

		this.dataColumn = new ArrayList<>();
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Host"), "host", "host", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("OS"), "os_name", "os_name", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Version"), "os_version", "os_version", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Arch"), "os_arch", "os_arch", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Tomcat"), "tomcat", "tomcat", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Version"), "tomcat_version", "tomcat_version", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("JVM"), "jvm_version", "jvm_version", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Vendor"), "jvm_vendor", "jvm_vendor", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Status"), "status", "status", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.Boolean, Model.of("Port Error"), "port_error", "port_error", this::dataColumn));
		this.dataColumn.add(new ActionFilterColumn<>(Model.of("Action"), this::dataAction, this::dataClick));
	}

	protected void dataClick(String column, Map<String, Object> model, AjaxRequestTarget target) {

		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
		String tomcatId = (String) model.get("id");
		String instance = (String) model.get("tomcat");
		String hostId = (String) model.get("host_id");
		String clientId = jdbcTemplate.queryForObject("SELECT " + Host.Field.CLIENT_ID + " FROM " + Host.NAME + " WHERE " + Host.Field.ID + " = ?", String.class, hostId);
		HttpServletRequest request = (HttpServletRequest) getRequest().getContainerRequest();
		JmsSession session = SpringBean.getBean(JmsSession.class);
		if ("start".equals(column)) {
			try (JmsMessageProducer producer = session.createProducer(clientId)) {
				StartCommand command = new StartCommand();
				command.setInstance(instance);
				command.setServer(Function.getHttpAddress(request) + String.format("/api/server/%s/%s", clientId, instance));
				XmlMessage message = new XmlMessage(command);
				producer.send(message.toText());
			} catch (IOException e) {
			}
		} else if ("stop".equals(column)) {
			try (JmsMessageProducer producer = session.createProducer(clientId)) {
				StopCommand command = new StopCommand();
				command.setInstance(instance);
				producer.send(new XmlMessage(command).toText());
			} catch (IOException e) {
			}
		} else if ("restart".equals(column)) {
			try (JmsMessageProducer producer = session.createProducer(clientId)) {
				RestartCommand command = new RestartCommand();
				command.setInstance(instance);
				command.setServer(Function.getHttpAddress(request) + String.format("/api/server/%s/%s", clientId, instance));
				producer.send(new XmlMessage(command).toText());
			} catch (IOException e) {
			}
		} else if ("kill".equals(column)) {
			try (JmsMessageProducer producer = session.createProducer(clientId)) {
				KillCommand command = new KillCommand();
				command.setInstance(instance);
				producer.send(new XmlMessage(command).toText());
			} catch (IOException e) {
			}
		} else if ("info".equals(column)) {
			PageParameters parameters = new PageParameters();
			parameters.add("tomcatId", tomcatId);
			setResponsePage(TomcatPreviewPage.class, parameters);
			return;
		} else if ("debug".equals(column)) {
			try (JmsMessageProducer producer = session.createProducer(clientId)) {
				DebugCommand command = new DebugCommand();
				command.setInstance(instance);
				command.setServer(Function.getHttpAddress(request) + String.format("/api/server/%s/%s", clientId, instance));
				XmlMessage message = new XmlMessage(command);
				producer.send(message.toText());
			} catch (IOException e) {
			}
		} else if ("delete".equals(column)) {
			jdbcTemplate.update("DELETE FROM " + Tomcat.NAME + " WHERE " + Tomcat.Field.ID + " = ?", tomcatId);
			jdbcTemplate.update("DELETE FROM " + HostPort.NAME + " WHERE " + HostPort.Field.TOMCAT_ID + " = ?", tomcatId);
		}
		if (target != null) {
			target.add(this.dataTable);
		}
	}

	protected List<ActionItem> dataAction(String column, Map<String, Object> model) {
		List<ActionItem> actions = Lists.newArrayList();
		actions.add(new ActionItem("info", Model.of("Info"), ItemCss.PRIMARY));
		Boolean port_error = (Boolean) model.get("port_error");
		if (port_error != null && port_error) {
		} else {
			String status = (String) model.get("status");
			if ("INITIALIZING".equals(status)) {
				actions.add(new ActionItem("kill", Model.of("Kill"), ItemCss.PRIMARY));
			} else if ("INITIALIZED".equals(status)) {
				actions.add(new ActionItem("kill", Model.of("Kill"), ItemCss.PRIMARY));
			} else if ("STARTING_PREP".equals(status)) {
				actions.add(new ActionItem("kill", Model.of("Kill"), ItemCss.PRIMARY));
			} else if ("STARTING".equals(status)) {
				actions.add(new ActionItem("kill", Model.of("Kill"), ItemCss.PRIMARY));
			} else if ("STARTED".equals(status)) {
				actions.add(new ActionItem("stop", Model.of("Stop"), ItemCss.PRIMARY));
				actions.add(new ActionItem("restart", Model.of("Restart"), ItemCss.PRIMARY));
				actions.add(new ActionItem("kill", Model.of("Kill"), ItemCss.PRIMARY));
			} else if ("STOPPING_PREP".equals(status)) {
				actions.add(new ActionItem("kill", Model.of("Kill"), ItemCss.PRIMARY));
			} else if ("STOPPING".equals(status)) {
				actions.add(new ActionItem("kill", Model.of("Kill"), ItemCss.PRIMARY));
			} else if ("STOPPED".equals(status)) {
				actions.add(new ActionItem("kill", Model.of("Kill"), ItemCss.PRIMARY));
			} else if ("DESTROYING".equals(status)) {
				actions.add(new ActionItem("start", Model.of("Start"), ItemCss.PRIMARY));
			} else if ("DESTROYED".equals(status)) {
				actions.add(new ActionItem("start", Model.of("Start"), ItemCss.PRIMARY));
				actions.add(new ActionItem("debug", Model.of("Debug"), ItemCss.PRIMARY));
				actions.add(new ActionItem("delete", Model.of("Delete"), ItemCss.PRIMARY));
			}
		}
		return actions;
	}

	protected ItemPanel dataColumn(String column, IModel<String> display, Map<String, Object> model) {
		if ("os_arch".equals(column) || "os_name".equals(column) || "os_version".equals(column) || "host".equals(column) || "tomcat".equals(column) || "tomcat_version".equals(column) || "jvm_version".equals(column) || "jvm_vendor".equals(column) || "status".equals(column)) {
			String value = (String) model.get(column);
			return new TextCell(value);
		} else if ("port_error".equals(column)) {
			Boolean value = (Boolean) model.get(column);
			return new TextCell(value);
		}
		throw new WicketRuntimeException("Unknown " + column);
	}

}
