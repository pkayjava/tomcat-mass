package com.angkorteam.cicd.dashboard.provider;

import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.spring.JdbcTemplate;

public class JdbcProvider extends com.angkorteam.framework.share.provider.JdbcProvider {

	public JdbcProvider() {
	}

	public JdbcProvider(String from) {
		super(from);
	}

	@Override
	protected JdbcTemplate getJdbcTemplate() {
		return SpringBean.getBean(JdbcTemplate.class);
	}

}
