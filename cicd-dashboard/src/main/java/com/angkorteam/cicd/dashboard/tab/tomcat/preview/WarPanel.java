package com.angkorteam.cicd.dashboard.tab.tomcat.preview;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.DomainValidator;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.apache.wicket.Page;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.SortOrder;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.ValidationError;

import com.angkorteam.cicd.dashboard.Repository;
import com.angkorteam.cicd.dashboard.pages.tomcat.TomcatPreviewPage;
import com.angkorteam.cicd.dashboard.provider.JdbcProvider;
import com.angkorteam.cicd.dashboard.provider.VHostProvider;
import com.angkorteam.cicd.dashboard.spring.JmsMessageProducer;
import com.angkorteam.cicd.dashboard.spring.JmsSession;
import com.angkorteam.cicd.ddl.File;
import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.cicd.ddl.Tomcat;
import com.angkorteam.cicd.ddl.TomcatFile;
import com.angkorteam.cicd.xml.command.RestartCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;
import com.angkorteam.framework.Function;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.InsertQuery;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.spring.JdbcTemplate;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.cell.TextCell;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ActionFilterColumn;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ActionItem;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.Calendar;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.FilterToolbar;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemClass;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemCss;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemPanel;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.TextFilterColumn;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.angkorteam.framework.wicket.markup.html.form.Button;
import com.angkorteam.framework.wicket.markup.html.form.Form;
import com.angkorteam.framework.wicket.markup.html.form.SmartTextField;
import com.angkorteam.framework.wicket.markup.html.panel.Panel;
import com.google.common.collect.Lists;

public class WarPanel extends Panel {

	protected String hostId;
	protected String tomcatId;
	protected String instance;

	protected Page itemPage;

	protected Form<Void> form;
	protected Button uploadButton;

	protected UIRow row1;

	protected VHostProvider vhostProvider;
	protected UIBlock vhostBlock;
	protected UIContainer vhostIContainer;
	protected String vhostValue;
	protected SmartTextField vhostField;

	protected UIBlock warBlock;
	protected UIContainer warIContainer;
	protected List<FileUpload> warValue;
	protected FileUploadField warField;

	protected FilterForm<Map<String, String>> form1;

	protected UIRow row2;

	protected UIBlock dataBlock;
	protected UIContainer dataIContainer;
	protected DataTable<Map<String, Object>, String> dataTable;
	protected JdbcProvider dataProvider;
	protected List<IColumn<Map<String, Object>, String>> dataColumn;

	protected BookmarkablePageLink<Void> deployLink;

	public WarPanel(String id, Page itemPage) {
		super(id);
		this.itemPage = itemPage;
	}

	@Override
	protected void initComponent() {
		this.form = new Form<>("form");
		this.form.setMultiPart(true);
		add(this.form);

		this.uploadButton = new Button("uploadButton");
		this.uploadButton.setOnSubmit(this::uploadButtonSubmit);
		this.form.add(this.uploadButton);

		this.row1 = UIRow.newUIRow("row1", this.form);

		this.vhostBlock = this.row1.newUIBlock("vhostBlock", Size.Six_6);
		this.vhostIContainer = this.vhostBlock.newUIContainer("vhostIContainer");
		this.vhostField = new SmartTextField("vhostField", new PropertyModel<>(this, "vhostValue"), this.vhostProvider);
		this.vhostIContainer.add(this.vhostField);
		this.vhostIContainer.newFeedback("vhostFeedback", this.vhostField);

		this.warBlock = this.row1.newUIBlock("warBlock", Size.Six_6);
		this.warIContainer = this.warBlock.newUIContainer("warIContainer");
		this.warField = new FileUploadField("warField", new PropertyModel<>(this, "warValue"));
		this.warIContainer.add(this.warField);
		this.warIContainer.newFeedback("warFeedback", this.warField);

		this.form1 = new FilterForm<>("form1", this.dataProvider);
		add(this.form1);

		this.row2 = UIRow.newUIRow("row2", this.form1);

		this.dataBlock = this.row2.newUIBlock("dataBlock", Size.Twelve_12);
		this.dataIContainer = this.dataBlock.newUIContainer("dataIContainer");
		this.dataTable = new DefaultDataTable<>("dataTable", this.dataColumn, this.dataProvider, 20);
		this.dataTable.addTopToolbar(new FilterToolbar(this.dataTable, this.form1));
		this.dataIContainer.add(this.dataTable);
	}

	@Override
	protected void configureMetaData() {
		this.warField.setRequired(true);
		this.warField.add(this::warValidation);

		this.vhostField.setRequired(true);
		this.vhostField.add(this::vhostValidation);
	}

	protected void vhostValidation(IValidatable<String> validatable) {
		String value = validatable.getValue();
		if (value != null && !"".equals(value)) {
			if (!InetAddressValidator.getInstance().isValid(value) && !DomainValidator.getInstance(true).isValid(value)) {
				validatable.error(new ValidationError("invalid name"));
			}
		}
	}

	protected void warValidation(IValidatable<List<FileUpload>> validatable) {
		List<FileUpload> values = validatable.getValue();
		if (values != null && !values.isEmpty()) {
			FileUpload value = values.get(0);
			String extension = StringUtils.lowerCase(FilenameUtils.getExtension(value.getClientFileName()));
			String basename = StringUtils.lowerCase(FilenameUtils.getBaseName(value.getClientFileName()));
			if (!"war".equals(extension)) {
				validatable.error(new ValidationError("accept only war file"));
			} else {
				if (!basename.matches("[-_.a-z0-9A-Z]+")) {
					validatable.error(new ValidationError("invalid name, only a to z, underscore(_), dot(.) and dash(-) are allow"));
				}
			}
		}
	}

	@Override
	protected void initData() {
		this.tomcatId = new PropertyModel<String>(this.itemPage, "tomcatId").getObject();

		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		Map<String, Object> tomcatObject = jdbcTemplate.queryForMap("SELECT * FROM " + Tomcat.NAME + " WHERE " + Tomcat.Field.ID + " = ?", this.tomcatId);
		this.hostId = (String) tomcatObject.get(Tomcat.Field.HOST_ID);
		this.instance = (String) tomcatObject.get(Tomcat.Field.INSTANCE);

		this.vhostProvider = new VHostProvider(this.tomcatId);

		this.dataProvider = new JdbcProvider(TomcatFile.NAME);
		this.dataProvider.applyJoin("file", "INNER JOIN " + File.NAME + " ON " + File.NAME + "." + File.Field.ID + " = " + TomcatFile.NAME + "." + TomcatFile.Field.FILE_ID);
		this.dataProvider.boardField(TomcatFile.NAME + "." + TomcatFile.Field.ID, "id", String.class);
		this.dataProvider.boardField(TomcatFile.NAME + "." + TomcatFile.Field.VHOST, "vhost", String.class);
		this.dataProvider.boardField(TomcatFile.NAME + "." + TomcatFile.Field.PATH, "path", String.class);
		this.dataProvider.boardField(TomcatFile.NAME + "." + TomcatFile.Field.READONLY, "readonly", Boolean.class);
		this.dataProvider.boardField(TomcatFile.NAME + "." + TomcatFile.Field.LAST_MODIFIED, "last_modified", Calendar.DateTime);
		this.dataProvider.boardField(TomcatFile.NAME + "." + TomcatFile.Field.STATUS, "status", String.class);
		this.dataProvider.boardField(File.NAME + "." + File.Field.FILE_SIZE, "file_size", String.class);

		this.dataProvider.selectField("id", String.class);
		this.dataProvider.setSort("last_modified", SortOrder.DESCENDING);

		this.dataProvider.applyWhere("type", TomcatFile.NAME + "." + TomcatFile.Field.TYPE + " = '" + TomcatFile.TYPE_WAR + "'");
		this.dataProvider.applyWhere("tomcat", TomcatFile.NAME + "." + TomcatFile.Field.TOMCAT_ID + " = '" + this.tomcatId + "'");

		this.dataColumn = new ArrayList<>();
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("VHost"), "vhost", "vhost", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Path"), "path", "path", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Size"), "file_size", "file_size", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.Boolean, Model.of("Read Only"), "readonly", "readonly", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Status"), "status", "status", this::dataColumn));
		this.dataColumn.add(new ActionFilterColumn<>(Model.of("Action"), this::dataAction, this::dataClick));
	}

	protected void dataClick(String column, Map<String, Object> model, AjaxRequestTarget target) {

		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
		String clientId = jdbcTemplate.queryForObject("SELECT " + Host.Field.CLIENT_ID + " FROM " + Host.NAME + " WHERE " + Host.Field.ID + " = ?", String.class, this.hostId);
		HttpServletRequest request = (HttpServletRequest) getRequest().getContainerRequest();
		JmsSession session = SpringBean.getBean(JmsSession.class);
		if ("delete".equals(column)) {
			String tomcatFileId = (String) model.get("id");
			jdbcTemplate.update("DELETE FROM " + TomcatFile.NAME + " WHERE " + TomcatFile.Field.ID + " = ?", tomcatFileId);
			try (JmsMessageProducer producer = session.createProducer(clientId)) {
				RestartCommand command = new RestartCommand();
				command.setInstance(this.instance);
				command.setServer(Function.getHttpAddress(request) + String.format("/api/server/%s/%s", clientId, this.instance));
				producer.send(new XmlMessage(command).toText());
			} catch (IOException e) {
			}
		}

		if (target != null) {
			target.add(this.dataTable);
		}
	}

	protected List<ActionItem> dataAction(String column, Map<String, Object> model) {
		List<ActionItem> actions = Lists.newArrayList();
		Boolean readonly = (Boolean) model.get("readonly");

		if (readonly == null || !readonly) {
			actions.add(new ActionItem("delete", Model.of("Delete"), ItemCss.PRIMARY));
		}

		return actions;
	}

	protected ItemPanel dataColumn(String column, IModel<String> display, Map<String, Object> model) {
		if ("vhost".equals(column) || "file_size".equals(column) || "path".equals(column) || "status".equals(column)) {
			String value = (String) model.get(column);
			return new TextCell(value);
		} else if ("readonly".equals(column)) {
			Boolean value = (Boolean) model.get(column);
			return new TextCell(value);
		}
		throw new WicketRuntimeException("Unknown " + column);
	}

	protected void uploadButtonSubmit(Button button) {
		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		String clientFileName = this.warField.getFileUpload().getClientFileName();

		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);

		jdbcTemplate.update("DELETE FROM " + TomcatFile.NAME + " WHERE " + TomcatFile.Field.TOMCAT_ID + " = ? AND " + TomcatFile.Field.PATH + " = ?", this.tomcatId, "/webapps/" + this.vhostValue + "/" + clientFileName);

		InsertQuery insertQuery = null;

		String fileId = Repository.saveFile(this.warField.getFileUpload());

		insertQuery = new InsertQuery(TomcatFile.NAME);
		insertQuery.addValue(TomcatFile.Field.ID + " = uuid()");
		insertQuery.addValue(TomcatFile.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
		insertQuery.addValue(TomcatFile.Field.FILE_ID + " = :file_id", fileId);
		insertQuery.addValue(TomcatFile.Field.PATH + " = :path", "/webapps/" + this.vhostValue + "/" + clientFileName);
		insertQuery.addValue(TomcatFile.Field.LAST_MODIFIED + " = :last_modified", new Date());
		insertQuery.addValue(TomcatFile.Field.VHOST + " = :vhost", this.vhostValue);
		insertQuery.addValue(TomcatFile.Field.TYPE + " = :type", TomcatFile.TYPE_WAR);
		insertQuery.addValue(TomcatFile.Field.READONLY + " = :readonly", false);
		insertQuery.addValue(TomcatFile.Field.STATUS + " = :status", TomcatFile.STATUS_STOPPED);
		named.update(insertQuery.toSQL(), insertQuery.getParam());

		String clientId = jdbcTemplate.queryForObject("SELECT " + Host.Field.CLIENT_ID + " FROM " + Host.NAME + " WHERE " + Host.Field.ID + " = ?", String.class, this.hostId);

		JmsSession session = SpringBean.getBean(JmsSession.class);

		HttpServletRequest request = (HttpServletRequest) getRequest().getContainerRequest();

		try (JmsMessageProducer producer = session.createProducer(clientId)) {
			RestartCommand command = new RestartCommand();
			command.setInstance(this.instance);
			command.setServer(Function.getHttpAddress(request) + String.format("/api/server/%s/%s", clientId, this.instance));
			producer.send(new XmlMessage(command).toText());
		} catch (IOException e) {
			throw new WicketRuntimeException(e);
		}

		PageParameters parameters = new PageParameters();
		parameters.add("tomcatId", this.tomcatId);
		parameters.add("tab", TomcatPreviewPage.TAB_WAR);
		setResponsePage(TomcatPreviewPage.class, parameters);
	}

}
