package com.angkorteam.cicd.dashboard.tab.tomcat.preview;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.Page;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.SortOrder;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.ValidationError;

import com.angkorteam.cicd.dashboard.Repository;
import com.angkorteam.cicd.dashboard.pages.tomcat.TomcatPreviewPage;
import com.angkorteam.cicd.dashboard.provider.JdbcProvider;
import com.angkorteam.cicd.ddl.File;
import com.angkorteam.cicd.ddl.Tomcat;
import com.angkorteam.cicd.ddl.TomcatFile;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.InsertQuery;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.spring.JdbcTemplate;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.cell.TextCell;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ActionFilterColumn;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ActionItem;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.Calendar;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.FilterToolbar;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemClass;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemCss;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemPanel;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.TextFilterColumn;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.angkorteam.framework.wicket.markup.html.form.Button;
import com.angkorteam.framework.wicket.markup.html.form.Form;
import com.angkorteam.framework.wicket.markup.html.panel.Panel;
import com.google.common.collect.Lists;

public class CertificatePanel extends Panel {

	protected String hostId;
	protected String tomcatId;
	protected String instance;

	protected Page itemPage;

	protected Form<Void> form;
	protected Button uploadButton;

	protected UIRow row1;

	protected UIBlock certificateBlock;
	protected UIContainer certificateIContainer;
	protected List<FileUpload> certificateValue;
	protected FileUploadField certificateField;

	protected FilterForm<Map<String, String>> form1;

	protected UIRow row2;

	protected UIBlock dataBlock;
	protected UIContainer dataIContainer;
	protected DataTable<Map<String, Object>, String> dataTable;
	protected JdbcProvider dataProvider;
	protected List<IColumn<Map<String, Object>, String>> dataColumn;

	public CertificatePanel(String id, Page itemPage) {
		super(id);
		this.itemPage = itemPage;
	}

	@Override
	protected void initComponent() {
		this.form = new Form<>("form");
		this.form.setMultiPart(true);
		add(this.form);

		this.uploadButton = new Button("uploadButton");
		this.uploadButton.setOnSubmit(this::uploadButtonSubmit);
		this.form.add(this.uploadButton);

		this.row1 = UIRow.newUIRow("row1", this.form);

		this.certificateBlock = this.row1.newUIBlock("certificateBlock", Size.Twelve_12);
		this.certificateIContainer = this.certificateBlock.newUIContainer("certificateIContainer");
		this.certificateField = new FileUploadField("certificateField", new PropertyModel<>(this, "certificateValue"));
		this.certificateIContainer.add(this.certificateField);
		this.certificateIContainer.newFeedback("certificateFeedback", this.certificateField);

		this.form1 = new FilterForm<>("form1", this.dataProvider);
		add(this.form1);

		this.row2 = UIRow.newUIRow("row2", this.form1);

		this.dataBlock = this.row2.newUIBlock("dataBlock", Size.Twelve_12);
		this.dataIContainer = this.dataBlock.newUIContainer("dataIContainer");
		this.dataTable = new DefaultDataTable<>("dataTable", this.dataColumn, this.dataProvider, 20);
		this.dataTable.addTopToolbar(new FilterToolbar(this.dataTable, this.form1));
		this.dataIContainer.add(this.dataTable);
	}

	@Override
	protected void configureMetaData() {
		this.certificateField.setRequired(true);
		this.certificateField.add(this::certificateValidation);
	}

	protected void certificateValidation(IValidatable<List<FileUpload>> validatable) {
		List<FileUpload> values = validatable.getValue();
		if (values != null && !values.isEmpty()) {
			FileUpload value = values.get(0);
			String extension = StringUtils.lowerCase(FilenameUtils.getExtension(value.getClientFileName()));
			String basename = StringUtils.lowerCase(FilenameUtils.getBaseName(value.getClientFileName()));
			if (!"pem".equals(extension) && !"key".equals(extension) && !"crt".equals(extension) && !"p12".equals(extension) && !"jks".equals(extension)) {
				validatable.error(new ValidationError("accept only pem, key, crt, p12 and, jks files"));
			} else {
				if (!basename.matches("[-_.a-z0-9A-Z]+")) {
					validatable.error(new ValidationError("invalid name, only a to z, underscore(_), dot(.) and dash(-) are allow"));
				}
			}
		}
	}

	@Override
	protected void initData() {
		this.tomcatId = new PropertyModel<String>(this.itemPage, "tomcatId").getObject();

		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		Map<String, Object> tomcatObject = jdbcTemplate.queryForMap("SELECT * FROM " + Tomcat.NAME + " WHERE " + Tomcat.Field.ID + " = ?", this.tomcatId);
		this.hostId = (String) tomcatObject.get(Tomcat.Field.HOST_ID);
		this.instance = (String) tomcatObject.get(Tomcat.Field.INSTANCE);

		this.dataProvider = new JdbcProvider(TomcatFile.NAME);
		this.dataProvider.applyJoin("file", "INNER JOIN " + File.NAME + " ON " + File.NAME + "." + File.Field.ID + " = " + TomcatFile.NAME + "." + TomcatFile.Field.FILE_ID);
		this.dataProvider.boardField(TomcatFile.NAME + "." + TomcatFile.Field.ID, "id", String.class);
		this.dataProvider.boardField(TomcatFile.NAME + "." + TomcatFile.Field.PATH, "path", String.class);
		this.dataProvider.boardField(TomcatFile.NAME + "." + TomcatFile.Field.LAST_MODIFIED, "last_modified", Calendar.DateTime);
		this.dataProvider.boardField(TomcatFile.NAME + "." + TomcatFile.Field.READONLY, "readonly", Boolean.class);
		this.dataProvider.boardField(File.NAME + "." + File.Field.FILE_SIZE, "file_size", String.class);

		this.dataProvider.selectField("id", String.class);

		this.dataProvider.setSort("last_modified", SortOrder.DESCENDING);

		this.dataProvider.applyWhere("type", TomcatFile.NAME + "." + TomcatFile.Field.TYPE + " = '" + TomcatFile.TYPE_SSL + "'");
		this.dataProvider.applyWhere("tomcat", TomcatFile.NAME + "." + TomcatFile.Field.TOMCAT_ID + " = '" + this.tomcatId + "'");

		this.dataColumn = new ArrayList<>();
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Path"), "path", "path", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Size"), "file_size", "file_size", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.Boolean, Model.of("Read Only"), "readonly", "readonly", this::dataColumn));
		this.dataColumn.add(new ActionFilterColumn<>(Model.of("Action"), this::dataAction, this::dataClick));
	}

	protected void dataClick(String column, Map<String, Object> model, AjaxRequestTarget target) {
		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
		if ("delete".equals(column)) {
			String tomcatFileId = (String) model.get("id");
			jdbcTemplate.update("DELETE FROM " + TomcatFile.NAME + " WHERE " + TomcatFile.Field.ID + " = ?", tomcatFileId);
		}

		if (target != null) {
			target.add(this.dataTable);
		}
	}

	protected List<ActionItem> dataAction(String column, Map<String, Object> model) {
		List<ActionItem> actions = Lists.newArrayList();
		Boolean readonly = (Boolean) model.get("readonly");

		if (readonly == null || !readonly) {
			actions.add(new ActionItem("delete", Model.of("Delete"), ItemCss.PRIMARY));
		}

		return actions;
	}

	protected ItemPanel dataColumn(String column, IModel<String> display, Map<String, Object> model) {
		if ("file_size".equals(column) || "path".equals(column) || "status".equals(column)) {
			String value = (String) model.get(column);
			return new TextCell(value);
		} else if ("readonly".equals(column)) {
			Boolean value = (Boolean) model.get(column);
			return new TextCell(value);
		}
		throw new WicketRuntimeException("Unknown " + column);
	}

	protected void uploadButtonSubmit(Button button) {

		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);
		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
		String clientFileName = this.certificateField.getFileUpload().getClientFileName();

		String fileId = Repository.saveFile(this.certificateField.getFileUpload());

		jdbcTemplate.update("DELETE FROM " + TomcatFile.NAME + " WHERE " + TomcatFile.Field.TOMCAT_ID + " = ? AND " + TomcatFile.Field.PATH + " = ?", this.tomcatId, "/lib/" + this.certificateField.getFileUpload().getClientFileName());

		InsertQuery insertQuery = null;

		insertQuery = new InsertQuery(TomcatFile.NAME);
		insertQuery.addValue(TomcatFile.Field.ID + " = uuid()");
		insertQuery.addValue(TomcatFile.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
		insertQuery.addValue(TomcatFile.Field.FILE_ID + " = :file_id", fileId);
		insertQuery.addValue(TomcatFile.Field.PATH + " = :path", "/ssl/" + clientFileName);
		insertQuery.addValue(TomcatFile.Field.LAST_MODIFIED + " = :last_modified", new Date());
		insertQuery.addValue(TomcatFile.Field.TYPE + " = :type", TomcatFile.TYPE_SSL);
		insertQuery.addValue(TomcatFile.Field.READONLY + " = :readonly", false);
		insertQuery.addValue(TomcatFile.Field.STATUS + " = :status", TomcatFile.STATUS_NA);
		named.update(insertQuery.toSQL(), insertQuery.getParam());

		PageParameters parameters = new PageParameters();
		parameters.add("tomcatId", this.tomcatId);
		parameters.add("tab", TomcatPreviewPage.TAB_CERTIFICATE);
		setResponsePage(TomcatPreviewPage.class, parameters);
	}

}
