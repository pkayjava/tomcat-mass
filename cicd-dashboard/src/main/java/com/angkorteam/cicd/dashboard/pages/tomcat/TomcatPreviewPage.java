package com.angkorteam.cicd.dashboard.pages.tomcat;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.angkorteam.cicd.dashboard.pages.Page;
import com.angkorteam.cicd.dashboard.tab.tomcat.preview.CertificateTab;
import com.angkorteam.cicd.dashboard.tab.tomcat.preview.CipherTab;
import com.angkorteam.cicd.dashboard.tab.tomcat.preview.ConfigurationTab;
import com.angkorteam.cicd.dashboard.tab.tomcat.preview.ConnectorTab;
import com.angkorteam.cicd.dashboard.tab.tomcat.preview.InfoTab;
import com.angkorteam.cicd.dashboard.tab.tomcat.preview.LibraryTab;
import com.angkorteam.cicd.dashboard.tab.tomcat.preview.ProtocolTab;
import com.angkorteam.cicd.dashboard.tab.tomcat.preview.WarTab;
import com.angkorteam.cicd.ddl.Tomcat;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.models.PageBreadcrumb;
import com.angkorteam.framework.spring.JdbcTemplate;
import com.angkorteam.framework.wicket.extensions.markup.html.tabs.AjaxTabbedPanel;
import com.angkorteam.framework.wicket.extensions.markup.html.tabs.ITab;
import com.angkorteam.framework.wicket.markup.html.form.Button;
import com.google.common.collect.Lists;

public class TomcatPreviewPage extends Page {

	public static int TAB_INFO = 0;
	public static int TAB_CONFIGURATION = 1;
	public static int TAB_WAR = 2;
	public static int TAB_LIBRARY = 3;
	public static int TAB_CERTIFICATE = 4;
	public static int TAB_CONNECTOR = 5;
	public static int TAB_PROTOCOL = 6;
	public static int TAB_CIPHER = 7;

	protected String hostId;
	protected String tomcatId;

	protected AjaxTabbedPanel<ITab> tab;

	@Override
	public IModel<List<PageBreadcrumb>> buildPageBreadcrumb() {
		List<PageBreadcrumb> BREADCRUMB = Lists.newArrayList();
		{
			BREADCRUMB.add(new PageBreadcrumb.Builder().withLabel(Model.of("Tomcat")).build());
		}
		return Model.ofList(BREADCRUMB);
	}

	@Override
	protected void initData() {
		PageParameters parameters = getPageParameters();
		this.tomcatId = parameters.get("tomcatId").toString();

		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		Map<String, Object> tomcatObject = jdbcTemplate.queryForMap("SELECT * FROM " + Tomcat.NAME + " WHERE " + Tomcat.Field.ID + " = ?", this.tomcatId);
		this.hostId = (String) tomcatObject.get(Tomcat.Field.HOST_ID);
	}

	@Override
	protected void initComponent() {
		this.tab = new AjaxTabbedPanel<>("tab", Arrays.asList(new InfoTab(this), new ConfigurationTab(this), new WarTab(this), new LibraryTab(this), new CertificateTab(this), new ConnectorTab(this), new ProtocolTab(this), new CipherTab(this)));
		add(this.tab);
	}

	@Override
	protected void configureMetaData() {
		PageParameters parameters = getPageParameters();
		int tabIndex = parameters.get("tab").toInt(TAB_INFO);
		this.tab.setSelectedTab(tabIndex);
	}

	public void saveButtonSubmit(Button button) {

	}

}
