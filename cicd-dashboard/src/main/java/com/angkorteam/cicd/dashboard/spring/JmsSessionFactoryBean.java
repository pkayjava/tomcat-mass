package com.angkorteam.cicd.dashboard.spring;

import javax.jms.Connection;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

public class JmsSessionFactoryBean implements FactoryBean<JmsSession>, InitializingBean, DisposableBean {

	private String activemqServer = "tcp://172.16.1.107:61616";
	private String clientId = "DASHBOARD";

	private boolean transactional = false;
	private int autoAcknowledge = javax.jms.Session.AUTO_ACKNOWLEDGE;

	private Connection activemqConnection;
	private javax.jms.Session activemqSession;

	private JmsSession session;

	@Override
	public void destroy() throws Exception {
		this.activemqSession.close();
		this.activemqConnection.close();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(this.activemqServer);
			connectionFactory.setClientID(this.clientId);
			this.activemqConnection = connectionFactory.createConnection();
			this.activemqConnection.start();

			// Create a Session
			this.activemqSession = this.activemqConnection.createSession(this.transactional, this.autoAcknowledge);
			this.session = new JmsSession(this.activemqSession);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	@Override
	public JmsSession getObject() throws Exception {
		return this.session;
	}

	@Override
	public Class<?> getObjectType() {
		return JmsSession.class;
	}

	public void setActivemqServer(String activemqServer) {
		this.activemqServer = activemqServer;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

}
