package com.angkorteam.cicd.dashboard.spring;

import java.io.IOException;

import javax.jms.JMSException;

public class JmsSession {

	private javax.jms.Session activemqSession;

	public JmsSession(javax.jms.Session activemqSession) {
		this.activemqSession = activemqSession;
	}

	public JmsMessageProducer createProducer(String destination) throws IOException {
		try {
			javax.jms.MessageProducer producer = this.activemqSession.createProducer(this.activemqSession.createQueue(destination));
			return new JmsMessageProducer(this.activemqSession, producer);
		} catch (JMSException e) {
			throw new IOException(e);
		}
	}

}
