//package com.angkorteam.cicd.dashboard.tab.tomcat.create;
//
//import org.apache.wicket.Page;
//import org.apache.wicket.markup.html.form.TextField;
//import org.apache.wicket.markup.html.link.BookmarkablePageLink;
//import org.apache.wicket.model.PropertyModel;
//import org.apache.wicket.validation.IValidatable;
//import org.apache.wicket.validation.ValidationError;
//
//import com.angkorteam.cicd.dashboard.pages.tomcat.TomcatBrowsePage;
//import com.angkorteam.cicd.dashboard.pages.tomcat.TomcatCreatePage;
//import com.angkorteam.cicd.ddl.Host;
//import com.angkorteam.framework.SpringBean;
//import com.angkorteam.framework.jdbc.SelectQuery;
//import com.angkorteam.framework.spring.JdbcNamed;
//import com.angkorteam.framework.wicket.extensions.markup.html.tabs.AjaxTabbedPanel;
//import com.angkorteam.framework.wicket.extensions.markup.html.tabs.ITab;
//import com.angkorteam.framework.wicket.layout.Size;
//import com.angkorteam.framework.wicket.layout.UIBlock;
//import com.angkorteam.framework.wicket.layout.UIContainer;
//import com.angkorteam.framework.wicket.layout.UIRow;
//import com.angkorteam.framework.wicket.markup.html.form.Button;
//import com.angkorteam.framework.wicket.markup.html.form.Form;
//import com.angkorteam.framework.wicket.markup.html.panel.Panel;
//
//public class InfoPanel extends Panel {
//
//	protected Page itemPage;
//	protected PropertyModel<AjaxTabbedPanel<ITab>> tab;
//	protected PropertyModel<Boolean> errorInfo;
//
//	protected Form<Void> form;
//	protected Button saveButton;
//	protected BookmarkablePageLink<Void> closeLink;
//
//	protected UIRow row1;
//
//	protected UIBlock infoNameBlock;
//	protected UIContainer infoNameIContainer;
//	protected PropertyModel<String> infoNameValue;
//	protected TextField<String> infoNameField;
//
//	protected UIBlock row1Block1;
//
//	public InfoPanel(String id, Page itemPage) {
//		super(id);
//		this.itemPage = itemPage;
//	}
//
//	@Override
//	protected void initData() {
//		this.errorInfo = new PropertyModel<>(this.itemPage, "errorInfo");
//		this.tab = new PropertyModel<>(this.itemPage, "tab");
//
//	}
//
//	@Override
//	protected void initComponent() {
//		this.form = new Form<>("form");
//		add(this.form);
//
//		this.saveButton = new Button("saveButton");
//		this.saveButton.setOnSubmit(this::saveButtonSubmit);
//		this.form.add(this.saveButton);
//
//		this.closeLink = new BookmarkablePageLink<>("closeLink", TomcatBrowsePage.class);
//		this.form.add(this.closeLink);
//
//		this.row1 = UIRow.newUIRow("row1", this.form);
//
//		this.infoNameBlock = this.row1.newUIBlock("infoNameBlock", Size.Six_6);
//		this.infoNameIContainer = this.infoNameBlock.newUIContainer("infoNameIContainer");
//		this.infoNameField = new TextField<>("infoNameField", new PropertyModel<>(this.itemPage, "infoNameValue"));
//		this.infoNameIContainer.add(this.infoNameField);
//		this.infoNameIContainer.newFeedback("infoNameFeedback", this.infoNameField);
//
//		this.row1Block1 = this.row1.newUIBlock("row1Block1", Size.Six_6);
//	}
//
//	@Override
//	protected void configureMetaData() {
//		this.infoNameField.setRequired(true);
//		this.infoNameField.add(this::infoNameValidation);
//	}
//
//	protected void infoNameValidation(IValidatable<String> validatable) {
//		String name = validatable.getValue();
//		if (name != null && !"".equals(name)) {
//			JdbcNamed named = SpringBean.getBean(JdbcNamed.class);
//			SelectQuery query = new SelectQuery(Host.NAME);
//			query.addField("count(*)");
//			query.addWhere(Host.Field.CLIENT_ID + " = :instance_id", name);
//			if (named.queryForObject(query.toSQL(), query.getParam(), Boolean.class)) {
//				validatable.error(new ValidationError("duplicated"));
//			}
//		}
//	}
//
//	protected void saveButtonSubmit(Button button) {
//		if (this.itemPage instanceof TomcatCreatePage) {
//			((TomcatCreatePage) this.itemPage).saveButtonSubmit(button);
//		}
//	}
//
//}
