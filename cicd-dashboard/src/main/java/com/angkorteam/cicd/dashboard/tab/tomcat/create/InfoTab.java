//package com.angkorteam.cicd.dashboard.tab.tomcat.create;
//
//import org.apache.wicket.Page;
//import org.apache.wicket.markup.html.WebMarkupContainer;
//import org.apache.wicket.model.IModel;
//import org.apache.wicket.model.Model;
//
//import com.angkorteam.framework.wicket.extensions.markup.html.tabs.ITab;
//
//public class InfoTab extends ITab {
//
//	protected Page itemPage;
//
//	public InfoTab(Page itemPage) {
//		this.itemPage = itemPage;
//	}
//
//	@Override
//	public IModel<String> getTitle() {
//		return Model.of("1. Info");
//	}
//
//	@Override
//	public WebMarkupContainer getPanel(String containerId) {
//		return new InfoPanel(containerId, this.itemPage);
//	}
//
//	@Override
//	public boolean isVisible() {
//		return true;
//	}
//
//	@Override
//	public boolean isEnabled() {
//		return true;
//	}
//}
