package com.angkorteam.cicd.dashboard.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.angkorteam.cicd.ddl.File;
import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.cicd.ddl.Jdk;
import com.angkorteam.cicd.ddl.Tomcat;
import com.angkorteam.cicd.ddl.TomcatFile;
import com.angkorteam.cicd.xml.XmlFile;
import com.angkorteam.cicd.xml.command.TomcatFileCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;
import com.angkorteam.framework.Function;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.jdbc.SelectQuery;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.spring.JdbcTemplate;

@Controller
public class ServerController {

	@RequestMapping(path = "/server/{clientId}/{instance}")
	public void server(Principal principal, @PathVariable("clientId") String clientId, @PathVariable("instance") String instance, HttpServletRequest request, HttpServletResponse response) throws IOException {
		JdbcNamed named = SpringBean.getBean(JdbcNamed.class);
		JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);

		Map<String, Object> hostObject = jdbcTemplate.queryForMap("SELECT " + Host.Field.ID + ", " + Host.Field.USER_ID + " FROM " + Host.NAME + " WHERE " + Host.Field.CLIENT_ID + " = ?", clientId);
		if (hostObject == null || !principal.getName().equals(hostObject.get(Host.Field.USER_ID))) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}

		SelectQuery selectQuery = new SelectQuery(Tomcat.NAME);

		selectQuery.addWhere(Tomcat.Field.HOST_ID + " = :hostId", hostObject.get(Host.Field.ID));
		selectQuery.addWhere(Tomcat.Field.INSTANCE + " = :instanceId", instance);

		selectQuery.addField(Tomcat.Field.ID);
		selectQuery.addField(Tomcat.Field.JDK_ID);

		Map<String, Object> tomcatObject = named.queryForMap(selectQuery.toSQL(), selectQuery.getParam());

		if (tomcatObject == null || tomcatObject.get(Tomcat.Field.ID) == null || "".equals(tomcatObject.get(Tomcat.Field.ID))) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		String tomcatId = (String) tomcatObject.get(Tomcat.Field.ID);

		String jdkName = jdbcTemplate.queryForObject("SELECT " + Jdk.Field.JDK_NAME + " FROM " + Jdk.NAME + " WHERE " + Jdk.Field.ID + " = ?", String.class, tomcatObject.get(Tomcat.Field.JDK_ID));

		TomcatFileCommand command = new TomcatFileCommand();
		command.setInstance(instance);
		command.setJdkName(jdkName);

		selectQuery = new SelectQuery(TomcatFile.NAME);
		selectQuery.addJoin("INNER JOIN " + File.NAME + " ON " + TomcatFile.NAME + "." + TomcatFile.Field.FILE_ID + " = " + File.NAME + "." + File.Field.ID);

		selectQuery.addField(TomcatFile.NAME + "." + TomcatFile.Field.FILE_ID);
		selectQuery.addField(TomcatFile.NAME + "." + TomcatFile.Field.PATH);
		selectQuery.addField(File.NAME + "." + File.Field.FILE_SHA);

		selectQuery.addWhere(TomcatFile.NAME + "." + TomcatFile.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);

		String httpAddress = Function.getHttpAddress(request);
		List<Map<String, Object>> fileObjects = named.queryForList(selectQuery.toSQL(), selectQuery.getParam());
		for (Map<String, Object> fileObject : fileObjects) {
			XmlFile file = new XmlFile();
			file.setDownload(httpAddress + "/api/file/" + (String) fileObject.get(TomcatFile.Field.FILE_ID));
			file.setPath((String) fileObject.get(TomcatFile.Field.PATH));
			file.setSha((String) fileObject.get(File.Field.FILE_SHA));
			command.getFiles().add(file);
		}
		response.getWriter().write(new XmlMessage(command).toText());
	}

}
