package com.angkorteam.cicd.dashboard.pages.jdk;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.angkorteam.cicd.dashboard.WicketSession;
import com.angkorteam.cicd.dashboard.pages.Page;
import com.angkorteam.cicd.dashboard.provider.JdbcProvider;
import com.angkorteam.cicd.ddl.File;
import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.cicd.ddl.Jdk;
import com.angkorteam.framework.SpringBean;
import com.angkorteam.framework.models.PageBreadcrumb;
import com.angkorteam.framework.spring.JdbcTemplate;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.cell.TextCell;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ActionFilterColumn;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ActionItem;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.FilterToolbar;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemClass;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemCss;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.ItemPanel;
import com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter.TextFilterColumn;
import com.angkorteam.framework.wicket.layout.Size;
import com.angkorteam.framework.wicket.layout.UIBlock;
import com.angkorteam.framework.wicket.layout.UIContainer;
import com.angkorteam.framework.wicket.layout.UIRow;
import com.google.common.collect.Lists;

@AuthorizeInstantiation(WicketSession.ALL_FUNCTION)
public class JdkBrowsePage extends Page {

	protected FilterForm<Map<String, String>> form;

	protected UIRow row1;

	protected UIBlock dataBlock;
	protected UIContainer dataIContainer;
	protected DataTable<Map<String, Object>, String> dataTable;
	protected JdbcProvider dataProvider;
	protected List<IColumn<Map<String, Object>, String>> dataColumn;

	protected BookmarkablePageLink<Void> createLink;

	@Override
	public IModel<List<PageBreadcrumb>> buildPageBreadcrumb() {
		List<PageBreadcrumb> BREADCRUMB = Lists.newArrayList();
		{
			BREADCRUMB.add(new PageBreadcrumb.Builder().withLabel(Model.of("Jdk")).build());
		}
		return Model.ofList(BREADCRUMB);
	}

	@Override
	protected void initComponent() {
		this.createLink = new BookmarkablePageLink<>("createLink", JdkCreatePage.class);
		add(this.createLink);

		this.form = new FilterForm<>("form", this.dataProvider);
		add(this.form);

		this.row1 = UIRow.newUIRow("row1", this.form);

		this.dataBlock = this.row1.newUIBlock("dataBlock", Size.Twelve_12);
		this.dataIContainer = this.dataBlock.newUIContainer("dataIContainer");
		this.dataTable = new DefaultDataTable<>("dataTable", this.dataColumn, this.dataProvider, 20);
		this.dataTable.addTopToolbar(new FilterToolbar(this.dataTable, this.form));
		this.dataIContainer.add(this.dataTable);
	}

	@Override
	protected void configureMetaData() {
	}

	@Override
	protected void initData() {
		this.dataProvider = new JdbcProvider(Jdk.NAME);
		this.dataProvider.applyJoin("host", "INNER JOIN " + Host.NAME + " ON " + Host.NAME + "." + Host.Field.ID + " = " + Jdk.NAME + "." + Jdk.Field.HOST_ID);
		this.dataProvider.applyJoin("file", "INNER JOIN " + File.NAME + " ON " + File.NAME + "." + File.Field.ID + " = " + Jdk.NAME + "." + Jdk.Field.FILE_ID);

		this.dataProvider.boardField(Jdk.NAME + "." + Jdk.Field.ID, "id", String.class);
		this.dataProvider.boardField(Host.NAME + "." + Host.Field.CLIENT_ID, "host", String.class);
		this.dataProvider.boardField(Jdk.NAME + "." + Jdk.Field.JDK_NAME, "jdk", String.class);
		this.dataProvider.boardField(File.NAME + "." + File.Field.FILE_SIZE, "size", String.class);

		this.dataProvider.selectField("id", String.class);

		this.dataProvider.applyWhere("user_id", Jdk.NAME + "." + Jdk.Field.USER_ID + " = '" + getSession().getUserId() + "'");

		this.dataColumn = new ArrayList<>();
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Host"), "host", "host", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("JDK"), "jdk", "jdk", this::dataColumn));
		this.dataColumn.add(new TextFilterColumn(this.dataProvider, ItemClass.String, Model.of("Size"), "size", "size", this::dataColumn));

		this.dataColumn.add(new ActionFilterColumn<>(Model.of("Action"), this::dataAction, this::dataClick));
	}

	protected void dataClick(String column, Map<String, Object> model, AjaxRequestTarget target) {
		if ("delete".equals(column)) {
			String jdkId = (String) model.get("id");
			JdbcTemplate jdbcTemplate = SpringBean.getBean(JdbcTemplate.class);
			jdbcTemplate.update("DELETE FROM " + Jdk.NAME + " WHERE " + Jdk.Field.ID + " = ?", jdkId);
		}
		if (target != null) {
			target.add(this.dataTable);
		}
	}

	protected List<ActionItem> dataAction(String column, Map<String, Object> model) {
		List<ActionItem> actions = Lists.newArrayList();
		actions.add(new ActionItem("delete", Model.of("Delete"), ItemCss.PRIMARY));
		return actions;
	}

	protected ItemPanel dataColumn(String column, IModel<String> display, Map<String, Object> model) {
		if ("host".equals(column) || "jdk".equals(column) || "size".equals(column)) {
			String value = (String) model.get(column);
			return new TextCell(value);
		}
		throw new WicketRuntimeException("Unknown " + column);
	}

}
