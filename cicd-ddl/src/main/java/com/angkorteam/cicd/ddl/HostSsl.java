package com.angkorteam.cicd.ddl;

public interface HostSsl {

	public static final String NAME = "tbl_host_ssl";

	public static final String TYPE_JDK = "JDK";

	public static final String TYPE_OpenSSL = "OpenSSL";

	public interface Field {

		public static final String ID = "id";

		public static final String HOST_ID = "host_id";

		public static final String NAME = "name";

		public static final String TYPE = "type";

		public static final String SELECTED = "selected";

	}

}
