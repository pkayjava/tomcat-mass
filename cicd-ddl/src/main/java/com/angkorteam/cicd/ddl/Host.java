package com.angkorteam.cicd.ddl;

public interface Host {

	public static final String NAME = "tbl_host";

	public interface Field {

		public static final String ID = "id";

		public static final String USER_ID = "user_id";

		public static final String CLIENT_ID = "client_id";

		public static final String STATUS = "status";

		public static final String OS_NAME = "os_name";

		public static final String OS_VERSION = "os_version";

		public static final String OS_ARCH = "os_arch";

		public static final String HOST_HOME = "host_home";

		public static final String CONNECTED = "connected";

		public static final String LOCAL_PORT = "local_port";
		
		public static final String IP_ADDRESS = "ip_address";

	}

}
