package com.angkorteam.cicd.ddl;

public interface TomcatVHostPort {

	public static final String NAME = "tbl_tomcat_vhost_port";

	public interface Field {

		public static final String ID = "id";

		public static final String TOMCAT_ID = "tomcat_id";

		public static final String TOMCAT_VHOST_ID = "tomcat_vhost_id";

		public static final String PORT = "port";

		public static final String TLS_MODE = "tls_mode";

		public static final String TLS = "tls";

	}

}
