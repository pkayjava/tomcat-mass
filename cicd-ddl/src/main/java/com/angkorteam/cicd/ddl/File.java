package com.angkorteam.cicd.ddl;

public interface File {

	public static final String NAME = "tbl_file";

	public interface Field {

		public static final String ID = "id";

		public static final String USER_ID = "user_id";

		public static final String TYPE = "type";

		public static final String NAME = "name";

		public static final String PATH = "path";

		public static final String FILE_SHA = "file_sha";

		public static final String FILE_SIZE = "file_size";

	}

}
