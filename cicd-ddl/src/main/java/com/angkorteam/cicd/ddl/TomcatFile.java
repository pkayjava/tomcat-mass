package com.angkorteam.cicd.ddl;

public interface TomcatFile {

	public static final String NAME = "tbl_tomcat_file";

	public static final String TYPE_WAR = "WAR";
	public static final String TYPE_BACKGROUND = "BACKGROUND";
	public static final String TYPE_BASE = "BASE";
	public static final String TYPE_SSL = "SSL";
	public static final String TYPE_LIBRARY = "LIBRARY";
	public static final String TYPE_SERVER = "SERVER";

	public static final String STATUS_STARTED = "STARTED";
	public static final String STATUS_STOPPED = "STOPPED";
	public static final String STATUS_NA = "N/A";

	public interface Field {

		public static final String ID = "id";

		public static final String TOMCAT_ID = "tomcat_id";

		public static final String FILE_ID = "file_id";

		public static final String PATH = "path";

		public static final String TYPE = "type";

		public static final String READONLY = "readonly";

		public static final String STATUS = "status";

		public static final String LAST_MODIFIED = "last_modified";

		public static final String VHOST = "vhost";

	}

}
