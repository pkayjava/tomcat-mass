package com.angkorteam.cicd.ddl;

public interface HostPort {

	public static final String NAME = "tbl_host_port";

	public interface Field {

		public static final String ID = "id";

		public static final String USER_ID = "user_id";

		public static final String TOMCAT_ID = "tomcat_id";

		public static final String HOST_ID = "host_id";

		public static final String PORT = "port";

	}

}
