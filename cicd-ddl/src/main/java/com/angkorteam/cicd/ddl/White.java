package com.angkorteam.cicd.ddl;

public interface White {

	public static final String NAME = "tbl_white";

	public interface Field {

		public static final String ID = "id";

		public static final String CLIENT_ID = "client_id";

		public static final String REVOKED = "revoked";

		public static final String CONNECTED = "connected";

	}

}