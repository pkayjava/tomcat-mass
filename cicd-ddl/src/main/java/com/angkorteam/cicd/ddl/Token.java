package com.angkorteam.cicd.ddl;

public interface Token {

	public static final String NAME = "tbl_token";

	public interface Field {

		public static final String ID = "id";

		public static final String USER_ID = "user_id";

		public static final String TOKEN_ID = "token_id";

		public static final String TOKEN_SECRET = "token_secret";

		public static final String REVOKED = "revoked";

		public static final String ISSUED_DATETIME = "issued_datetime";

		public static final String REVOKED_DATETIME = "revoked_datetime";

	}

}
