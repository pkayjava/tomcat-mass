package com.angkorteam.cicd.ddl;

public interface Jdk {

	public static final String NAME = "tbl_jdk";

	public interface Field {

		public static final String ID = "id";

		public static final String HOST_ID = "host_id";

		public static final String JDK_NAME = "jdk_name";

		public static final String FILE_ID = "file_id";

		public static final String USER_ID = "user_id";

	}

}
