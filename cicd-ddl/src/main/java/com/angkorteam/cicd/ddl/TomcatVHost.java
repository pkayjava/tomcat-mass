package com.angkorteam.cicd.ddl;

public interface TomcatVHost {

	public static final String NAME = "tbl_tomcat_vhost";

	public interface Field {

		public static final String ID = "id";

		public static final String TOMCAT_ID = "tomcat_id";

		public static final String NAME = "name";

	}

}
