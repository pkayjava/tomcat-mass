package com.angkorteam.cicd.ddl;

public interface Tomcat {

	public static final String NAME = "tbl_tomcat";

	public interface Field {

		public static final String ID = "id";

		public static final String HOST_ID = "host_id";

		public static final String USER_ID = "user_id";

		public static final String INSTANCE = "instance";

		public static final String CONFIGURATION = "configuration";

		public static final String STATUS = "status";

		public static final String TOMCAT_VERSION = "tomcat_version";

		public static final String JVM_VERSION = "jvm_version";

		public static final String JVM_VENDOR = "jvm_vendor";

		public static final String SHUTDOWN_PORT = "shutdown_port";

		public static final String JDK_ID = "jdk_id";

		public static final String PORT_ERROR = "port_error";

	}

}
