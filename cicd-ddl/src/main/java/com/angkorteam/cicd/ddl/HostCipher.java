package com.angkorteam.cicd.ddl;

public interface HostCipher {

	public static final String NAME = "tbl_host_cipher";

	public interface Field {

		public static final String ID = "id";

		public static final String HOST_SSL_ID = "host_ssl_id";
		
		public static final String HOST_ID = "host_id";

		public static final String NAME = "name";

	}

}
