package com.angkorteam.cicd.ddl;

public interface User {

	public static final String NAME = "tbl_user";

	public interface Field {

		public static final String ID = "id";

		public static final String FIRST_NAME = "first_name";

		public static final String LAST_NAME = "last_name";

		public static final String EMAIL_ADDRESS = "email_address";

		public static final String LOGIN = "login";

		public static final String PASSWORD = "password";

		public static final String ENABLED = "enabled";

		public static final String CONFIRMED = "confirmed";

	}

}
