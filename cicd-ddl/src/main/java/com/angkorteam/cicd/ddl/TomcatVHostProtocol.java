package com.angkorteam.cicd.ddl;

public interface TomcatVHostProtocol {

	public static final String NAME = "tbl_tomcat_vhost_protocol";

	public interface Field {

		public static final String ID = "id";

		public static final String TOMCAT_ID = "tomcat_id";

		public static final String TOMCAT_VHOST_ID = "tomcat_vhost_id";

		public static final String NAME = "name";

	}

}
