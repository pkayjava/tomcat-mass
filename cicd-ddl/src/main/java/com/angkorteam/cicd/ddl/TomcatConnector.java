package com.angkorteam.cicd.ddl;

public interface TomcatConnector {

	public static final String NAME = "tbl_tomcat_connector";

	public interface Field {

		public static final String ID = "id";

		public static final String TOMCAT_ID = "tomcat_id";

		public static final String PORT = "port";

		public static final String PROTOCOL = "protocol";

		public static final String URI_ENCODING = "uri_encoding";

		public static final String MAX_THREAD = "max_thread";

		public static final String SSL_ENABLED = "ssl_enabled";

		public static final String SSL_IMPLEMENTATION_NAME = "ssl_implementation_name";

	}

}
