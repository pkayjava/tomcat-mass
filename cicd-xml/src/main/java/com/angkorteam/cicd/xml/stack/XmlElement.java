package com.angkorteam.cicd.xml.stack;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlElement {

	@XmlAttribute(name = "declaringClass")
	private String declaringClass;

	@XmlAttribute(name = "methodName")
	private String methodName;

	@XmlAttribute(name = "fileName")
	private String fileName;

	@XmlAttribute(name = "lineNumber")
	private Integer lineNumber;

	@XmlAttribute(name = "index")
	private Integer index;

	public XmlElement() {
	}

	public XmlElement(int index, StackTraceElement element) {
		this.declaringClass = element.getClassName();
		this.methodName = element.getMethodName();
		this.lineNumber = element.getLineNumber();
		this.fileName = element.getFileName();
		this.index = index;
	}

	public String getDeclaringClass() {
		return declaringClass;
	}

	public void setDeclaringClass(String declaringClass) {
		this.declaringClass = declaringClass;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

}
