package com.angkorteam.cicd.xml.tomcat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.namespace.QName;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlGlobalNamingResources {

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	@XmlElement(name = "Resource")
	private List<XmlResource> resources = new ArrayList<XmlResource>();

	@XmlElement(name = "Environment")
	private List<XmlEnvironment> environments = new ArrayList<XmlEnvironment>();

	public List<XmlResource> getResources() {
		return resources;
	}

	public List<XmlEnvironment> getEnvironments() {
		return environments;
	}

	public Map<QName, String> getExtension() {
		return extension;
	}

}
