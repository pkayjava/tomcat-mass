package com.angkorteam.cicd.xml.tomcat;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.namespace.QName;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlRealm {

	@XmlAttribute(name = "className")
	private String className;

	@XmlAttribute(name = "resourceName")
	private String resourceName;

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	@XmlElement(name = "Realm")
	private XmlRealm realm;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public XmlRealm getRealm() {
		return realm;
	}

	public void setRealm(XmlRealm realm) {
		this.realm = realm;
	}

	public Map<QName, String> getExtension() {
		return extension;
	}

}
