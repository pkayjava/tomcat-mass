package com.angkorteam.cicd.xml.command;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "tomcatstatus")
@XmlAccessorType(XmlAccessType.FIELD)
public class TomcatStatusCommand extends Command {

	@XmlAttribute(name = "clientId")
	private String clientId;

	@XmlAttribute(name = "instance")
	private String instance;

	@XmlAttribute(name = "status")
	private String status;

	@XmlAttribute(name = "tomcatVersion")
	private String tomcatVersion;

	@XmlAttribute(name = "jvmVersion")
	private String jvmVersion;

	@XmlAttribute(name = "jvmVendor")
	private String jvmVendor;

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getJvmVersion() {
		return jvmVersion;
	}

	public void setJvmVersion(String jvmVersion) {
		this.jvmVersion = jvmVersion;
	}

	public String getJvmVendor() {
		return jvmVendor;
	}

	public void setJvmVendor(String jvmVendor) {
		this.jvmVendor = jvmVendor;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getTomcatVersion() {
		return tomcatVersion;
	}

	public void setTomcatVersion(String tomcatVersion) {
		this.tomcatVersion = tomcatVersion;
	}

}
