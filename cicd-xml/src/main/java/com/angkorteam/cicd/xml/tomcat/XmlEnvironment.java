package com.angkorteam.cicd.xml.tomcat;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.namespace.QName;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlEnvironment {

	@XmlAttribute(name = "description")
	private String description;

	@XmlAttribute(name = "name")
	private String name;

	@XmlAttribute(name = "override")
	private Boolean override;

	@XmlAttribute(name = "type")
	private String type;

	@XmlAttribute(name = "value")
	private String value;

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

	public Boolean getOverride() {
		return override;
	}

	public String getType() {
		return type;
	}

	public String getValue() {
		return value;
	}

	public Map<QName, String> getExtension() {
		return extension;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOverride(Boolean override) {
		this.override = override;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
