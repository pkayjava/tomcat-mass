package com.angkorteam.cicd.xml.command;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.angkorteam.cicd.xml.XmlJdk;

@XmlRootElement(name = "hostjdk")
@XmlAccessorType(XmlAccessType.FIELD)
public class HostJdkCommand extends Command {

	@XmlElement(name = "jdk")
	private List<XmlJdk> jdk = new ArrayList<XmlJdk>();

	public List<XmlJdk> getJdk() {
		return jdk;
	}

	public void setJdk(List<XmlJdk> jdk) {
		this.jdk = jdk;
	}

}
