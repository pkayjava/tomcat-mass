package com.angkorteam.cicd.xml.tomcat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.namespace.QName;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlHost {

	@XmlAttribute(name = "name")
	private String name;

	@XmlAttribute(name = "appBase")
	private String appBase;

	@XmlAttribute(name = "unpackWARs")
	private Boolean unpackWARs;

	@XmlAttribute(name = "autoDeploy")
	private Boolean autoDeploy;

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	@XmlElement(name = "Valve")
	private List<XmlValve> valves = new ArrayList<XmlValve>();

	public String getName() {
		return name;
	}

	public String getAppBase() {
		return appBase;
	}

	public Boolean getUnpackWARs() {
		return unpackWARs;
	}

	public Boolean getAutoDeploy() {
		return autoDeploy;
	}

	public List<XmlValve> getValves() {
		return valves;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAppBase(String appBase) {
		this.appBase = appBase;
	}

	public void setUnpackWARs(Boolean unpackWARs) {
		this.unpackWARs = unpackWARs;
	}

	public void setAutoDeploy(Boolean autoDeploy) {
		this.autoDeploy = autoDeploy;
	}

	public Map<QName, String> getExtension() {
		return extension;
	}

}
