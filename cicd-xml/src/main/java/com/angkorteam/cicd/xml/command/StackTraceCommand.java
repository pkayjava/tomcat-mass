package com.angkorteam.cicd.xml.command;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.angkorteam.cicd.xml.stack.XmlElement;

@XmlRootElement(name = "stacktrace")
@XmlAccessorType(XmlAccessType.FIELD)
public class StackTraceCommand extends Command {

	@XmlAttribute(name = "clientId")
	private String clientId;

	@XmlAttribute(name = "instance")
	private String instance;

	@XmlAttribute(name = "when")
	private Date when;

	@XmlAttribute(name = "message")
	private String message;

	@XmlAttribute(name = "javaClass")
	private String javaClass;

	@javax.xml.bind.annotation.XmlElement(name = "javaClass")
	private List<XmlElement> elements = new ArrayList<XmlElement>();

	public StackTraceCommand() {
	}

	public StackTraceCommand(Throwable throwable) {
		this.message = throwable.getMessage();
		this.when = new Date();
		this.javaClass = throwable.getClass().getName();
		for (int i = 0; i < throwable.getStackTrace().length; i++) {
			XmlElement element = new XmlElement(i, throwable.getStackTrace()[i]);
			this.elements.add(element);
		}
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public Date getWhen() {
		return when;
	}

	public void setWhen(Date when) {
		this.when = when;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getJavaClass() {
		return javaClass;
	}

	public void setJavaClass(String javaClass) {
		this.javaClass = javaClass;
	}

	public List<XmlElement> getElements() {
		return elements;
	}

}
