package com.angkorteam.cicd.xml.message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.angkorteam.cicd.xml.XmlFile;
import com.angkorteam.cicd.xml.command.ApplicationCommand;
import com.angkorteam.cicd.xml.command.Command;
import com.angkorteam.cicd.xml.command.DebugCommand;
import com.angkorteam.cicd.xml.command.DeployTomcatCommand;
import com.angkorteam.cicd.xml.command.HostStatusCommand;
import com.angkorteam.cicd.xml.command.HostJdkCommand;
import com.angkorteam.cicd.xml.command.KillCommand;
import com.angkorteam.cicd.xml.command.RestartCommand;
import com.angkorteam.cicd.xml.command.RestartJdkCommand;
import com.angkorteam.cicd.xml.command.StackTraceCommand;
import com.angkorteam.cicd.xml.command.StartCommand;
import com.angkorteam.cicd.xml.command.StopCommand;
import com.angkorteam.cicd.xml.command.TomcatFileCommand;
import com.angkorteam.cicd.xml.command.TomcatStatusCommand;

@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlMessage {

	private static JAXBContext JAXB;
	private static Marshaller MARSHALLER;
	private static Unmarshaller UNMARSHALLER;
	private static XMLOutputFactory OUTPUT;

	static {
		try {
			JAXB = JAXBContext.newInstance(XmlMessage.class, TomcatFileCommand.class, DeployTomcatCommand.class, ApplicationCommand.class, HostStatusCommand.class, TomcatStatusCommand.class, KillCommand.class, StartCommand.class, StopCommand.class, RestartCommand.class, XmlFile.class, StackTraceCommand.class, com.angkorteam.cicd.xml.stack.XmlElement.class, DebugCommand.class, HostJdkCommand.class, RestartJdkCommand.class);
			OUTPUT = XMLOutputFactory.newFactory();
			UNMARSHALLER = JAXB.createUnmarshaller();
			MARSHALLER = JAXB.createMarshaller();
			MARSHALLER.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);
			MARSHALLER.setProperty(Marshaller.JAXB_FRAGMENT, true);
			MARSHALLER.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		} catch (javax.xml.bind.JAXBException e) {
		}
	}

	@XmlAttribute(name = "commandClass")
	private String commandClass;

	@XmlElement(name = "xml")
	private String xml;

	public XmlMessage() {
	}

	public XmlMessage(Command command) {
		this.commandClass = command.getClass().getName();
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			XMLStreamWriter sw = OUTPUT.createXMLStreamWriter(baos, (String) MARSHALLER.getProperty(Marshaller.JAXB_ENCODING));
			sw.writeStartDocument((String) MARSHALLER.getProperty(Marshaller.JAXB_ENCODING), "1.0");
			MARSHALLER.marshal(command, sw);
			sw.writeEndDocument();
			sw.close();
			this.xml = new String(baos.toByteArray());
		} catch (JAXBException e) {
		} catch (XMLStreamException e) {
		}
	}

	public String getCommandClass() {
		return this.commandClass;
	}

	public static XmlMessage fromText(String text) {
		try {
			return (XmlMessage) UNMARSHALLER.unmarshal(new ByteArrayInputStream(text.getBytes()));
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

	public String toText() {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			XMLStreamWriter sw = OUTPUT.createXMLStreamWriter(baos, (String) MARSHALLER.getProperty(Marshaller.JAXB_ENCODING));
			sw.writeStartDocument((String) MARSHALLER.getProperty(Marshaller.JAXB_ENCODING), "1.0");
			MARSHALLER.marshal(this, sw);
			sw.writeEndDocument();
			sw.close();
			return new String(baos.toByteArray());
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		} catch (XMLStreamException e) {
			throw new RuntimeException(e);
		}
	}

	public Command getCommand() {
		try {
			return (Command) UNMARSHALLER.unmarshal(new ByteArrayInputStream(this.xml.getBytes()));
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

}
