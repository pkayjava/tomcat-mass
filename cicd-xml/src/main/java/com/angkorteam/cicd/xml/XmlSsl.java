package com.angkorteam.cicd.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlSsl {

	@XmlAttribute(name = "implementation")
	private String implementation;

	@XmlAttribute(name = "type")
	private String type;

	@XmlAttribute(name = "selected")
	private Boolean selected = false;

	@XmlElement(name = "protocol")
	private List<String> protocol = new ArrayList<String>();

	@XmlElement(name = "cipher")
	private List<String> cipher = new ArrayList<String>();

	public String getImplementation() {
		return implementation;
	}

	public void setImplementation(String implementation) {
		this.implementation = implementation;
	}

	public List<String> getProtocol() {
		return protocol;
	}

	public List<String> getCipher() {
		return cipher;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

}
