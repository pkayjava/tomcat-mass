package com.angkorteam.cicd.xml.tomcat;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.namespace.QName;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlResource {

	@XmlAttribute(name = "name")
	private String name;

	@XmlAttribute(name = "auth")
	private String auth;

	@XmlAttribute(name = "type")
	private String type;

	@XmlAttribute(name = "description")
	private String description;

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	public String getName() {
		return name;
	}

	public String getAuth() {
		return auth;
	}

	public String getType() {
		return type;
	}

	public String getDescription() {
		return description;
	}

	public Map<QName, String> getExtension() {
		return extension;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
