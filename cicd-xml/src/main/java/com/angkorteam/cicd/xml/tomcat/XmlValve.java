package com.angkorteam.cicd.xml.tomcat;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.namespace.QName;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlValve {

	@XmlAttribute(name = "className")
	private String className;

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Map<QName, String> getExtension() {
		return extension;
	}

}
