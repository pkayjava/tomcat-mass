package com.angkorteam.cicd.xml.command;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.angkorteam.cicd.xml.XmlFile;

@XmlRootElement(name = "tomcatfile")
@XmlAccessorType(XmlAccessType.FIELD)
public class TomcatFileCommand extends Command {

	@XmlAttribute(name = "instance")
	private String instance;

	@XmlAttribute(name = "jdkName")
	private String jdkName;

	@XmlElement(name = "file")
	private List<XmlFile> files = new ArrayList<XmlFile>();

	public String getJdkName() {
		return jdkName;
	}

	public void setJdkName(String jdkName) {
		this.jdkName = jdkName;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public List<XmlFile> getFiles() {
		return files;
	}

}
