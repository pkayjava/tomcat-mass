package com.angkorteam.cicd.xml.command;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "kill")
@XmlAccessorType(XmlAccessType.FIELD)
public class KillCommand extends Command {

	@XmlAttribute(name = "instance")
	private String instance;

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

}
