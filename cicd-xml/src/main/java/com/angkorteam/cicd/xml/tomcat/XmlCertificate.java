package com.angkorteam.cicd.xml.tomcat;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.namespace.QName;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlCertificate {

	@XmlAttribute(name = "certificateChainFile")
	private String certificateChainFile;

	@XmlAttribute(name = "certificateFile")
	private String certificateFile;

	@XmlAttribute(name = "certificateKeyFile")
	private String certificateKeyFile;

	@XmlAttribute(name = "certificateKeyPassword")
	private String certificateKeyPassword;

	@XmlAttribute(name = "certificateKeystoreFile")
	private String certificateKeystoreFile;

	@XmlAttribute(name = "certificateKeystorePassword")
	private String certificateKeystorePassword;

	@XmlAttribute(name = "certificateKeyAlias")
	private String certificateKeyAlias;

	@XmlAttribute(name = "type")
	private String type;

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	public String getCertificateChainFile() {
		return certificateChainFile;
	}

	public String getCertificateFile() {
		return certificateFile;
	}

	public String getCertificateKeyFile() {
		return certificateKeyFile;
	}

	public String getCertificateKeyPassword() {
		return certificateKeyPassword;
	}

	public String getCertificateKeystoreFile() {
		return certificateKeystoreFile;
	}

	public String getCertificateKeystorePassword() {
		return certificateKeystorePassword;
	}

	public String getCertificateKeyAlias() {
		return certificateKeyAlias;
	}

	public String getType() {
		return type;
	}

	public Map<QName, String> getExtension() {
		return extension;
	}

	public void setCertificateChainFile(String certificateChainFile) {
		this.certificateChainFile = certificateChainFile;
	}

	public void setCertificateFile(String certificateFile) {
		this.certificateFile = certificateFile;
	}

	public void setCertificateKeyFile(String certificateKeyFile) {
		this.certificateKeyFile = certificateKeyFile;
	}

	public void setCertificateKeyPassword(String certificateKeyPassword) {
		this.certificateKeyPassword = certificateKeyPassword;
	}

	public void setCertificateKeystoreFile(String certificateKeystoreFile) {
		this.certificateKeystoreFile = certificateKeystoreFile;
	}

	public void setCertificateKeystorePassword(String certificateKeystorePassword) {
		this.certificateKeystorePassword = certificateKeystorePassword;
	}

	public void setCertificateKeyAlias(String certificateKeyAlias) {
		this.certificateKeyAlias = certificateKeyAlias;
	}

	public void setType(String type) {
		this.type = type;
	}

}
