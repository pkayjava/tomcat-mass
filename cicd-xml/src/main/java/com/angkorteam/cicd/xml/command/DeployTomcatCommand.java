package com.angkorteam.cicd.xml.command;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "deploytomcat")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeployTomcatCommand extends Command {

	@XmlAttribute(name = "instance")
	private String instance;

	@XmlAttribute(name = "server")
	private String server;

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

}
