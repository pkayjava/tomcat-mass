package com.angkorteam.cicd.xml.tomcat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.namespace.QName;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlEngine {

	@XmlAttribute(name = "name")
	private String name;

	@XmlAttribute(name = "defaultHost")
	private String defaultHost;

	@XmlAttribute(name = "jvmRoute")
	private String jvmRoute;

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	@XmlElement(name = "Cluster")
	private XmlCluster cluster;

	@XmlElement(name = "Realm")
	private XmlRealm realm;

	@XmlElement(name = "Host")
	private List<XmlHost> hosts = new ArrayList<XmlHost>();

	public List<XmlHost> getHosts() {
		return hosts;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefaultHost() {
		return defaultHost;
	}

	public void setDefaultHost(String defaultHost) {
		this.defaultHost = defaultHost;
	}

	public XmlRealm getRealm() {
		return realm;
	}

	public void setRealm(XmlRealm realm) {
		this.realm = realm;
	}

	public String getJvmRoute() {
		return jvmRoute;
	}

	public void setJvmRoute(String jvmRoute) {
		this.jvmRoute = jvmRoute;
	}

	public Map<QName, String> getExtension() {
		return extension;
	}

	public XmlCluster getCluster() {
		return cluster;
	}

	public void setCluster(XmlCluster cluster) {
		this.cluster = cluster;
	}

}
