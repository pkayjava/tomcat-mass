package com.angkorteam.cicd.xml.tomcat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

@XmlRootElement(name = "Server")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlServer {

	private static JAXBContext JAXB;
	private static Marshaller MARSHALLER;
	private static Unmarshaller UNMARSHALLER;
	private static XMLOutputFactory OUTPUT;

	static {
		try {
			OUTPUT = XMLOutputFactory.newFactory();
			JAXB = JAXBContext.newInstance(XmlServer.class);
			UNMARSHALLER = JAXB.createUnmarshaller();
			MARSHALLER = JAXB.createMarshaller();
			MARSHALLER.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);
			MARSHALLER.setProperty(Marshaller.JAXB_FRAGMENT, true);
			MARSHALLER.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		} catch (javax.xml.bind.JAXBException e) {
		}
	}

	public static XmlServer fromText(String text) {
		try {
			return (XmlServer) UNMARSHALLER.unmarshal(new ByteArrayInputStream(text.getBytes()));
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

	public String toText() {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			XMLStreamWriter sw = OUTPUT.createXMLStreamWriter(baos, (String) MARSHALLER.getProperty(Marshaller.JAXB_ENCODING));
			sw.writeStartDocument((String) MARSHALLER.getProperty(Marshaller.JAXB_ENCODING), "1.0");
			MARSHALLER.marshal(this, sw);
			sw.writeEndDocument();
			sw.close();
			return new String(baos.toByteArray());
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		} catch (XMLStreamException e) {
			throw new RuntimeException(e);
		}
	}

	@XmlAttribute(name = "port")
	private Integer port;

	@XmlAttribute(name = "shutdown")
	private String shutdown;

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	@XmlElement(name = "Listener")
	private List<XmlListener> listener = new ArrayList<XmlListener>();

	@XmlElement(name = "GlobalNamingResources")
	private XmlGlobalNamingResources globalNamingResources = new XmlGlobalNamingResources();

	@XmlElement(name = "Service")
	private XmlService service = new XmlService();

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getShutdown() {
		return shutdown;
	}

	public void setShutdown(String shutdown) {
		this.shutdown = shutdown;
	}

	public List<XmlListener> getListener() {
		return listener;
	}

	public void setGlobalNamingResources(XmlGlobalNamingResources globalNamingResources) {
		this.globalNamingResources = globalNamingResources;
	}

	public void setService(XmlService service) {
		this.service = service;
	}

	public XmlGlobalNamingResources getGlobalNamingResources() {
		return this.globalNamingResources;
	}

	public XmlService getService() {
		return this.service;
	}

	public Map<QName, String> getExtension() {
		return extension;
	}

}
