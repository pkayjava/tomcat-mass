package com.angkorteam.cicd.xml.command;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "restartjdk")
@XmlAccessorType(XmlAccessType.FIELD)
public class RestartJdkCommand extends Command {

}
