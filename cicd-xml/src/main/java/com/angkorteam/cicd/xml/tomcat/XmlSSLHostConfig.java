package com.angkorteam.cicd.xml.tomcat;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.namespace.QName;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlSSLHostConfig {

	@XmlAttribute(name = "protocols")
	private String protocols;

	@XmlAttribute(name = "hostName")
	private String hostName = "_default_";

	@XmlAttribute(name = "caCertificateFile")
	private String caCertificateFile;

	@XmlAttribute(name = "certificateVerification")
	private String certificateVerification;

	@XmlAttribute(name = "ciphers")
	private String ciphers;

	@XmlAttribute(name = "truststoreFile")
	private String truststoreFile;

	@XmlAttribute(name = "truststorePassword")
	private String truststorePassword;

	@XmlAttribute(name = "truststoreType")
	private String truststoreType;

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	@XmlElement(name = "Certificate")
	private XmlCertificate certificate;

	public String getProtocols() {
		return protocols;
	}

	public String getHostName() {
		return hostName;
	}

	public String getCaCertificateFile() {
		return caCertificateFile;
	}

	public String getCertificateVerification() {
		return certificateVerification;
	}

	public String getCiphers() {
		return ciphers;
	}

	public String getTruststoreFile() {
		return truststoreFile;
	}

	public String getTruststorePassword() {
		return truststorePassword;
	}

	public XmlCertificate getCertificate() {
		return certificate;
	}

	public Map<QName, String> getExtension() {
		return extension;
	}

	public void setProtocols(String protocols) {
		this.protocols = protocols;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public void setCaCertificateFile(String caCertificateFile) {
		this.caCertificateFile = caCertificateFile;
	}

	public void setCertificateVerification(String certificateVerification) {
		this.certificateVerification = certificateVerification;
	}

	public void setCiphers(String ciphers) {
		this.ciphers = ciphers;
	}

	public void setTruststoreFile(String truststoreFile) {
		this.truststoreFile = truststoreFile;
	}

	public void setTruststorePassword(String truststorePassword) {
		this.truststorePassword = truststorePassword;
	}

	public void setCertificate(XmlCertificate certificate) {
		this.certificate = certificate;
	}

	public String getTruststoreType() {
		return truststoreType;
	}

	public void setTruststoreType(String truststoreType) {
		this.truststoreType = truststoreType;
	}

}
