package com.angkorteam.cicd.xml.command;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.angkorteam.cicd.xml.XmlSsl;

@XmlRootElement(name = "hoststatus")
@XmlAccessorType(XmlAccessType.FIELD)
public class HostStatusCommand extends Command {

	@XmlAttribute(name = "clientId")
	private String clientId;

	@XmlAttribute(name = "status")
	private String status;

	@XmlAttribute(name = "osName")
	private String osName;

	@XmlAttribute(name = "osVersion")
	private String osVersion;

	@XmlAttribute(name = "osArch")
	private String osArch;

	@XmlAttribute(name = "jvmVersion")
	private String jvmVersion;

	@XmlAttribute(name = "jvmVendor")
	private String jvmVendor;

	@XmlAttribute(name = "hostHome")
	private String hostHome;

	@XmlAttribute(name = "localPort")
	private Integer localPort;

	@XmlElement(name = "ssl")
	private List<XmlSsl> ssl = new ArrayList<XmlSsl>();

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getOsArch() {
		return osArch;
	}

	public void setOsArch(String osArch) {
		this.osArch = osArch;
	}

	public String getJvmVersion() {
		return jvmVersion;
	}

	public void setJvmVersion(String jvmVersion) {
		this.jvmVersion = jvmVersion;
	}

	public String getJvmVendor() {
		return jvmVendor;
	}

	public void setJvmVendor(String jvmVendor) {
		this.jvmVendor = jvmVendor;
	}

	public String getHostHome() {
		return hostHome;
	}

	public void setHostHome(String hostHome) {
		this.hostHome = hostHome;
	}

	public Integer getLocalPort() {
		return localPort;
	}

	public void setLocalPort(Integer localPort) {
		this.localPort = localPort;
	}

	public List<XmlSsl> getSsl() {
		return ssl;
	}

}
