package com.angkorteam.cicd.xml.tomcat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.namespace.QName;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlExecutor {

	@XmlAttribute(name = "name")
	private String name;

	@XmlAttribute(name = "tomcatThreadPool")
	private String tomcatThreadPool;

	@XmlAttribute(name = "namePrefix")
	private String namePrefix;

	@XmlAttribute(name = "maxThreads")
	private Integer maxThreads = 150;

	@XmlAttribute(name = "minSpareThreads")
	private Integer minSpareThreads = 4;

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	@XmlElement(name = "Host")
	private List<XmlHost> hosts = new ArrayList<XmlHost>();

	public List<XmlHost> getHosts() {
		return hosts;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTomcatThreadPool() {
		return tomcatThreadPool;
	}

	public void setTomcatThreadPool(String tomcatThreadPool) {
		this.tomcatThreadPool = tomcatThreadPool;
	}

	public String getNamePrefix() {
		return namePrefix;
	}

	public void setNamePrefix(String namePrefix) {
		this.namePrefix = namePrefix;
	}

	public Integer getMaxThreads() {
		return maxThreads;
	}

	public void setMaxThreads(Integer maxThreads) {
		this.maxThreads = maxThreads;
	}

	public Integer getMinSpareThreads() {
		return minSpareThreads;
	}

	public void setMinSpareThreads(Integer minSpareThreads) {
		this.minSpareThreads = minSpareThreads;
	}

	public void setHosts(List<XmlHost> hosts) {
		this.hosts = hosts;
	}

}
