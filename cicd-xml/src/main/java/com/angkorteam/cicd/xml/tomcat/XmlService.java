package com.angkorteam.cicd.xml.tomcat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.namespace.QName;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlService {

	@XmlAttribute(name = "name")
	private String name;

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	@XmlElement(name = "Engine")
	private XmlEngine engine = new XmlEngine();

	@XmlElement(name = "Connector")
	private List<XmlConnector> connectors = new ArrayList<XmlConnector>();

	@XmlElement(name = "Executor")
	private List<XmlExecutor> executors = new ArrayList<XmlExecutor>();

	public List<XmlConnector> getConnectors() {
		return connectors;
	}

	public XmlEngine getEngine() {
		return engine;
	}

	public void setEngine(XmlEngine engine) {
		this.engine = engine;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<QName, String> getExtension() {
		return extension;
	}

	public List<XmlExecutor> getExecutors() {
		return executors;
	}

}
