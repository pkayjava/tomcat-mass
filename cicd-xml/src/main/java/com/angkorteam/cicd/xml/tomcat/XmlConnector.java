package com.angkorteam.cicd.xml.tomcat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.namespace.QName;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlConnector {

	@XmlAttribute(name = "port")
	private Integer port;

	@XmlAttribute(name = "connectionTimeout")
	private Integer connectionTimeout;

	@XmlAttribute(name = "redirectPort")
	private Integer redirectPort;

	@XmlAttribute(name = "protocol")
	private String protocol;

	@XmlAttribute(name = "URIEncoding")
	private String uriEncoding;

	@XmlAttribute(name = "executor")
	private String executor;

	@XmlAttribute(name = "maxThreads")
	private Integer maxThreads;

	@XmlAttribute(name = "SSLEnabled")
	private Boolean SSLEnabled;

	@XmlAttribute(name = "sslImplementationName")
	private String sslImplementationName;

	@XmlAnyAttribute
	private Map<QName, String> extension = new HashMap<QName, String>();

	@XmlElement(name = "UpgradeProtocol")
	private XmlUpgradeProtocol upgradeProtocol;

	@XmlElement(name = "SSLHostConfig")
	private List<XmlSSLHostConfig> sslHostConfigs = new ArrayList<XmlSSLHostConfig>();

	public Integer getPort() {
		return port;
	}

	public Integer getConnectionTimeout() {
		return connectionTimeout;
	}

	public Integer getRedirectPort() {
		return redirectPort;
	}

	public String getProtocol() {
		return protocol;
	}

	public String getUriEncoding() {
		return uriEncoding;
	}

	public Integer getMaxThreads() {
		return maxThreads;
	}

	public String getSslImplementationName() {
		return sslImplementationName;
	}

	public XmlUpgradeProtocol getUpgradeProtocol() {
		return upgradeProtocol;
	}

	public List<XmlSSLHostConfig> getSslHostConfigs() {
		return sslHostConfigs;
	}

	public String getExecutor() {
		return executor;
	}

	public void setExecutor(String executor) {
		this.executor = executor;
	}

	public Boolean getSSLEnabled() {
		return SSLEnabled;
	}

	public void setSSLEnabled(Boolean sSLEnabled) {
		SSLEnabled = sSLEnabled;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public void setConnectionTimeout(Integer connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public void setRedirectPort(Integer redirectPort) {
		this.redirectPort = redirectPort;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public void setUriEncoding(String uriEncoding) {
		this.uriEncoding = uriEncoding;
	}

	public void setMaxThreads(Integer maxThreads) {
		this.maxThreads = maxThreads;
	}

	public void setSslImplementationName(String sslImplementationName) {
		this.sslImplementationName = sslImplementationName;
	}

	public void setUpgradeProtocol(XmlUpgradeProtocol upgradeProtocol) {
		this.upgradeProtocol = upgradeProtocol;
	}

	public Map<QName, String> getExtension() {
		return extension;
	}

}
