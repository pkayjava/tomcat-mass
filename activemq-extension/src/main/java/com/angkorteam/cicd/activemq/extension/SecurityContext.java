package com.angkorteam.cicd.activemq.extension;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

public class SecurityContext extends org.apache.activemq.security.SecurityContext {

	private Set<Principal> groups = new HashSet<>();

	public SecurityContext(String userName) {
		super(userName);
		this.groups.add(new GroupPrincipal("admins"));
		this.groups.add(new GroupPrincipal("users"));
	}

	@Override
	public Set<Principal> getPrincipals() {
		return this.groups;
	}

}
