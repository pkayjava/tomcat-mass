package com.angkorteam.cicd.activemq.extension;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.sql.DataSource;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerFilter;
import org.apache.activemq.broker.ConnectionContext;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ConnectionInfo;
import org.apache.activemq.security.SecurityContext;
import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.cicd.ddl.Token;
import com.angkorteam.cicd.ddl.User;
import com.angkorteam.cicd.ddl.White;

public class CICDBroker extends BrokerFilter {

	protected final DataSource dataSource;

	protected final CopyOnWriteArrayList<SecurityContext> securityContexts = new CopyOnWriteArrayList<SecurityContext>();

	public CICDBroker(Broker next, DataSource dataSource) {
		super(next);
		this.dataSource = dataSource;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update("UPDATE " + White.NAME + " SET " + White.Field.CONNECTED + " = false");
		jdbcTemplate.update("UPDATE " + Host.NAME + " SET " + Host.Field.CONNECTED + " = false");
	}

	@Override
	public void addConnection(ConnectionContext context, ConnectionInfo info) throws Exception {
		SecurityContext securityContext = context.getSecurityContext();
		if (securityContext == null) {
			securityContext = authenticate(info, null);
			context.setSecurityContext(securityContext);
			this.securityContexts.add(securityContext);
		}
		try {
			super.addConnection(context, info);
		} catch (Exception e) {
			this.securityContexts.remove(securityContext);
			context.setSecurityContext(null);
			throw e;
		}
	}

	public SecurityContext authenticate(ConnectionInfo info, X509Certificate[] peerCertificates) throws SecurityException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(this.dataSource);

		String username = info.getUserName();
		String password = info.getPassword();
		String clientId = info.getClientId();

		List<String> infos = new ArrayList<>();
		if (StringUtils.isNotEmpty(clientId)) {
			infos.add(String.format("clientId %s", clientId));
		}
		if (StringUtils.isNotEmpty(username)) {
			infos.add(String.format("username %s", username));
		}
		if (StringUtils.isNotEmpty(password)) {
			infos.add(String.format("password %s", StringUtils.repeat("*", 20)));
		}
		System.out.println(StringUtils.join(infos, " "));

		Map<String, Object> whiteObject = null;
		try {
			if (StringUtils.isNotEmpty(clientId)) {
				whiteObject = jdbcTemplate.queryForMap("SELECT " + White.Field.ID + ", " + White.Field.CLIENT_ID + ", " + White.Field.REVOKED + " FROM " + White.NAME + " WHERE " + White.Field.CLIENT_ID + " = ?", clientId);
			}
		} catch (DataAccessException e) {
		}
		if (whiteObject != null) {
			if ((boolean) whiteObject.get(White.Field.REVOKED) && ((boolean) whiteObject.get(White.Field.CONNECTED))) {
				throw new SecurityException("instance is revoked");
			} else {
				jdbcTemplate.update("UPDATE " + White.NAME + " SET " + White.Field.CONNECTED + " = true WHERE " + White.Field.CLIENT_ID + " = ?", info.getClientId());
				return new com.angkorteam.cicd.activemq.extension.SecurityContext(clientId);
			}
		}

		Map<String, Object> tokenObject = null;
		try {
			if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)) {
				tokenObject = jdbcTemplate.queryForMap("SELECT " + Token.Field.ID + ", " + Token.Field.USER_ID + ", " + Token.Field.REVOKED + " FROM " + Token.NAME + " WHERE " + Token.Field.TOKEN_ID + " = ? AND " + Token.Field.TOKEN_SECRET + " = MD5(?)", username, password);
			}
		} catch (DataAccessException e) {
			throw new SecurityException("token is not valid");
		}

		if (tokenObject == null || (boolean) tokenObject.get(Token.Field.REVOKED)) {
			throw new SecurityException("token is not valid");
		}

		String userId = (String) tokenObject.get(Token.Field.USER_ID);
		Map<String, Object> userObject = null;
		try {
			userObject = jdbcTemplate.queryForMap("SELECT " + User.Field.CONFIRMED + ", " + User.Field.ENABLED + " FROM " + User.NAME + " WHERE " + User.Field.ID + " = ?", userId);
		} catch (DataAccessException e) {
			throw new SecurityException("token is not valid");
		}
		if (userObject == null || !((boolean) userObject.get(User.Field.CONFIRMED)) || !((boolean) userObject.get(User.Field.ENABLED))) {
			throw new SecurityException("token is not valid");
		}

		Map<String, Object> hostObject = null;
		try {
			hostObject = jdbcTemplate.queryForMap("SELECT " + Host.Field.ID + ", " + Host.Field.CONNECTED + " FROM " + Host.NAME + " WHERE " + Host.Field.CLIENT_ID + " = ? AND " + Host.Field.USER_ID + " = ?", clientId, userId);
		} catch (DataAccessException e) {
			throw new SecurityException("instance is not valid");
		}

		if ((boolean) hostObject.get(Host.Field.CONNECTED)) {
			throw new SecurityException("instance connection is not available");
		}

		List<String> values = new ArrayList<>();
		values.add(Host.Field.CONNECTED + " = true");
		int index = info.getClientIp().indexOf("//");
		String clientIp = info.getClientIp();
		if (index > 0) {
			int m = info.getClientIp().indexOf(":", index + 2);
			if (m > 0) {
				clientIp = info.getClientIp().substring(index + 2, m);
			} else {
				clientIp = info.getClientIp().substring(index + 2);
			}
		} else {
			int m = info.getClientIp().indexOf(":");
			if (m > 0) {
				clientIp = info.getClientIp().substring(0, m);
			} else {
				clientIp = info.getClientIp().substring(0);
			}
		}
		values.add(Host.Field.IP_ADDRESS + " = '" + clientIp + "'");
		jdbcTemplate.update("UPDATE " + Host.NAME + " SET " + StringUtils.join(values, ", ") + " WHERE " + Host.Field.ID + " = ?", hostObject.get(Host.Field.ID));

		System.out.println(String.format("instance %s username %s is authenticated", clientId, username));

		return new com.angkorteam.cicd.activemq.extension.SecurityContext(username);
	}

	@Override
	public void removeDestination(ConnectionContext context, ActiveMQDestination destination, long timeout) throws Exception {
		next.removeDestination(context, destination, timeout);

		for (SecurityContext sc : securityContexts) {
			sc.getAuthorizedWriteDests().remove(destination);
		}
	}

	@Override
	public void removeConnection(ConnectionContext context, ConnectionInfo info, Throwable error) throws Exception {
		super.removeConnection(context, info, error);
		if (securityContexts.remove(context.getSecurityContext())) {
			context.setSecurityContext(null);
		}
		JdbcTemplate jdbcTemplate = new JdbcTemplate(this.dataSource);
		jdbcTemplate.update("UPDATE " + Host.NAME + " SET " + Host.Field.CONNECTED + " = false WHERE " + Host.Field.CLIENT_ID + " = ?", info.getClientId());
		jdbcTemplate.update("UPDATE " + White.NAME + " SET " + White.Field.CONNECTED + " = false WHERE " + White.Field.CLIENT_ID + " = ?", info.getClientId());
	}

	public void refresh() {
		for (SecurityContext sc : securityContexts) {
			sc.getAuthorizedWriteDests().clear();
		}
	}

}
