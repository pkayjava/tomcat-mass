package com.angkorteam.cicd.activemq.extension;

import java.sql.Connection;

import javax.sql.DataSource;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerPlugin;
import org.apache.commons.dbcp2.BasicDataSource;

public class CICDPlugin implements BrokerPlugin {

	private String driverClassName;

	private String username;

	private String password;

	private int maxIdle = 10;

	private int minIdle = 5;

	private int maxWaitMillis = 5000;

	private int maxTotal = 20;

	private int initialSize = 5;

	private String url;

	private boolean autoCommit = false;

	private boolean autoCommitOnReturn = true;

	private int transactionIsolation = Connection.TRANSACTION_READ_COMMITTED;

	@Override
	public Broker installPlugin(Broker next) throws Exception {
		return new CICDBroker(next, createDataSource());
	}

	protected DataSource createDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(this.driverClassName);
		dataSource.setUsername(this.username);
		dataSource.setPassword(this.password);
		dataSource.setUrl(this.url);
		dataSource.setMaxIdle(this.maxIdle);
		dataSource.setMinIdle(this.minIdle);
		dataSource.setMaxWaitMillis(this.maxWaitMillis);
		dataSource.setMaxTotal(this.maxTotal);
		dataSource.setInitialSize(this.initialSize);
		dataSource.setDefaultAutoCommit(this.autoCommit);
		dataSource.setEnableAutoCommitOnReturn(this.autoCommitOnReturn);
		dataSource.setDefaultTransactionIsolation(this.transactionIsolation);
		return dataSource;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setMaxIdle(int maxIdle) {
		this.maxIdle = maxIdle;
	}

	public void setMinIdle(int minIdle) {
		this.minIdle = minIdle;
	}

	public void setMaxWaitMillis(int maxWaitMillis) {
		this.maxWaitMillis = maxWaitMillis;
	}

	public void setMaxTotal(int maxTotal) {
		this.maxTotal = maxTotal;
	}

	public void setInitialSize(int initialSize) {
		this.initialSize = initialSize;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setAutoCommit(boolean autoCommit) {
		this.autoCommit = autoCommit;
	}

	public void setAutoCommitOnReturn(boolean autoCommitOnReturn) {
		this.autoCommitOnReturn = autoCommitOnReturn;
	}

	public void setTransactionIsolation(int transactionIsolation) {
		this.transactionIsolation = transactionIsolation;
	}

}
