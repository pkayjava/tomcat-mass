#!/bin/sh

# CATALINA_OPTS="$CATALINA_OPTS -javaagent:/opt/glowroot/glowroot.jar"

# JAVA_OPTS="$JAVA_OPTS -Djavax.net.debug=ssl"
JAVA_OPTS="$JAVA_OPTS -Djava.security.egd=file:/dev/./urandom"

JAVA_OPTS="$JAVA_OPTS -DclientId=COLLECTOR"
JAVA_OPTS="$JAVA_OPTS -DactivemqServer=failover:tcp://172.16.1.107:61616"
JAVA_OPTS="$JAVA_OPTS -DactivemqChannel=stateChannel"

JAVA_OPTS="$JAVA_OPTS -DmysqlUrl='jdbc:mysql://172.16.1.107:3306/tmass?createDatabaseIfNotExist=true&autoReconnect=true&useSSL=false&serverTimezone=UTC'"
JAVA_OPTS="$JAVA_OPTS -DmysqlUsername=root"
JAVA_OPTS="$JAVA_OPTS -DmysqlPassword=password"
JAVA_OPTS="$JAVA_OPTS -DmysqlAutoCommit=true"

JPDA_SUSPEND=y
