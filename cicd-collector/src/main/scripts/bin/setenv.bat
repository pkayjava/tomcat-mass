@echo off

rem set "CATALINA_OPTS=%CATALINA_OPTS% -javaagent:/opt/glowroot/glowroot.jar"

rem set "JAVA_OPTS=%$JAVA_OPTS% -Djavax.net.debug=ssl"

set "JAVA_OPTS=%JAVA_OPTS% -DclientId=COLLECTOR"
set "JAVA_OPTS=%JAVA_OPTS% -DactivemqServer=failover:tcp://172.16.1.107:61616"
set "JAVA_OPTS=%JAVA_OPTS% -DactivemqChannel=stateChannel"

set "JAVA_OPTS=%JAVA_OPTS% -DmysqlUrl='jdbc:mysql://172.16.1.107:3306/tmass?createDatabaseIfNotExist=true&autoReconnect=true&useSSL=false&serverTimezone=UTC'"
set "JAVA_OPTS=%JAVA_OPTS% -DmysqlUsername=root"
set "JAVA_OPTS=%JAVA_OPTS% -DmysqlPassword=password"
set "JAVA_OPTS=%JAVA_OPTS% -DmysqlAutoCommit=true"

set "JPDA_SUSPEND=y"
