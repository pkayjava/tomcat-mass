package com.angkorteam.cicd.collector;

import java.sql.SQLException;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleListener;
import org.apache.catalina.LifecycleState;
import org.apache.catalina.core.StandardServer;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;

import com.mysql.jdbc.Driver;

public class CollectorListener implements LifecycleListener {

	protected String url;
	protected String username;
	protected String password;
	protected String driverClassName = Driver.class.getName();
	protected boolean autoCommit = true;
	protected int maxIdle = 10;
	protected int minIdle = 5;
	protected int maxWaitMillis = 5000;
	protected int maxTotal = 20;
	protected int initialSize = 5;
	protected boolean autoCommitOnReturn = true;
	protected int transactionIsolation = java.sql.Connection.TRANSACTION_READ_COMMITTED;
	protected BasicDataSource dataSource = null;

	protected String activemqServer;
	protected String activemqChannel;
	protected String clientId;

	protected Connection activemqConnection;
	protected Session activemqSession;
	protected MessageConsumer activemqConsumer;

	@Override
	public void lifecycleEvent(LifecycleEvent event) {
		if (event.getSource() != null) {
			if (event.getSource() instanceof StandardServer) {
				if (event.getLifecycle().getState() == LifecycleState.INITIALIZING) {
					initSystem();
					connectDatabase();
					connectActiveMQ();
				} else if (event.getLifecycle().getState() == LifecycleState.DESTROYED) {
					disconnectActiveMQ();
					disconnectDatabase();
				}
			}
		}
	}

	protected void initSystem() {
		this.activemqServer = System.getProperty("activemqServer");
		this.activemqChannel = System.getProperty("activemqChannel");
		this.clientId = System.getProperty("clientId");

		String mysqlDriverClassName = System.getProperty("mysqlDriverClassName");
		if (!StringUtils.isEmpty(mysqlDriverClassName)) {
			this.driverClassName = mysqlDriverClassName;
		}
		this.username = System.getProperty("mysqlUsername");
		this.password = System.getProperty("mysqlPassword");
		this.url = System.getProperty("mysqlUrl");
		String mysqlAutoCommit = System.getProperty("mysqlAutoCommit");
		if (StringUtils.isNotEmpty(mysqlAutoCommit)) {
			this.autoCommit = Boolean.valueOf(mysqlAutoCommit);
		}
	}

	protected void disconnectActiveMQ() {
		try {
			if (this.activemqConsumer != null) {
				this.activemqConsumer.close();
			}
		} catch (JMSException e) {
		}
		try {
			if (this.activemqSession != null) {
				this.activemqSession.close();
			}
		} catch (JMSException e) {
		}
		try {
			if (this.activemqConnection != null) {
				this.activemqConnection.close();
			}
		} catch (JMSException e) {
		}
	}

	protected void connectActiveMQ() {

		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(this.activemqServer);
		connectionFactory.setClientID(this.clientId);

		try {
			// Create a Connection
			this.activemqConnection = connectionFactory.createConnection();
			this.activemqConnection.start();

			// Create a Session
			this.activemqSession = this.activemqConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Create the destination (Topic or Queue)
			Destination destination = this.activemqSession.createQueue(this.activemqChannel);

			// Create a MessageConsumer from the Session to the Topic or Queue
			this.activemqConsumer = this.activemqSession.createConsumer(destination);
			this.activemqConsumer.setMessageListener(new MessageListener(this));
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}

	protected void connectDatabase() {
		this.dataSource = new BasicDataSource();
		this.dataSource.setDriverClassName(this.driverClassName);
		this.dataSource.setUsername(this.username);
		this.dataSource.setPassword(this.password);
		this.dataSource.setUrl(this.url);
		this.dataSource.setDefaultAutoCommit(this.autoCommit);
	}

	protected void disconnectDatabase() {
		if (this.dataSource != null) {
			try {
				this.dataSource.close();
			} catch (SQLException e) {
			}
		}
	}

}
