package com.angkorteam.cicd.collector;

import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.catalina.LifecycleState;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import com.angkorteam.cicd.ddl.Host;
import com.angkorteam.cicd.ddl.HostCipher;
import com.angkorteam.cicd.ddl.HostProtocol;
import com.angkorteam.cicd.ddl.HostSsl;
import com.angkorteam.cicd.ddl.Tomcat;
import com.angkorteam.cicd.ddl.TomcatFile;
import com.angkorteam.cicd.xml.XmlSsl;
import com.angkorteam.cicd.xml.command.ApplicationCommand;
import com.angkorteam.cicd.xml.command.HostStatusCommand;
import com.angkorteam.cicd.xml.command.TomcatStatusCommand;
import com.angkorteam.cicd.xml.message.XmlMessage;
import com.angkorteam.framework.jdbc.DeleteQuery;
import com.angkorteam.framework.jdbc.InsertQuery;
import com.angkorteam.framework.jdbc.UpdateQuery;
import com.angkorteam.framework.spring.JdbcNamed;
import com.angkorteam.framework.spring.JdbcTemplate;

public class MessageListener implements javax.jms.MessageListener {

	private static final Log LOGGER = LogFactory.getLog(MessageListener.class);

	private final CollectorListener listener;

	public MessageListener(CollectorListener listener) {
		this.listener = listener;
	}

	@Override
	public void onMessage(Message message) {
		if (message instanceof TextMessage) {
			onTextMessage((TextMessage) message);
		}
	}

	public void onTextMessage(TextMessage textMessage) {
		try {
			XmlMessage xmlMessage = XmlMessage.fromText(textMessage.getText());
			LOGGER.info(xmlMessage.getCommandClass());
			if (HostStatusCommand.class.getName().equals(xmlMessage.getCommandClass())) {
				HostStatusCommand command = (HostStatusCommand) xmlMessage.getCommand();

				JdbcNamed named = new JdbcNamed(this.listener.dataSource);
				JdbcTemplate jdbcTemplate = new JdbcTemplate(this.listener.dataSource);

				String hostId = named.queryForObject("SELECT " + Host.Field.ID + " FROM " + Host.NAME + " WHERE "
						+ Host.Field.CLIENT_ID + " = '" + command.getClientId() + "'", String.class);

				UpdateQuery updateQuery = null;

				updateQuery = new UpdateQuery(Host.NAME);

				updateQuery.addWhere(Host.Field.CLIENT_ID + " = :client_id", command.getClientId());
				updateQuery.addValue(Host.Field.STATUS + " = :status", command.getStatus());

				if (LifecycleState.STARTING.name().equals(command.getStatus())) {
					updateQuery.addValue(Host.Field.OS_NAME + " = :os_name", command.getOsName());
					updateQuery.addValue(Host.Field.OS_VERSION + " = :os_version", command.getOsVersion());
					updateQuery.addValue(Host.Field.OS_ARCH + " = :os_arch", command.getOsArch());
					updateQuery.addValue(Host.Field.HOST_HOME + " = :host_home", command.getHostHome());
					updateQuery.addValue(Host.Field.LOCAL_PORT + " = :local_port", command.getLocalPort());

					// clear data
					DeleteQuery deleteQuery = null;

					deleteQuery = new DeleteQuery(HostCipher.NAME);
					deleteQuery.addWhere(HostCipher.Field.HOST_ID + " = :host_id", hostId);
					named.update(deleteQuery.toSQL(), deleteQuery.getParam());

					deleteQuery = new DeleteQuery(HostSsl.NAME);
					deleteQuery.addWhere(HostSsl.Field.HOST_ID + " = :host_id", hostId);
					named.update(deleteQuery.toSQL(), deleteQuery.getParam());

					deleteQuery = new DeleteQuery(HostProtocol.NAME);
					deleteQuery.addWhere(HostProtocol.Field.HOST_ID + " = :host_id", hostId);
					named.update(deleteQuery.toSQL(), deleteQuery.getParam());

					if (command.getSsl() != null && !command.getSsl().isEmpty()) {
						for (XmlSsl ssl : command.getSsl()) {
							InsertQuery insertQuery = null;
							String hostSslId = jdbcTemplate.queryForObject("SELECT uuid() FROM DUAL", String.class);
							insertQuery = new InsertQuery(HostSsl.NAME);
							insertQuery.addValue(HostSsl.Field.ID + " = :id", hostSslId);
							insertQuery.addValue(HostSsl.Field.NAME + " = :name", ssl.getImplementation());
							insertQuery.addValue(HostSsl.Field.TYPE + " = :type", ssl.getType());
							insertQuery.addValue(HostSsl.Field.HOST_ID + " = :host_id", hostId);
							insertQuery.addValue(HostSsl.Field.SELECTED + " = :selected", ssl.getSelected());
							named.update(insertQuery.toSQL(), insertQuery.getParam());
							for (String cipher : ssl.getCipher()) {
								insertQuery = new InsertQuery(HostCipher.NAME);
								insertQuery.addValue(HostCipher.Field.ID + " = uuid()");
								insertQuery.addValue(HostCipher.Field.HOST_SSL_ID + " = :id", hostSslId);
								insertQuery.addValue(HostCipher.Field.NAME + " = :name", cipher);
								insertQuery.addValue(HostCipher.Field.HOST_ID + " = :host_id", hostId);
								named.update(insertQuery.toSQL(), insertQuery.getParam());
							}
							for (String protocol : ssl.getProtocol()) {
								insertQuery = new InsertQuery(HostProtocol.NAME);
								insertQuery.addValue(HostProtocol.Field.ID + " = uuid()");
								insertQuery.addValue(HostProtocol.Field.HOST_SSL_ID + " = :id", hostSslId);
								insertQuery.addValue(HostProtocol.Field.NAME + " = :name", protocol);
								insertQuery.addValue(HostProtocol.Field.HOST_ID + " = :host_id", hostId);
								named.update(insertQuery.toSQL(), insertQuery.getParam());
							}
						}
					}
				}

				named.update(updateQuery.toSQL(), updateQuery.getParam());

			} else if (TomcatStatusCommand.class.getName().equals(xmlMessage.getCommandClass())) {
				TomcatStatusCommand command = (TomcatStatusCommand) xmlMessage.getCommand();
				LOGGER.info("status " + command.getStatus());

				JdbcNamed named = new JdbcNamed(this.listener.dataSource);
				JdbcTemplate jdbcTemplate = new JdbcTemplate(this.listener.dataSource);

				String hostId = named.queryForObject("SELECT " + Host.Field.ID + " FROM " + Host.NAME + " WHERE "
						+ Host.Field.CLIENT_ID + " = '" + command.getClientId() + "'", String.class);
				String tomcatId = jdbcTemplate.queryForObject(
						"SELECT " + Tomcat.Field.ID + " FROM " + Tomcat.NAME + " WHERE " + Tomcat.Field.HOST_ID
								+ " = ? AND " + Tomcat.Field.INSTANCE + " = ?",
						String.class, hostId, command.getInstance());

				UpdateQuery updateQuery = null;

				updateQuery = new UpdateQuery(Tomcat.NAME);

				updateQuery.addWhere(Tomcat.Field.ID + " = :id", tomcatId);
				updateQuery.addValue(Tomcat.Field.STATUS + " = :status", command.getStatus());

				if (LifecycleState.INITIALIZING.name().equals(command.getStatus())) {
					updateQuery.addValue(Tomcat.Field.TOMCAT_VERSION + " = :os_arch", command.getTomcatVersion());
					updateQuery.addValue(Tomcat.Field.JVM_VERSION + " = :jvm_version", command.getJvmVersion());
					updateQuery.addValue(Tomcat.Field.JVM_VENDOR + " = :jvm_vendor", command.getJvmVendor());
				}

				named.update(updateQuery.toSQL(), updateQuery.getParam());

				if (LifecycleState.INITIALIZING.name().equals(command.getStatus())
						|| LifecycleState.STOPPING.name().equals(command.getStatus())) {
					jdbcTemplate.update("UPDATE " + TomcatFile.NAME + " SET " + TomcatFile.Field.STATUS + " = '"
							+ TomcatFile.STATUS_STOPPED + "' WHERE " + TomcatFile.Field.TOMCAT_ID + " = ? AND "
							+ TomcatFile.Field.TYPE + " = ?", tomcatId, TomcatFile.TYPE_BACKGROUND);
					jdbcTemplate.update("UPDATE " + TomcatFile.NAME + " SET " + TomcatFile.Field.STATUS + " = '"
							+ TomcatFile.STATUS_STOPPED + "' WHERE " + TomcatFile.Field.TOMCAT_ID + " = ? AND "
							+ TomcatFile.Field.TYPE + " = ?", tomcatId, TomcatFile.TYPE_WAR);
				}

			} else if (ApplicationCommand.class.getName().equals(xmlMessage.getCommandClass())) {
				ApplicationCommand command = (ApplicationCommand) xmlMessage.getCommand();

				JdbcNamed named = new JdbcNamed(this.listener.dataSource);
				JdbcTemplate jdbcTemplate = new JdbcTemplate(this.listener.dataSource);

				String hostId = named.queryForObject("SELECT " + Host.Field.ID + " FROM " + Host.NAME + " WHERE "
						+ Host.Field.CLIENT_ID + " = '" + command.getClientId() + "'", String.class);
				String tomcatId = jdbcTemplate.queryForObject(
						"SELECT " + Tomcat.Field.ID + " FROM " + Tomcat.NAME + " WHERE " + Tomcat.Field.HOST_ID
								+ " = ? AND " + Tomcat.Field.INSTANCE + " = ?",
						String.class, hostId, command.getInstance());

				String war = command.getResource();
				String type = command.getType();
				if ("WAR".equals(type)) {
					UpdateQuery updateQuery = new UpdateQuery(TomcatFile.NAME);

					updateQuery.addValue(TomcatFile.Field.STATUS + " = :status", command.getStatus());

					updateQuery.addWhere(TomcatFile.Field.TOMCAT_ID + " = :tomcat_id", tomcatId);
					updateQuery.addWhere(TomcatFile.Field.TYPE + " = :type", TomcatFile.TYPE_WAR);
					updateQuery.addWhere(TomcatFile.Field.PATH + " = :path", war);

					named.update(updateQuery.toSQL(), updateQuery.getParam());

				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

}
