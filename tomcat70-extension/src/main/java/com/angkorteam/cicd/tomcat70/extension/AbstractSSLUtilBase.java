package com.angkorteam.cicd.tomcat70.extension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSessionContext;
import javax.net.ssl.TrustManager;

import org.apache.tomcat.util.net.AbstractEndpoint;
import org.apache.tomcat.util.net.Constants;
import org.apache.tomcat.util.net.jsse.JSSESocketFactory;

public abstract class AbstractSSLUtilBase extends JSSESocketFactory {

	protected String[] enabledProtocols;
	protected String[] enabledCiphers;
	protected AbstractEndpoint<?> endpoint;
	protected static final String defaultProtocol = Constants.SSL_PROTO_TLS;

	public AbstractSSLUtilBase(AbstractEndpoint<?> endpoint) {
		super(endpoint);
		this.endpoint = endpoint;

		Set<String> configuredProtocols = new TreeSet<String>();
		configuredProtocols.addAll(Arrays.asList(endpoint.getSslEnabledProtocolsArray()));
		Set<String> implementedProtocols = getImplementedProtocols();
		List<String> enabledProtocols = getEnabled("protocols", true, configuredProtocols, implementedProtocols);
		this.enabledProtocols = enabledProtocols.toArray(new String[enabledProtocols.size()]);

		List<String> ciphers = new ArrayList<>();
		if (endpoint.getCiphers() != null && !"".equals(endpoint.getCiphers()) && !"HIGH:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!kRSA".equals(endpoint.getCiphers())) {
			for (String cipher : endpoint.getCiphers().split(",")) {
				ciphers.add(cipher);
			}
		}
		if (ciphers.isEmpty()) {
			for (String cipher : getImplementedCiphers()) {
				ciphers.add(cipher);
			}
		} else {
			ciphers.retainAll(getImplementedCiphers());
		}
		this.enabledCiphers = ciphers.toArray(new String[ciphers.size()]);
	}

	static <T> List<T> getEnabled(String name, boolean warnOnSkip, Collection<T> configured, Collection<T> implemented) {

		List<T> enabled = new ArrayList<>();

		if (implemented.size() == 0) {
			// Unable to determine the list of available protocols. This will
			// have been logged previously.
			// Use the configuredProtocols and hope they work. If not, an error
			// will be generated when the list is used. Not ideal but no more
			// can be done at this point.
			enabled.addAll(configured);
		} else {
			enabled.addAll(configured);
			enabled.retainAll(implemented);

			if (enabled.isEmpty()) {
				throw new IllegalArgumentException("no supported protocol");
			}
		}

		return enabled;
	}

	@Override
	public String[] getEnableableProtocols(SSLContext context) {
		return this.enabledProtocols;
	}

	@Override
	public KeyManager[] getKeyManagers() throws Exception {
		return super.getKeyManagers();
	}

	@Override
	public TrustManager[] getTrustManagers() throws Exception {
		return super.getTrustManagers();
	}

	@Override
	public void configureSessionContext(SSLSessionContext sslSessionContext) {
		super.configureSessionContext(sslSessionContext);
	}

	@Override
	public String[] getEnableableCiphers(SSLContext context) {
		return this.enabledCiphers;
	}

	protected abstract Set<String> getImplementedProtocols();

	protected abstract Set<String> getImplementedCiphers();

}
