package com.angkorteam.cicd.tomcat70.extension.google;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.apache.tomcat.util.compat.JreVendor;
import org.apache.tomcat.util.net.AbstractEndpoint;
import org.apache.tomcat.util.net.Constants;
import org.apache.tomcat.util.res.StringManager;
import org.conscrypt.OpenSSLProvider;

import com.angkorteam.cicd.tomcat70.extension.AbstractSSLUtilBase;
import com.angkorteam.cicd.tomcat70.extension.ExposeJSSESSLContext;
import com.angkorteam.cicd.tomcat70.extension.SSLContext;

public class GoogleSSLUtil extends AbstractSSLUtilBase {

	private static final Log log = LogFactory.getLog(GoogleSSLUtil.class);
	private static final StringManager sm = StringManager.getManager(GoogleSSLUtil.class);

	public static Set<String> PROTOCOLS;
	public static Set<String> CIPHERS;

	static {
		SSLContext context;
		try {
			context = new ExposeJSSESSLContext(Constants.SSL_PROTO_TLS, new OpenSSLProvider());
			context.init(null, null, null);
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			// This is fatal for the connector so throw an exception to prevent
			// it from starting
			throw new IllegalArgumentException(e);
		}

		String[] implementedProtocolsArray = context.getSupportedSSLParameters().getProtocols();
		PROTOCOLS = new HashSet<>(implementedProtocolsArray.length);

		// Filter out SSLv2 from the list of implemented protocols (just in case
		// we are running on a JVM that supports it) since it is no longer
		// considered secure but allow SSLv2Hello.
		// Note SSLv3 is allowed despite known insecurities because some users
		// still have a requirement for it.
		for (String protocol : implementedProtocolsArray) {
			String protocolUpper = protocol.toUpperCase(Locale.ENGLISH);
			if (!"SSLV2HELLO".equals(protocolUpper) && !"SSLV3".equals(protocolUpper)) {
				if (protocolUpper.contains("SSL")) {
					log.debug(sm.getString("jsse.excludeProtocol", protocol));
					continue;
				}
			}
			PROTOCOLS.add(protocol);
		}

		if (PROTOCOLS.size() == 0) {
			log.warn(sm.getString("jsse.noDefaultProtocols"));
		}
		PROTOCOLS = Collections.unmodifiableSet(PROTOCOLS);

		String[] implementedCipherSuiteArray = context.getSupportedSSLParameters().getCipherSuites();
		// The IBM JRE will accept cipher suites names SSL_xxx or TLS_xxx but
		// only returns the SSL_xxx form for supported cipher suites. Therefore
		// need to filter the requested cipher suites using both forms with an
		// IBM JRE.
		if (JreVendor.IS_IBM_JVM) {
			CIPHERS = new HashSet<>(implementedCipherSuiteArray.length * 2);
			for (String name : implementedCipherSuiteArray) {
				CIPHERS.add(name);
				if (name.startsWith("SSL")) {
					CIPHERS.add("TLS" + name.substring(3));
				}
			}
		} else {
			CIPHERS = new HashSet<>(implementedCipherSuiteArray.length);
			CIPHERS.addAll(Arrays.asList(implementedCipherSuiteArray));
		}
		CIPHERS = Collections.unmodifiableSet(CIPHERS);
	}

	public GoogleSSLUtil(AbstractEndpoint<?> endpoint) {
		super(endpoint);
	}

	@Override
	protected Set<String> getImplementedProtocols() {
		return PROTOCOLS;
	}

	@Override
	protected Set<String> getImplementedCiphers() {
		return CIPHERS;
	}

	@Override
	public javax.net.ssl.SSLContext createSSLContext() throws Exception {
		String protocol = endpoint.getSslProtocol();
		if (protocol == null) {
			protocol = defaultProtocol;
		}

		return javax.net.ssl.SSLContext.getInstance(protocol, new OpenSSLProvider());
	}

}
