package com.angkorteam.cicd.tomcat70.extension;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class ExposePEMFile extends com.angkorteam.cicd.tomcat70.extension.PEMFile {

	public ExposePEMFile(String filename) throws IOException, GeneralSecurityException {
		super(filename);
	}

	public ExposePEMFile(String filename, String password) throws IOException, GeneralSecurityException {
		super(filename, password);
	}

}
