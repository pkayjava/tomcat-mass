package com.angkorteam.cicd.tomcat70.extension.google;

import org.apache.tomcat.util.net.AbstractEndpoint;
import org.apache.tomcat.util.net.SSLUtil;
import org.apache.tomcat.util.net.jsse.JSSEImplementation;

public class GoogleSSLImplementation extends JSSEImplementation {

	@Override
	public SSLUtil getSSLUtil(AbstractEndpoint<?> endpoint) {
		return super.getSSLUtil(endpoint);
	}

}
