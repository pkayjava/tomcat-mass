package com.angkorteam.cicd.tomcat80.extension.jsse;

import org.apache.tomcat.util.net.AbstractEndpoint;
import org.apache.tomcat.util.net.SSLUtil;
import org.apache.tomcat.util.net.ServerSocketFactory;
import org.apache.tomcat.util.net.jsse.JSSEImplementation;

public class JsseSSLImplementation extends JSSEImplementation {

	@Override
	public SSLUtil getSSLUtil(AbstractEndpoint<?> endpoint) {
		return new JsseUtil(endpoint);
	}

	@Override
	public ServerSocketFactory getServerSocketFactory(AbstractEndpoint<?> endpoint) {
		return new JsseUtil(endpoint);
	}

}
